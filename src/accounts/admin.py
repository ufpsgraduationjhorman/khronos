from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import (
    UsuarioUFPS,
)

# Register your models here.


class UsuarioUFPSAdmin(UserAdmin):
    list_display = ('correo', 'apellidos', 'nombres', 'es_activo')
    # inlines = [PerfilEstudianteInline, PerfilProfesorInline]
    ordering = ['correo']
    list_filter = ['correo']

admin.site.register(UsuarioUFPS, UsuarioUFPSAdmin)