from django.apps import AppConfig


class AccountsConfig(AppConfig):
    name = 'accounts'
    verbose_name = 'Cuentas de Usuario'

    def ready(self):
        import accounts.signals
