from django.contrib.auth import authenticate, get_user_model
from .custom_model import (
    get_estudiante,
    get_docente,
)
from oauth2client import client, crypt
from schedulex.settings import (
    CLIENT_ID,
    GSUITE_DOMAIN_NAME,
)
from .models import DirectorProgramaUFPS
from rest_framework.authentication import TokenAuthentication
from rest_framework.exceptions import AuthenticationFailed
import datetime

User = get_user_model()


class UsuarioUFPSAuthBackend:
    def authenticate(self, **kwargs):
        idinfo = client.verify_id_token(kwargs['id_token'], CLIENT_ID)
        if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
            raise crypt.AppIdentityError('Wrong issuer')
        if 'hd' not in idinfo or idinfo['hd'] != GSUITE_DOMAIN_NAME:
            raise crypt.AppIdentityError('Para acceder a la aplicación debe utilizar una cuenta de correo asociada '
                                         'al dominio web de la UFPS (@ufps.edu.co)')
        userid = idinfo['sub']
        # Validar el rol con el usuario.###
        persona = None
        plan_de_estudios = codigo = ''
        if kwargs['rol_usuario'] == '1': # Si rol estudiante
            persona = get_estudiante(correoelectronico=idinfo['email'])
            plan_de_estudios = '115'
            codigo = ''
            if persona:
                plan_de_estudios = persona['plandeestudios']
                codigo = persona['codigo']
        if kwargs['rol_usuario'] == '2': # Si rol docente
            persona = get_docente(correoelectronico=idinfo['email'])
            if not persona:
                raise ValueError('Correo o tipo de usuario incorrecto')
            plan_de_estudios = persona['plandeestudios'] if kwargs['rol_usuario'] != '2' else None
            codigo = persona['codigo']
        if kwargs['rol_usuario'] == '3':
            try:
                persona = DirectorProgramaUFPS.objects.get(correo=idinfo['email'])
                plan_de_estudios = persona.plan_de_estudios
                codigo = persona.codigo
            except DirectorProgramaUFPS.DoesNotExist:
                raise ValueError('Correo o tipo de usuario incorrecto')
        try:
            usuario, created = User.objects.update_or_create(
                user_id_google=userid,
                defaults={
                    'nombres': idinfo['given_name'],
                    'apellidos': idinfo['family_name'],
                    'ingreso_tipo': int(kwargs['rol_usuario']),
                    'correo': idinfo['email'],
                    'plan_de_estudios': plan_de_estudios,
                    'departamento': persona['departamento'] if kwargs['rol_usuario'] == '2' else plan_de_estudios[1:],
                    'codigo': codigo,
                }
            )
        except (KeyError, ValueError) as error:
            return error
        return usuario

    def get_user(self, id_usuario):
        try:
            return User.objects.get(pk=id_usuario)
        except User.DoesNotExist:
            return None

