from django.db import connections


def get_estudiante(**credentials):
    cursor = connections['default'].cursor()
    cursor.execute("SELECT * FROM alumno WHERE correoelectronico = %s",
                   [credentials['correoelectronico']])
    row = cursor.fetchone()
    if row:
        columns = [col[0] for col in cursor.description]
        return dict(zip(columns, row))
    return None


def get_docente(**credentials):
    cursor = connections['default'].cursor()
    cursor.execute("SELECT * FROM profesor WHERE correoelectronico = %s",
                   [credentials['correoelectronico']])
    row = cursor.fetchone()
    if row:
        columns = [col[0] for col in cursor.description]
        return dict(zip(columns, row))
    return None
