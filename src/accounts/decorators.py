from django.core.exceptions import PermissionDenied


def login_required_ajax(function):
    def wrap(request, *args, **kwargs):
        if request.user.is_authenticated() and request.user.es_activo():
            return function(request, *args, **kwargs)
        else:
            raise PermissionDenied
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


def login_required_director_programa(funcion=None):
    def login_required_wrapper(function):
        def wrap(request, *args, **kwargs):
            if request.user.is_authenticated() and request.user.es_activo() and request.user.es_director():
                return function(request, *args, **kwargs)
            else:
                raise PermissionDenied
        wrap.__doc__ = function.__doc__
        wrap.__name__ = function.__name__
        return wrap
    return login_required_wrapper