# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import accounts.models
import django.utils.timezone
import django.core.validators
from django.conf import settings
import re


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='UsuarioUFPS',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(verbose_name='password', max_length=128)),
                ('last_login', models.DateTimeField(verbose_name='last login', null=True, blank=True)),
                ('is_superuser', models.BooleanField(verbose_name='superuser status', default=False, help_text='Designates that this user has all permissions without explicitly assigning them.')),
                ('correo', models.CharField(verbose_name='correo', max_length=100, help_text='Obligatorio. Debe ser un correo institucional de la UFPS.', validators=[django.core.validators.RegexValidator(re.compile('^[\\w.@ufps.edu.co+-]+$', 32), 'Ingrese un correo válido.', 'invalid')], unique=True)),
                ('user_id_google', models.CharField(verbose_name='user_id_google', max_length=50, unique=True)),
                ('nombres', models.CharField(verbose_name='nombres', max_length=50, null=True)),
                ('apellidos', models.CharField(verbose_name='apellidos', max_length=50, null=True)),
                ('ingreso_tipo', models.SmallIntegerField(verbose_name='ingreso_tipo')),
                ('codigo', models.CharField(verbose_name='codigo', max_length=10, unique=True)),
                ('is_staff', models.BooleanField(verbose_name='staff status', default=False, help_text='Determina si un usuario puede o no acceder al sitio de administración.')),
                ('estado', models.BooleanField(verbose_name='estado', default=True, help_text='Define el estado de un usuario (activo o inactivo).')),
                ('fecha_registro', models.DateTimeField(verbose_name='date joined', default=django.utils.timezone.now)),
                ('groups', models.ManyToManyField(verbose_name='groups', help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', blank=True)),
                ('user_permissions', models.ManyToManyField(verbose_name='user permissions', help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', blank=True)),
            ],
            options={
                'verbose_name_plural': 'Usuarios Schedulex UFPS',
                'db_table': 'auth_user_schedulex',
            },
            managers=[
                ('objects', accounts.models.UsuarioUFPSManager()),
            ],
        ),
        migrations.CreateModel(
            name='UsuarioUFPSTipo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tipo_usuario', models.SmallIntegerField(verbose_name='tipo_usuario')),
                ('usuario', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
