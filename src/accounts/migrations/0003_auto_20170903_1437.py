# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import re
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_remove_usuarioufpstipo_tipo_usuario'),
    ]

    operations = [
        migrations.CreateModel(
            name='DirectorProgramaUFPS',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('correo', models.CharField(unique=True, max_length=100, validators=[django.core.validators.RegexValidator(re.compile('^[\\w.@ufps.edu.co+-]+$', 32), 'Ingrese un correo válido.', 'invalid')], verbose_name='correo')),
                ('user_id_google', models.CharField(unique=True, max_length=50, verbose_name='user_id_google')),
            ],
        ),
        migrations.RemoveField(
            model_name='usuarioufpstipo',
            name='usuario',
        ),
        migrations.DeleteModel(
            name='UsuarioUFPSTipo',
        ),
    ]
