# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_auto_20170903_1437'),
    ]

    operations = [
        migrations.AddField(
            model_name='usuarioufps',
            name='plan_de_estudios',
            field=models.CharField(max_length=3, verbose_name='plan_de_estudios', null=True),
        ),
    ]
