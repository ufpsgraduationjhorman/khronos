# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0004_usuarioufps_plan_de_estudios'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='directorprogramaufps',
            name='user_id_google',
        ),
    ]
