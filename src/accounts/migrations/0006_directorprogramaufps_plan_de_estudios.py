# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0005_remove_directorprogramaufps_user_id_google'),
    ]

    operations = [
        migrations.AddField(
            model_name='directorprogramaufps',
            name='plan_de_estudios',
            field=models.CharField(default='', max_length=3),
        ),
    ]
