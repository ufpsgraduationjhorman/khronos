# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0006_directorprogramaufps_plan_de_estudios'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usuarioufps',
            name='codigo',
            field=models.CharField(verbose_name='codigo', max_length=10),
        ),
        migrations.AlterUniqueTogether(
            name='usuarioufps',
            unique_together=set([('plan_de_estudios', 'codigo')]),
        ),
    ]
