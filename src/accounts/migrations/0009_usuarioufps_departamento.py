# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0008_auto_20171218_1555'),
    ]

    operations = [
        migrations.AddField(
            model_name='usuarioufps',
            name='departamento',
            field=models.CharField(verbose_name='departamento', max_length=2, null=True),
        ),
    ]
