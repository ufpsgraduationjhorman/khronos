# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0009_usuarioufps_departamento'),
    ]

    operations = [
        migrations.AddField(
            model_name='directorprogramaufps',
            name='codigo',
            field=models.CharField(default='', max_length=10),
        ),
    ]
