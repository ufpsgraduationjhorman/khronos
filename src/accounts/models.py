from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.core import validators
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
import datetime
import pytz
import re

# Create your models here.


class UsuarioUFPSManager(BaseUserManager):

    use_in_migrations = True

    def _create_user(self, user_id_google, correo, codigo, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not correo:
            raise ValueError('Debe agregar un correo electrónico al usuario.')
        correo = self.normalize_email(correo)
        user = self.model(correo=correo, codigo=codigo, **extra_fields)
        user.save(using=self._db)
        return user

    def create_user(self, login, correo, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(login, correo, **extra_fields)

    def create_superuser(self, login, correo, **extra_fields):
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser debe tener is_superuser=True.')

        return self._create_user(login, correo, **extra_fields)


class UsuarioUFPS(AbstractBaseUser, PermissionsMixin):
    correo = models.CharField(_('correo'), max_length=100, unique=True,
                             help_text=_('Obligatorio. Debe ser un correo institucional de la UFPS.'),
                             validators=[
                                 validators.RegexValidator(re.compile('^[\w.@ufps.edu.co+-]+$'),
                                                           _('Ingrese un correo válido.'), 'invalid')
                             ])
    user_id_google = models.CharField('user_id_google', max_length=50, unique=True)
    nombres = models.CharField(_('nombres'), max_length=50, null=True)
    apellidos = models.CharField(_('apellidos'), max_length=50, null=True)
    ingreso_tipo = models.SmallIntegerField(_('ingreso_tipo'))
    departamento = models.CharField(_('departamento'), max_length=2, null=True)
    plan_de_estudios = models.CharField(_('plan_de_estudios'), max_length=3, null=True)
    codigo = models.CharField(_('codigo'), max_length=10)

    is_staff = models.BooleanField(_('staff status'), default=False,
                                   help_text=_('Determina si un usuario puede o no acceder al sitio de '
                                               'administración.'))
    estado = models.BooleanField(_('estado'), default=True,
                                 help_text=_('Define el estado de un usuario (activo o inactivo).'))
    fecha_registro = models.DateTimeField(_('date joined'), default=timezone.now)

    objects = UsuarioUFPSManager()

    USERNAME_FIELD = 'user_id_google'

    REQUIRED_FIELDS = ['correo', 'codigo']

    def es_activo(self):
        return self.estado

    def es_estudiante(self):
        return self.ingreso_tipo == 1

    def es_docente(self):
        return self.ingreso_tipo == 2

    def es_director(self):
        return self.ingreso_tipo == 3

    @property
    def auth_token(self):
        try:
            return Token.objects.get(user=self.id)
        except Token.DoesNotExist:
            return None


    class Meta:
        db_table = 'auth_user_schedulex'
        verbose_name_plural = 'Usuarios Schedulex UFPS'


class DirectorProgramaUFPS(models.Model):
    correo = models.CharField(_('correo'), max_length=100, unique=True,
                              validators=[
                                  validators.RegexValidator(re.compile('^[\w.@ufps.edu.co+-]+$'),
                                                            _('Ingrese un correo válido.'), 'invalid')
                              ])
    plan_de_estudios = models.CharField(max_length=3, default='')
    codigo = models.CharField(max_length=10, default='')


# @receiver(post_save, sender=settings.AUTH_USER_MODEL)
# def create_auth_token(sender, instance=None, created=False, **kwargs):
#     token, created = Token.objects.get_or_create(
#         user=instance
#     )
#     if not created:
#         # update the created time of the token to keep it valid
#         print('jojo')
#         token.created = datetime.datetime.utcnow()
#         token.save()
#     # if token.created < utc_now - datetime.timedelta(hours=2):
#     #     raise AuthenticationFailed('Token has expired')
