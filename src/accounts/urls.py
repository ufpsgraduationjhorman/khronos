from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'^$', views.IndexPublicView.as_view(), name='index'),
    url(r'^login/$', views.log_in, name='log-in'),
    url(r'^logout/$', views.log_out, name='log-out'),
    # ex: /signup/
    #url(r'^signup/$', views.sign_up, name='sign-up'),
]