from django.shortcuts import render
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.contrib.auth import (
    authenticate,
    login,
    logout,
    get_user_model
)
from oauth2client import crypt


User = get_user_model()


class IndexPublicView(generic.TemplateView):
    template_name = 'accounts/index.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponseRedirect(reverse('timetabling:index'))
        return self.render_to_response({})


def log_in(request):
    next_page = request.GET.get('next', 'timetabling:index')
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse(next_page))
    if request.method == 'POST':
        rol_usuario = request.POST['rol_usuario']
        id_token = request.POST['id_token']
        try:
            auth_user = authenticate(id_token=id_token, rol_usuario=rol_usuario)
        except crypt.AppIdentityError as error:
            return JsonResponse({'operacion': 0, 'detalles': str(error)})
        except (KeyError, ValueError) as error:
            return JsonResponse({'operacion': 0, 'detalles': 'Ingreso fallido ({0}).'.format(error)})
        if not auth_user:
            return JsonResponse({'operacion': 0, 'detalles': 'Ingreso fallido.'})
        login(request, auth_user)
        if auth_user.es_estudiante():
            return JsonResponse({'operacion': 1, 'detalles': 'Ingreso exitoso.', 'tipo_ingreso': 1,})
        elif auth_user.es_docente():
            return JsonResponse({'operacion': 1, 'detalles': 'Ingreso exitoso.', 'tipo_ingreso': 2,})
        elif auth_user.es_director():
            return JsonResponse({'operacion': 1, 'detalles': 'Ingreso exitoso.', 'tipo_ingreso': 3,})

    return JsonResponse({'operacion': 0, 'detalles': 'Ingreso fallido.'}, status=405)


def log_out(request):
    logout(request)
    return HttpResponseRedirect(reverse('accounts:index'))
