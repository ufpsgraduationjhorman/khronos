# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Alumno',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('plandeestudios', models.CharField(max_length=3)),
                ('codigo', models.CharField(max_length=4)),
                ('documento', models.CharField(max_length=15, null=True, blank=True)),
                ('tipodocumento', models.CharField(max_length=2, null=True, blank=True)),
                ('nombres', models.CharField(max_length=100, null=True, blank=True)),
                ('sexo', models.CharField(max_length=1, null=True, blank=True)),
                ('seccional', models.CharField(max_length=2, null=True, blank=True)),
                ('pensum', models.CharField(max_length=2, null=True, blank=True)),
                ('correoelectronico', models.CharField(max_length=200, null=True, blank=True)),
                ('estahaciendo', models.CharField(max_length=1, null=True, blank=True)),
                ('creditoscursados', models.SmallIntegerField(null=True, blank=True)),
                ('creditosaprobados', models.SmallIntegerField(null=True, blank=True)),
                ('semestre', models.SmallIntegerField(null=True, blank=True)),
                ('matriculado', models.CharField(max_length=1, null=True, blank=True)),
            ],
            options={
                'db_table': 'alumno',
            },
        ),
        migrations.CreateModel(
            name='Grupo',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('plandeestudios', models.CharField(max_length=3)),
                ('materia', models.CharField(max_length=4)),
                ('grupo', models.CharField(max_length=2)),
                ('seccional', models.CharField(max_length=2, null=True, blank=True)),
                ('profesor', models.CharField(max_length=5, null=True, blank=True)),
                ('maximoalumnos', models.SmallIntegerField(null=True, blank=True)),
                ('totalalumnos', models.SmallIntegerField(null=True, blank=True)),
            ],
            options={
                'db_table': 'grupo',
            },
        ),
        migrations.CreateModel(
            name='Horario',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('plandeestudios', models.CharField(max_length=3)),
                ('materia', models.CharField(max_length=4)),
                ('grupo', models.CharField(max_length=2)),
                ('dia', models.CharField(max_length=12)),
                ('salon', models.CharField(max_length=5, null=True, blank=True)),
                ('seccional', models.CharField(max_length=2, null=True, blank=True)),
                ('horainicial', models.TimeField(null=True, blank=True)),
                ('horafinal', models.TimeField(null=True, blank=True)),
            ],
            options={
                'db_table': 'horario',
            },
        ),
        migrations.CreateModel(
            name='Listagrupo',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('curso', models.CharField(max_length=9)),
                ('alumno', models.CharField(max_length=7)),
                ('seccional', models.CharField(max_length=2, null=True, blank=True)),
            ],
            options={
                'db_table': 'listagrupo',
            },
        ),
        migrations.CreateModel(
            name='Materia',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('plandeestudios', models.CharField(max_length=3)),
                ('codigo', models.CharField(max_length=4)),
                ('departamento', models.CharField(max_length=2, null=True, blank=True)),
                ('nombre', models.CharField(max_length=60, null=True, blank=True)),
                ('horasteorica', models.SmallIntegerField(null=True, blank=True)),
                ('horaspractica', models.SmallIntegerField(null=True, blank=True)),
                ('creditos', models.SmallIntegerField(null=True, blank=True)),
                ('semestre', models.CharField(max_length=1, null=True, blank=True)),
            ],
            options={
                'db_table': 'materia',
            },
        ),
        migrations.CreateModel(
            name='Matricula',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('plandeestudios', models.CharField(max_length=3)),
                ('alumno', models.CharField(max_length=4)),
                ('materia', models.CharField(max_length=4)),
                ('plandeestudiosmatricula', models.CharField(max_length=3, null=True, blank=True)),
                ('materiamatricula', models.CharField(max_length=4, null=True, blank=True)),
                ('grupo', models.CharField(max_length=2, null=True, blank=True)),
                ('seccional', models.CharField(max_length=2, null=True, blank=True)),
                ('estado', models.CharField(max_length=1, null=True, blank=True)),
            ],
            options={
                'db_table': 'matricula',
            },
        ),
        migrations.CreateModel(
            name='Profesor',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('departamento', models.CharField(max_length=2)),
                ('codigo', models.CharField(max_length=5)),
                ('documento', models.CharField(max_length=15)),
                ('tipodocumento', models.CharField(max_length=2, null=True, blank=True)),
                ('nombres', models.CharField(max_length=100, null=True, blank=True)),
                ('sexo', models.CharField(max_length=1, null=True, blank=True)),
                ('vinculacion', models.CharField(max_length=3, null=True, blank=True)),
                ('seccional', models.CharField(max_length=2, null=True, blank=True)),
                ('correoelectronico', models.CharField(max_length=200, null=True, blank=True)),
            ],
            options={
                'db_table': 'profesor',
            },
        ),
        migrations.AlterUniqueTogether(
            name='profesor',
            unique_together=set([('departamento', 'codigo', 'documento')]),
        ),
        migrations.AlterUniqueTogether(
            name='matricula',
            unique_together=set([('plandeestudios', 'alumno', 'materia')]),
        ),
        migrations.AlterUniqueTogether(
            name='materia',
            unique_together=set([('plandeestudios', 'codigo')]),
        ),
        migrations.AlterUniqueTogether(
            name='listagrupo',
            unique_together=set([('curso', 'alumno')]),
        ),
        migrations.AlterUniqueTogether(
            name='horario',
            unique_together=set([('plandeestudios', 'materia', 'grupo', 'dia')]),
        ),
        migrations.AlterUniqueTogether(
            name='grupo',
            unique_together=set([('plandeestudios', 'materia', 'grupo')]),
        ),
        migrations.AlterUniqueTogether(
            name='alumno',
            unique_together=set([('plandeestudios', 'codigo')]),
        ),
    ]
