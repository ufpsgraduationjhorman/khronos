# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_auto_20171112_2144'),
    ]

    operations = [
        migrations.AlterField(
            model_name='alumno',
            name='pensum',
            field=models.CharField(blank=True, null=True, max_length=3),
        ),
    ]
