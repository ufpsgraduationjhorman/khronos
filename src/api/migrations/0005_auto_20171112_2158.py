# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0004_auto_20171112_2150'),
    ]

    operations = [
        migrations.RenameField(
            model_name='alumno',
            old_name='estahaciendo',
            new_name='estahaciendotrabajo',
        ),
    ]
