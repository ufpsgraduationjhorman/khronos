# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0006_auto_20171112_2159'),
    ]

    operations = [
        migrations.AlterField(
            model_name='materia',
            name='semestre',
            field=models.CharField(max_length=2, null=True, blank=True),
        ),
    ]
