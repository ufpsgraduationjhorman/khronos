# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0007_auto_20180207_1637'),
    ]

    operations = [
        migrations.AddField(
            model_name='materia',
            name='peso',
            field=models.SmallIntegerField(null=True, default=3),
        ),
    ]
