# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models
import datetime


class Alumno(models.Model):
    plandeestudios = models.CharField(max_length=3)
    codigo = models.CharField(max_length=4)
    documento = models.CharField(max_length=15, blank=True, null=True)
    tipodocumento = models.CharField(max_length=2, blank=True, null=True)
    nombres = models.CharField(max_length=100, blank=True, null=True)
    sexo = models.CharField(max_length=1, blank=True, null=True)
    seccional = models.CharField(max_length=2, blank=True, null=True)
    pensum = models.CharField(max_length=4, blank=True, null=True)
    correoelectronico = models.CharField(max_length=200, blank=True, null=True)
    estahaciendotrabajo = models.CharField(max_length=1, blank=True, null=True)
    creditoscursados = models.SmallIntegerField(blank=True, null=True)
    creditosaprobados = models.SmallIntegerField(blank=True, null=True)
    semestre = models.SmallIntegerField(blank=True, null=True)
    matriculado = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        db_table = 'alumno'
        unique_together = (('plandeestudios', 'codigo'),)


class Grupo(models.Model):
    plandeestudios = models.CharField(max_length=3)
    materia = models.CharField(max_length=4)
    grupo = models.CharField(max_length=2)
    seccional = models.CharField(max_length=2, blank=True, null=True)
    profesor = models.CharField(max_length=5, blank=True, null=True)
    maximoalumnos = models.SmallIntegerField(blank=True, null=True)
    totalalumnos = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        db_table = 'grupo'
        unique_together = (('plandeestudios', 'materia', 'grupo'),)


class Horario(models.Model):
    plandeestudios = models.CharField(max_length=3)
    materia = models.CharField(max_length=4)
    grupo = models.CharField(max_length=2)
    dia = models.CharField(max_length=12)
    salon = models.CharField(max_length=5, blank=True, null=True)
    seccional = models.CharField(max_length=2, blank=True, null=True)
    horainicial = models.TimeField(blank=True, null=True)   # HH:MI:SS
    horafinal = models.TimeField(blank=True, null=True)     # HH:MI:SS

    @property
    def get_horario_size(self):
        return (datetime.datetime.combine(datetime.date.min, self.horainicial) -\
        datetime.datetime.combine(datetime.date.min, self.horafinal))

    def __repr__(self):
        return '{self.plandeestudios} {self.materia} - {self.grupo} {self.dia} {self.horainicial}, {self.horafinal}'.format(self=self)

    def __str__(self):
        return '{self.plandeestudios} {self.materia} - {self.grupo} {self.dia} {self.horainicial}, {self.horafinal}'.format(self=self)

    class Meta:
        db_table = 'horario'
        unique_together = (('plandeestudios', 'materia', 'grupo', 'dia'),)


class Listagrupo(models.Model):
    curso = models.CharField(max_length=9)
    alumno = models.CharField(max_length=7)
    seccional = models.CharField(max_length=2, blank=True, null=True)

    class Meta:
        db_table = 'listagrupo'
        unique_together = (('curso', 'alumno'),)


class Materia(models.Model):
    plandeestudios = models.CharField(max_length=3)
    codigo = models.CharField(max_length=4)
    departamento = models.CharField(max_length=2, blank=True, null=True)
    nombre = models.CharField(max_length=60, blank=True, null=True)
    horasteorica = models.SmallIntegerField(blank=True, null=True)
    horaspractica = models.SmallIntegerField(blank=True, null=True)
    creditos = models.SmallIntegerField(blank=True, null=True)
    peso = models.SmallIntegerField(default=3, null=True)
    semestre = models.CharField(max_length=2, blank=True, null=True)

    class Meta:
        db_table = 'materia'
        unique_together = (('plandeestudios', 'codigo'),)


class Matricula(models.Model):
    plandeestudios = models.CharField(max_length=3)
    alumno = models.CharField(max_length=4)
    materia = models.CharField(max_length=4)
    plandeestudiosmatricula = models.CharField(max_length=3, blank=True, null=True)
    materiamatricula = models.CharField(max_length=4, blank=True, null=True)
    grupo = models.CharField(max_length=2, blank=True, null=True)
    seccional = models.CharField(max_length=2, blank=True, null=True)
    estado = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        db_table = 'matricula'
        unique_together = (('plandeestudios', 'alumno', 'materia'),)


class Profesor(models.Model):
    departamento = models.CharField(max_length=2)
    codigo = models.CharField(max_length=5)
    documento = models.CharField(max_length=15)
    tipodocumento = models.CharField(max_length=2, blank=True, null=True)
    nombres = models.CharField(max_length=100, blank=True, null=True)
    sexo = models.CharField(max_length=1, blank=True, null=True)
    vinculacion = models.CharField(max_length=3, blank=True, null=True)
    seccional = models.CharField(max_length=2, blank=True, null=True)
    correoelectronico = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        db_table = 'profesor'
        unique_together = (('departamento', 'codigo', 'documento'),)
