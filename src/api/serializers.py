from django.contrib.auth import get_user_model
from rest_framework import serializers
from apps.timetabling.models import *
from fcm_django.models import FCMDevice
from rest_framework.authtoken.models import Token
from .models import *

User = get_user_model()


class TokenSerializer(serializers.ModelSerializer):
    #user = UserSerializer()
    class Meta:
        model = Token
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    auth_token_app = serializers.SerializerMethodField()

    def get_auth_token_app(self, user):
        return user.auth_token.key

    class Meta:
        model = User
        exclude = ('password', 'groups', 'user_permissions', 'last_login', 'is_superuser',)


class MobileDevicesSerializer(serializers.ModelSerializer):
    class Meta:
        model = FCMDevice
        fields = ('id', 'registration_id', 'type')


class SemestreCalendarioAcademicoSerializer(serializers.ModelSerializer):
    class Meta:
        model = SemestreCalendarioAcademico
        fields = '__all__'


class ExamenCalendarioSerializer(serializers.ModelSerializer):
    semestre_calendario_academico = SemestreCalendarioAcademicoSerializer()

    class Meta:
        model = ExamenCalendario
        fields = '__all__'


class HorarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Horario
        fields = '__all__'


class FechaFestivaInactivaSerializer(serializers.ModelSerializer):
    class Meta:
        model = FechaFestivaInactiva
        fields = '__all__'

