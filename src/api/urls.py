from django.conf.urls import url, patterns, include
from rest_framework.routers import DefaultRouter
from rest_framework_nested import routers
from . import views


urlpatterns = [
    url(r'^auth-devices/$', views.custom_obtain_token),
    url(r'^auth-devices/(?P<registration_id>[0-9]+)/$', views.TokenDeviceDetailAPIView.as_view()),
]

router = routers.DefaultRouter()
router.register(r'usuarios', views.UsuariosViewSet, 'usuario')
router.register(r'calendario-examenes', views.CalendarioExamenesViewSet, 'calendario-examen')
router.register(r'calendario', views.CalendarioViewSet, 'calendario')
router.register(r'calendario-temporal', views.CalendarioTemporalViewSet, 'calendario-temporal')


router.register(r'horarios-grupo', views.HorariosGrupoViewSet, 'horarios-grupo')
router.register(r'fechas-disponibles-grupo', views.FechasDisponiblesGrupoViewSet, 'fechas-disponibles-grupo')
router.register(r'fechas-inactivas', views.FechasInactivasViewSet, 'fechas-inactivas')


#usuarios_router = routers.NestedDefaultRouter(router, r'usuarios', lookup='usuario')
#usuarios_router.register(r'calendario', views.CalendarioViewSet,
#                              base_name='usuario_calendario')

urlpatterns += router.urls
