from django.contrib.auth import get_user_model
from django.shortcuts import render
from django.views import generic
from django.db.models import Q
from rest_framework.views import (
    APIView,
    PermissionDenied,
)
from rest_framework import viewsets, generics, filters, status
from rest_framework.response import Response
from django.http import JsonResponse, Http404, HttpResponseBadRequest, HttpResponse
from rest_framework.permissions import IsAuthenticated
from .serializers import *
import datetime
import pytz
import unidecode
import json
from apps.timetabling import custom_model
from fcm_django.models import FCMDevice
from accounts.authentication import authenticate
from accounts.models import DirectorProgramaUFPS
from oauth2client import crypt
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from .models import (
    Grupo,
    Horario,
)
from utils import dates_handler
import sys, traceback

User = get_user_model()


class UsuariosViewSet(viewsets.ViewSet):
    """
    API endpoint para los Usuarios del sistema.
    """
    def list(self, request):
        queryset = User.objects.all()
        user_serializer = UserSerializer(queryset, many=True)
        return Response(user_serializer.data)

    def retrieve(self, request, pk):
        user = User.objects.get(user_id_google=pk)
        user_serializer = UserSerializer(user)
        return Response(user_serializer.data)


class MobileAppLoginAPIView(APIView):
    """
    API endpoint para los Dispositivos móviles registrados en la aplicación.
    """
    def get_object(self, registration_id):
        try:
            return FCMDevice.objects.get(registration_id=registration_id)
        except FCMDevice.DoesNotExist:
            return None

    def post(self, request, format=None):
        registration_id = request.data['registration_id']
        type_app = request.data['type']
        device = self.get_object(registration_id)
        rol_usuario = request.data['rol_usuario']
        id_token = request.data['id_token']
        try:
            auth_user = authenticate(id_token=id_token, rol_usuario=rol_usuario)
            if not auth_user:
                return Response({'operacion': 0, 'detalles': 'Ingreso fallido.'}, status=status.HTTP_400_BAD_REQUEST)
            if not device:
                device = FCMDevice(registration_id=registration_id, type=type_app, user_id=auth_user.id)
                device.save()
            token, created = Token.objects.get_or_create(
                user=auth_user
            )
            if not created:
                utc_now = datetime.datetime.now(tz=pytz.UTC)
                if token.created < utc_now - datetime.timedelta(hours=24):
                    token.delete()
                    Token.objects.create(user=auth_user)
            user_serializer = UserSerializer(auth_user)
        except crypt.AppIdentityError as error:
            return Response({'operacion': 0, 'detalles': str(error)}, status=status.HTTP_400_BAD_REQUEST)
        except (KeyError, ValueError) as error:
            return Response({'operacion': 0, 'detalles': 'Ingreso fallido ({0}).'.format(error)})
        except Exception:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            print("*** print_exception:")
            traceback.print_exception(exc_type, exc_value, exc_traceback,
                                      limit=2, file=sys.stdout)
            print(repr(traceback.format_tb(exc_traceback)))
            print("*** tb_lineno:", exc_traceback.tb_lineno)
            return Response(user_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(user_serializer.data)


class TokenDeviceDetailAPIView(APIView):
    """
    API endpoint para los Dispositivos móviles registrados en la aplicación.
    """
    def get_object(self, pk):
        try:
            return FCMDevice.objects.get(pk=pk)
        except FCMDevice.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        device = self.get_object(pk)
        devices_serializer = MobileDevicesSerializer(device)
        return Response(devices_serializer.data)

    def put(self, request, pk, format=None):
        device = self.get_object(pk)
        devices_serializer = MobileDevicesSerializer(device, data=request.data)
        if devices_serializer.is_valid():
            devices_serializer.save()
            return Response(devices_serializer.data)
        return Response(devices_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        device = self.get_object(pk)
        device.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class CalendarioExamenesViewSet(viewsets.ViewSet):
    """
    API endpoint para el Calendario de exámenes del usuario (Estudiante, Docente o Director).
    """
    def list(self, request):

        periodo_examenes = self.request.GET.get('periodo_examen', None)
        auth_token_app = self.request.GET.get('auth_token_app', None)
        fecha_presentacion = self.request.GET.get('fecha_presentacion', None)
        # Parámetro alternativo para filtrar exámenes por plan de estudio de los grupos.
        plan_estudios = self.request.GET.get('plan_estudios', None)
        try:
            user = Token.objects.get(key=auth_token_app).user
            if user.es_estudiante():
                codigos_plan = ('0' + self.request.user.plan_de_estudios, '1' + self.request.user.plan_de_estudios)
                cursos_matriculados = custom_model.get_cursos_estudiante(
                    plan_de_estudios=codigos_plan,
                    codigo=user.codigo,
                    plan_de_estudios_matricula=user.plan_de_estudios)

                if periodo_examenes:
                    periodo_examen = PeriodoTipoExamen.objects.get(tipo_examen=periodo_examenes)
                    curso_examenes = [ExamenCalendario.objects.filter(plan_de_estudios=curso['plandeestudiosmatricula'],
                                                                      codigo_materia=curso['materiamatricula'],
                                                                      grupo=curso['grupo'],
                                                                      tipo_examen=periodo_examen)
                                      for curso in cursos_matriculados]
                else:
                    curso_examenes = [ExamenCalendario.objects.filter(plan_de_estudios=curso['plandeestudiosmatricula'],
                                                                      codigo_materia=curso['materiamatricula'],
                                                                      grupo=curso['grupo'])
                                      for curso in cursos_matriculados]
                examenes = list()
                for examenes_cal in curso_examenes:
                    for examen in examenes_cal:
                        if fecha_presentacion and examen.fecha_presentacion.date().isoformat() != fecha_presentacion:
                            continue
                        examenes.append(examen)
            elif user.es_director():
                director = DirectorProgramaUFPS.objects.get(correo=user.correo)
                codigos_plan = ('0' + director.plan_de_estudios, '1' + director.plan_de_estudios)
                if periodo_examenes:
                    periodo_examen = PeriodoTipoExamen.objects.get(tipo_examen=periodo_examenes)
                    examenes = ExamenCalendario.objects.filter(Q(plan_de_estudios=codigos_plan[0]) |
                                                               Q(plan_de_estudios=codigos_plan[1]),
                                                               semestre_calendario_academico__activo=True,
                                                               tipo_examen=periodo_examen). \
                        order_by('fecha_presentacion')
                else:
                    examenes = ExamenCalendario.objects.filter(Q(plan_de_estudios=codigos_plan[0]) |
                                                               Q(plan_de_estudios=codigos_plan[1]),
                                                               semestre_calendario_academico__activo=True). \
                        order_by('fecha_presentacion')
                if fecha_presentacion:
                    fecha = datetime.datetime.strptime(fecha_presentacion, '%Y-%m-%d').date()
                    examenes = examenes.filter(fecha_presentacion__year=fecha.year,
                                               fecha_presentacion__month=fecha.month,
                                               fecha_presentacion__day=fecha.day)
            elif user.es_docente():
                # Si es docente, busco los grupos donde está el docente y saco las materias que sean de sistemas.
                if plan_estudios:
                    codigos_plan = ('0' + plan_estudios, '1' + plan_estudios)
                    cursos_matriculados = custom_model.get_cursos_profesor(
                        codigo=user.codigo,
                        plan_de_estudios_matricula=codigos_plan)
                else:
                    cursos_matriculados = custom_model.get_cursos_total_profesor(
                        departamento=user.departamento,
                        codigo=user.codigo)
                if periodo_examenes:
                    periodo_examen = PeriodoTipoExamen.objects.get(tipo_examen=periodo_examenes)
                    curso_examenes = [ExamenCalendario.objects.filter(plan_de_estudios=curso['plandeestudios'],
                                                                      codigo_materia=curso['codigo'],
                                                                      grupo=curso['grupo'],
                                                                      semestre_calendario_academico__activo=True,
                                                                      tipo_examen=periodo_examen)
                                      for curso in cursos_matriculados]
                else:
                    curso_examenes = [ExamenCalendario.objects.filter(plan_de_estudios=curso['plandeestudios'],
                                                                      semestre_calendario_academico__activo=True,
                                                                      codigo_materia=curso['codigo'],
                                                                      grupo=curso['grupo'])
                                      for curso in cursos_matriculados]
                examenes = list()
                for examenes_cal in curso_examenes:
                    for examen in examenes_cal:
                        if fecha_presentacion and examen.fecha_presentacion.date().isoformat() != fecha_presentacion:
                            continue
                        examenes.append(examen)
            else:
                # Si es docente, busco los grupos donde está el docente y saco las materias que sean de sistemas.
                examenes = []

            examenes_calendario_serializer = ExamenCalendarioSerializer(examenes, many=True)

            return Response(examenes_calendario_serializer.data)
        except User.DoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class CalendarioViewSet(viewsets.ViewSet):
    """
    API endpoint para el Calendario de exámenes del usuario (Estudiante, Docente o Director).
    """
    def list(self, request):

        # Parámetro alternativo para filtrar período de evaluaciones a consultar.
        periodo_examenes = self.request.GET.get('periodo_examen', None)
        # Parámetro alternativo para filtrar exámenes por semestre pénsum.
        semestre_pensum = self.request.GET.get('semestre_pensum', None)
        # Parámetro alternativo para filtrar exámenes por año.
        anio_calendario_academico = self.request.GET.get('anio_calendario_academico', None)
        # Parámetro alternativo para filtrar exámenes por semestre académico del año.
        semestre_calendario_academico = self.request.GET.get('semestre_calendario_academico', None)
        # Parámetro alternativo para filtrar exámenes por plan de estudio de los grupos.
        plan_estudios = self.request.GET.get('plan_estudios', None)

        if self.request.user.es_estudiante():
            codigos_plan = ('0' + self.request.user.plan_de_estudios, '1' + self.request.user.plan_de_estudios)
            cursos_matriculados = custom_model.get_cursos_estudiante(
                plan_de_estudios=codigos_plan,
                codigo=self.request.user.codigo,
                plan_de_estudios_matricula=self.request.user.plan_de_estudios)

            if periodo_examenes:
                periodo_examen = PeriodoTipoExamen.objects.get(tipo_examen=periodo_examenes)
                curso_examenes = [ExamenCalendario.objects.filter(plan_de_estudios=curso['plandeestudiosmatricula'],
                                                                  codigo_materia=curso['materiamatricula'],
                                                                  semestre_calendario_academico__activo=True,
                                                                  grupo=curso['grupo'],
                                                                  tipo_examen=periodo_examen)
                                  for curso in cursos_matriculados]
            else:
                curso_examenes = [ExamenCalendario.objects.filter(plan_de_estudios=curso['plandeestudiosmatricula'],
                                                                  codigo_materia=curso['materiamatricula'],
                                                                  semestre_calendario_academico__activo=True,
                                                                  grupo=curso['grupo'])
                                  for curso in cursos_matriculados]
            examenes = list()
            for examenes_cal in curso_examenes:
                for examen in examenes_cal:
                    examenes.append(examen)
        elif self.request.user.es_docente():
            if plan_estudios:
                codigos_plan = ('0' + plan_estudios, '1' + plan_estudios)
                cursos_matriculados = custom_model.get_cursos_profesor(
                    codigo=self.request.user.codigo,
                    plan_de_estudios_matricula=codigos_plan)
            else:
                cursos_matriculados = custom_model.get_cursos_total_profesor(
                    departamento=self.request.user.departamento,
                    codigo=self.request.user.codigo)
            if periodo_examenes:
                periodo_examen = PeriodoTipoExamen.objects.get(tipo_examen=periodo_examenes)
                curso_examenes = [ExamenCalendario.objects.filter(plan_de_estudios=curso['plandeestudios'],
                                                                  codigo_materia=curso['codigo'],
                                                                  grupo=curso['grupo'],
                                                                  semestre_calendario_academico__activo=True,
                                                                  tipo_examen=periodo_examen)
                                  for curso in cursos_matriculados]
            else:
                curso_examenes = [ExamenCalendario.objects.filter(plan_de_estudios=curso['plandeestudios'],
                                                                  semestre_calendario_academico__activo=True,
                                                                  codigo_materia=curso['codigo'],
                                                                  grupo=curso['grupo'])
                                  for curso in cursos_matriculados]
            examenes = list()
            for examenes_cal in curso_examenes:
                for examen in examenes_cal:
                    examenes.append(examen)
        elif self.request.user.es_director():
            director = DirectorProgramaUFPS.objects.get(correo=self.request.user.correo)
            codigos_plan = ('0' + director.plan_de_estudios, '1' + director.plan_de_estudios)
            if not anio_calendario_academico:
                examenes = ExamenCalendario.objects.filter(Q(plan_de_estudios=codigos_plan[0]) |
                                                           Q(plan_de_estudios=codigos_plan[1]),
                                                           semestre_calendario_academico__activo=True,
                                                           publicado=True). \
                order_by('fecha_presentacion')
            else:
                examenes = ExamenCalendario.objects.filter(Q(plan_de_estudios=codigos_plan[0]) |
                                                           Q(plan_de_estudios=codigos_plan[1]),
                                                           semestre_calendario_academico__anio=anio_calendario_academico,
                                                           publicado=True). \
                order_by('fecha_presentacion')
            if periodo_examenes:
                periodo_examen = PeriodoTipoExamen.objects.get(tipo_examen=periodo_examenes)
                examenes = examenes.filter(tipo_examen=periodo_examen)
            if semestre_pensum:
                examenes = examenes.filter(semestre_pensum=semestre_pensum)
            if semestre_calendario_academico:
                examenes = examenes.filter(semestre_calendario_academico__periodo=semestre_calendario_academico)

        else:
            # Si es docente, busco los grupos donde está el docente y saco las materias que sean de sistemas.
            examenes = []

        if examenes:
            examenes_calendario_serializer = ExamenCalendarioSerializer(examenes, many=True)
            return Response(examenes_calendario_serializer.data)

        return Response(status=status.HTTP_204_NO_CONTENT)


class CalendarioTemporalViewSet(viewsets.ViewSet):
    """
    API endpoint para el Calendario de exámenes del usuario (Estudiante, Docente o Director).
    """
    def list(self, request):
        # Parámetro alternativo para filtrar período de evaluaciones a consultar.
        periodo_examenes = self.request.GET.get('periodo_examen', None)
        # Parámetro alternativo para filtrar exámenes por semestre pénsum.
        semestre_pensum = self.request.GET.get('semestre_pensum', None)
        # Parámetro alternativo para filtrar exámenes por año.
        anio_calendario_academico = self.request.GET.get('anio_calendario_academico', None)
        # Parámetro alternativo para filtrar exámenes por semestre académico del año.
        semestre_calendario_academico = self.request.GET.get('semestre_calendario_academico', None)

        if not self.request.user.es_director():
            raise PermissionDenied

        codigos_plan = ('0' + self.request.user.plan_de_estudios, '1' + self.request.user.plan_de_estudios)
        if not anio_calendario_academico:
            examenes = ExamenCalendario.objects.filter(Q(plan_de_estudios=codigos_plan[0]) |
                                                       Q(plan_de_estudios=codigos_plan[1]),
                                                       semestre_calendario_academico__activo=True,
                                                       publicado=False). \
            order_by('fecha_presentacion')
        else:
            examenes = ExamenCalendario.objects.filter(Q(plan_de_estudios=codigos_plan[0]) |
                                                       Q(plan_de_estudios=codigos_plan[1]),
                                                       semestre_calendario_academico__anio=anio_calendario_academico,
                                                       publicado=False). \
            order_by('fecha_presentacion')
        if periodo_examenes:
            periodo_examen = PeriodoTipoExamen.objects.get(tipo_examen=periodo_examenes)
            examenes = examenes.filter(tipo_examen=periodo_examen)
        if semestre_pensum and int(semestre_pensum) != 0:
            examenes = examenes.filter(semestre_pensum=semestre_pensum)
        if semestre_calendario_academico:
            examenes = examenes.filter(semestre_calendario_academico__periodo=semestre_calendario_academico)
        if examenes:
            examenes_calendario_serializer = ExamenCalendarioSerializer(examenes, many=True)
            return Response(examenes_calendario_serializer.data)

        return Response(status=status.HTTP_204_NO_CONTENT)


class HorariosGrupoViewSet(viewsets.ViewSet):
    """
    API endpoint para el Calendario de exámenes del usuario (Estudiante, Docente o Director).
    """
    def list(self, request):
        # Parámetro alternativo para filtrar período de evaluaciones a consultar.
        plan_estudios = self.request.GET.get('plan_estudios', None)
        materia = self.request.GET.get('materia', None)
        grupo = self.request.GET.get('grupo', None)
        if not self.request.user.es_director():
            raise PermissionDenied
        if plan_estudios and materia and grupo:
            horarios = Horario.objects.filter(
                Q(dia='LUNES') | Q(dia='MARTES') | Q(dia='MIERCOLES') | Q(dia='JUEVES') | Q(dia='VIERNES') |
                Q(dia='SABADO') | Q(dia='DOMINGO'),
                plandeestudios=plan_estudios,
                materia=materia,
                grupo=grupo,
            )
            horario_serializer = HorarioSerializer(horarios, many=True)
            return Response(horario_serializer.data)

        return Response(status=status.HTTP_204_NO_CONTENT)


class FechasInactivasViewSet(viewsets.ViewSet):
    """
    API endpoint para el Calendario de exámenes del usuario (Estudiante, Docente o Director).
    """
    def list(self, request):
        fechas_inactivas = FechaFestivaInactiva.objects.filter(
            estado=True,
        )
        if fechas_inactivas:
            fechas_inactivas_serializer = FechaFestivaInactivaSerializer(fechas_inactivas, many=True)
            return Response(fechas_inactivas_serializer.data)
        return Response(status=status.HTTP_204_NO_CONTENT)


class FechasDisponiblesGrupoViewSet(viewsets.ViewSet):
    """
    API endpoint para el Calendario de exámenes del usuario (Estudiante, Docente o Director).
    """
    def list(self, request):
        # Parámetro alternativo para filtrar período de evaluaciones a consultar.
        plan_estudios = self.request.GET.get('plan_estudios', None)
        materia = self.request.GET.get('materia', None)
        grupo = self.request.GET.get('grupo', None)
        if not self.request.user.es_director():
            raise PermissionDenied
        if plan_estudios and materia and grupo:
            dias_semana = {'LUNES': 0, 'MARTES': 1, 'MIERCOLES': 2, 'JUEVES': 3, 'VIERNES': 4, 'SABADO': 5,
                           'DOMINGO': 6}
            horarios = Horario.objects.filter(
                Q(dia='LUNES') | Q(dia='MARTES') | Q(dia='MIERCOLES') | Q(dia='JUEVES') | Q(dia='VIERNES') |
                Q(dia='SABADO') | Q(dia='DOMINGO'),
                plandeestudios=plan_estudios,
                materia=materia,
                grupo=grupo,
            )
            semestre_periodo_temporal = SemestreCalendarioPeriodoExamen.objects.get(
                semestre_calendario_academico__activo=True,
                pendiente_temporal=True,
            )
            festivos = FechaFestivaInactiva.objects.filter(estado=True)
            rango_fechas = dates_handler.calcular_rango_fechas(fecha_inicio=semestre_periodo_temporal.fecha_inicio,
                                                               fecha_fin=semestre_periodo_temporal.fecha_fin,
                                                               fechas_ignorar=[festivo.fecha for festivo in festivos])

            dias_filtro = [dias_semana[unidecode.unidecode(horario.dia).upper()] for horario in horarios]
            fechasxdia_semana = dates_handler.set_dates_weekday_isoformat(rango_fechas=rango_fechas,
                                                                          dias_filtro=dias_filtro,
                                                                          isoformat=True)
            return JsonResponse(dates_handler.formatear_dict_fechas_dia(fechasxdia_semana), safe=False)

        return Response(status=status.HTTP_204_NO_CONTENT)


custom_obtain_token = MobileAppLoginAPIView.as_view()