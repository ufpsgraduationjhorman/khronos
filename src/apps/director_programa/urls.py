from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.IndexDirectorProgramaView.as_view(), name='index'),
    url(r'^calendario/$', views.CalendarioDirectorProgramaView.as_view(), name='calendario'),
    url(r'^calendario-temporal/$', views.CalendarioTemporalDirectorProgramaView.as_view(), name='calendario-temporal'),
    url(r'^generar-calendario/$', views.GenerarCalendarioView.as_view(), name='programacion-calendario'),
    url(r'^generar-calendario/crear/$', views.programar_calendario, name='programar-calendario'),
    url(r'^generar-calendario/guardar/$', views.guardar_nuevo_calendario, name='guardar-calendario'),
    url(r'^generar-calendario/actualizar-examen/$', views.actualizar_programacion_valida, name='actualizar-examen-valido'),

    #url(r'^notificar/$', views.notificar, name='notificar'),

    url(r'^programacion-grupo/$', views.ProgramacionGrupoView.as_view(), name='programacion-grupo'),
    url(r'^programacion-grupo/guardar/$', views.programar_grupo_individual, name='programacion-grupo-guardar'),

    url(r'^registro-semestre/$', views.RegistroSemestre.as_view(), name='registro-semestre'),
    url(r'^registro-semestre/crear/$', views.registrar_semestre, name='registro-semestre-crear'),
    url(r'^registro-semestre/actualizar/$', views.actualizar_estado_semestre, name='registro-semestre-actualizar'),

    url(r'^registro-docente/$', views.RegistroDocente.as_view(), name='registro-docente'),
    url(r'^registro-docente/actualizar-correo/$', views.actualizar_correo_docente, name='actualizar-correo-docente'),

    url(r'^registro-grupos/$', views.RegistroDocente.as_view(), name='registro-grupos'),

    url(r'^configuracion-calendario/$', views.ConfiguracionCalendarioView.as_view(), name='configuracion-calendario'),
    url(r'^configuracion-calendario/actualizar-periodo/$', views.actualizar_periodo, name='actualizar-periodo'),

    url(r'^calendario-festivo/$', views.CalendarioFestivoView.as_view(), name='festivos'),
    url(r'^calendario-festivo/restaurar/$', views.restaurar_festivo, name='restaurar-festivos'),
    url(r'^calendario-festivo/agregar/$', views.agregar_festivo, name='agregar-festivo'),
    url(r'^calendario-festivo/actualizar/$', views.actualizar_festivo, name='actualizar-festivo'),

    url(r'^configuracion-restricciones/$', views.ConfiguracionRestriccionesView.as_view(),
        name='configuracion-restricciones'),

    url(r'^actualizacion-datos/cargar-horarios/$', views.CargarHorariosView.as_view(),
       name='cargar-horarios'),

    url(r'^ponderacion-dificultad/$', views.DificultadAsignaturasView.as_view(),
       name='ponderacion-dificultad'),

    url(r'^ver-horarios/$', views.VerHorariosView.as_view(),
       name='ver-horarios'),
]
