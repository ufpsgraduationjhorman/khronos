from django.http import HttpResponseRedirect, HttpResponse, HttpResponseBadRequest, JsonResponse, Http404
from django.core.exceptions import PermissionDenied
from django.views import generic
from braces import views
from django.contrib.auth import get_user_model
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.template import loader
from django.utils.decorators import method_decorator
from fcm_django.models import FCMDevice
from apps.timetabling.models import (
    ExamenCalendario,
    #ExamenTemporalCalendario,
    FechaFestivaInactiva,
    PeriodoTipoExamen,
    SemestreCalendarioAcademico,
    SemestreCalendarioPeriodoExamen
)
from api.models import (
    Materia,
    Grupo,
    Horario,
    Profesor,
)
from django.db.models import Q
from apps.timetabling.timetabling import QueriesManager
from accounts.decorators import (
    login_required_ajax,
    login_required_director_programa
)
from apps.timetabling import custom_model
from schedulex import settings
from utils.algoritmo_genetico.evol_algs import ga
from utils.notifications import send_mobile_notifications, send_email_analisis_tiempo
from utils.validators import validate_date_es
from api import serializers
from zeep import Client
from collections import OrderedDict
from operator import (
    attrgetter
)
import datetime
import json
import unidecode
import sys, traceback
import time
import pandas as pd
import numpy as np


User = get_user_model()


class IndexDirectorProgramaView(views.LoginRequiredMixin, generic.TemplateView):
    template_name = 'director_programa/bienvenida_jefeplan.html'
    login_url = 'accounts:index'

    @method_decorator(login_required_director_programa())
    def get(self, request, *args, **kwargs):
        return self.render_to_response({'macromenu_actual': 'index'})


class CalendarioDirectorProgramaView(views.LoginRequiredMixin, generic.TemplateView):
    template_name = 'director_programa/calendario_jefeplan.html'
    login_url = 'accounts:index'

    @method_decorator(login_required_director_programa())
    def get(self, request, *args, **kwargs):
        semestres_academicos = SemestreCalendarioAcademico.objects.all()
        try:
            semestre_academico_activo = SemestreCalendarioAcademico.objects.get(activo=True)
        except SemestreCalendarioAcademico.DoesNotExist:
            semestre_academico_activo = None
        return self.render_to_response({'macromenu_actual': 'calendario',
                                        'semestres_academicos': semestres_academicos,
                                        'semestre_academico_activo': semestre_academico_activo})


class CalendarioTemporalDirectorProgramaView(views.LoginRequiredMixin, generic.TemplateView):
    template_name = 'director_programa/calendario_temporal_jefeplan.html'
    login_url = 'accounts:index'

    @method_decorator(login_required_director_programa())
    def get(self, request, *args, **kwargs):
        periodo_examen_semestre = None
        try:
            semestre_academico_activo = SemestreCalendarioAcademico.objects.get(activo=True)
            periodo_examen_semestre = SemestreCalendarioPeriodoExamen.objects.get(
                pendiente_temporal=True,
                semestre_calendario_academico=semestre_academico_activo
            )
        except SemestreCalendarioAcademico.DoesNotExist:
            semestre_academico_activo = None
        except SemestreCalendarioPeriodoExamen.DoesNotExist:
            periodo_examen_semestre = None
        return self.render_to_response({'macromenu_actual': 'calendario',
                                        'semestre_academico_activo': semestre_academico_activo,
                                        'periodo_examen_semestre': periodo_examen_semestre,})


class GenerarCalendarioView(views.LoginRequiredMixin, generic.TemplateView):
    template_name = 'director_programa/generar_calendario.html'
    login_url = 'accounts:index'

    @method_decorator(login_required_director_programa())
    def get(self, request, *args, **kwargs):
        semestres_grupos = dict()
        semestres_periodos = list()
        try:
            codigos_plan = ('0' + request.user.plan_de_estudios, '1' + request.user.plan_de_estudios)
            grupos = custom_model.get_cursos_activos(plan_de_estudios=codigos_plan)
            for grupo in grupos:
                grupo_semestre = int(grupo['semestre'])
                if grupo_semestre not in semestres_grupos.keys():
                    semestres_grupos[grupo_semestre] = list()
                semestres_grupos[grupo_semestre].append(grupo)
            semestres_grupos = OrderedDict(sorted(semestres_grupos.items()))
            semestre_academico_activo = SemestreCalendarioAcademico.objects.get(activo=True)
            semestres_periodos = SemestreCalendarioPeriodoExamen.objects.filter(
                semestre_calendario_academico=semestre_academico_activo
            ).order_by('periodo_tipo_examen')
        except SemestreCalendarioAcademico.DoesNotExist:
            semestre_academico_activo = None
        return self.render_to_response(
            {'macromenu_actual': 'programacion-calendario',
             'semestres_grupos': semestres_grupos,
             'semestre_academico_activo': semestre_academico_activo,
             'semestres_periodos': semestres_periodos}
        )


class ProgramacionGrupoView(views.LoginRequiredMixin, generic.TemplateView):
    template_name = 'director_programa/programacion_grupo_individual.html'
    login_url = 'accounts:index'

    @method_decorator(login_required_director_programa())
    def get(self, request, *args, **kwargs):
        semestres_calendarios_academicos = SemestreCalendarioAcademico.objects.all().order_by('-fecha_inicio')
        grupos = None
        try:
            codigos_plan = ('0' + request.user.plan_de_estudios, '1' + request.user.plan_de_estudios)
            grupos = custom_model.get_cursos_activos(plan_de_estudios=codigos_plan)
            semestre_academico_activo = SemestreCalendarioAcademico.objects.get(activo=True)
        except SemestreCalendarioAcademico.DoesNotExist:
            semestre_academico_activo = None
        return self.render_to_response({'macromenu_actual': 'programacion-calendario',
                                        'semestres_calendarios_academicos': semestres_calendarios_academicos,
                                        'grupos': grupos,
                                        'semestre_academico_activo': semestre_academico_activo,
                                        })


class RegistroSemestre(views.LoginRequiredMixin, generic.TemplateView):
    template_name = 'director_programa/registro_semestre.html'
    login_url = 'accounts:index'

    @method_decorator(login_required_director_programa())
    def get(self, request, *args, **kwargs):
        semestres_calendarios_academicos = SemestreCalendarioAcademico.objects.all().order_by('-fecha_inicio')
        semestre_academico_activo = semestres_calendarios_academicos.filter(activo=True)
        ultimo_semestre_academico = semestres_calendarios_academicos[0]
        return self.render_to_response({'macromenu_actual': 'registro-semestre',
                                        'semestres_calendarios_academicos': semestres_calendarios_academicos,
                                        'semestre_academico_activo': semestre_academico_activo,
                                        'ultimo_semestre_academico': ultimo_semestre_academico,
                                        })


class RegistroDocente(views.LoginRequiredMixin, generic.TemplateView):
    template_name = 'director_programa/registro_docente.html'
    login_url = 'accounts:index'

    @method_decorator(login_required_director_programa())
    def get(self, request, *args, **kwargs):
        profesores = Profesor.objects.all().order_by('nombres')
        return self.render_to_response({'macromenu_actual': 'registro-docente',
                                        'profesores': profesores,
                                        })


class ConfiguracionCalendarioView(views.LoginRequiredMixin, generic.TemplateView):
    template_name = 'director_programa/configuracion_calendario.html'
    login_url = 'accounts:index'

    @method_decorator(login_required_director_programa())
    def get(self, request, *args, **kwargs):
        primeros_previos_semestre = None
        segundos_previos_semestre = None
        examenes_finales_semestre = None
        examenes_ho_semestre = None
        semestre_academico_activo = None
        try:
            semestre_academico_activo = SemestreCalendarioAcademico.objects.get(activo=True)
            primeros_previos_semestre = SemestreCalendarioPeriodoExamen.objects.get(
                semestre_calendario_academico=semestre_academico_activo,
                periodo_tipo_examen=PeriodoTipoExamen.objects.get(tipo_examen=1)
            )
            segundos_previos_semestre = SemestreCalendarioPeriodoExamen.objects.get(
                semestre_calendario_academico=semestre_academico_activo,
                periodo_tipo_examen=PeriodoTipoExamen.objects.get(tipo_examen=2)
            )
            examenes_finales_semestre = SemestreCalendarioPeriodoExamen.objects.get(
                semestre_calendario_academico=semestre_academico_activo,
                periodo_tipo_examen=PeriodoTipoExamen.objects.get(tipo_examen=3)
            )
            examenes_ho_semestre = SemestreCalendarioPeriodoExamen.objects.get(
                semestre_calendario_academico=semestre_academico_activo,
                periodo_tipo_examen=PeriodoTipoExamen.objects.get(tipo_examen=4)
            )
        except (SemestreCalendarioPeriodoExamen.DoesNotExist, SemestreCalendarioAcademico.DoesNotExist) as error:
            pass
        return self.render_to_response({'macromenu_actual': 'configuracion-calendario',
                                        'primeros_previos': primeros_previos_semestre,
                                        'segundos_previos': segundos_previos_semestre,
                                        'examenes_finales': examenes_finales_semestre,
                                        'examenes_ho': examenes_ho_semestre,
                                        'semestre_academico_activo': semestre_academico_activo,
                                        })


class CalendarioFestivoView(views.LoginRequiredMixin, generic.TemplateView):
    template_name = 'director_programa/fechas_festivas_inactivas.html'
    login_url = 'accounts:index'

    @method_decorator(login_required_director_programa())
    def get(self, request, *args, **kwargs):
        calendario_festivos = FechaFestivaInactiva.objects.order_by('fecha')
        return self.render_to_response({'macromenu_actual': 'festivos', 'calendario_festivos': calendario_festivos})


class ConfiguracionRestriccionesView(views.LoginRequiredMixin, generic.TemplateView):
    template_name = 'director_programa/configuracion_restricciones.html'
    login_url = 'accounts:index'

    @method_decorator(login_required_director_programa())
    def get(self, request, *args, **kwargs):
        hard_constraints_tipo1 = ''
        soft_constraints_tipo1 = ''
        hard_constraints_tipo2 = ''
        soft_constraints_tipo2 = ''
        return self.render_to_response({'macromenu_actual': 'configuracion-restricciones',
                                        'hard_constraints_tipo1': hard_constraints_tipo1,
                                        'soft_constraints_tipo1': soft_constraints_tipo1,
                                        'hard_constraints_tipo2': hard_constraints_tipo2,
                                        'soft_constraints_tipo2': soft_constraints_tipo2})


class VerHorariosView(views.LoginRequiredMixin, generic.TemplateView):
    template_name = 'director_programa/horarios_actuales.html'
    login_url = 'accounts:index'

    @method_decorator(login_required_director_programa())
    def get(self, request, *args, **kwargs):
        materias_horarios = dict()
        codigos_plan = ('0' + request.user.plan_de_estudios, '1' + request.user.plan_de_estudios)
        horarios = custom_model.get_horarios_cursos_activos(plan_de_estudios=codigos_plan)
        for horario in horarios:
            materia = horario['semestre'] + '-' + horario['plandeestudios'] + horario['materia'] + horario['grupo']
            if materia not in materias_horarios.keys():
                materias_horarios[materia] = {
                    'codigo': horario['plandeestudios'] + horario['materia'],
                    'nombre': horario['nombre'],
                    'grupos': dict()
                }
            if horario['grupo'] not in materias_horarios[materia]['grupos'].keys():
                materias_horarios[materia]['grupos'][horario['grupo']] = list()
            materias_horarios[materia]['grupos'][horario['grupo']].append(horario)
        for materia in materias_horarios:
            materias_horarios[materia]['grupos'] = OrderedDict(sorted(materias_horarios[materia]['grupos'].items()))
        materias_horarios = OrderedDict(sorted(materias_horarios.items()))
        return self.render_to_response({'macromenu_actual': 'ver-horarios',
                                        'materias_horarios': materias_horarios
                                        })


class CargarHorariosView(views.LoginRequiredMixin, generic.TemplateView):
    template_name = 'director_programa/cargar_horarios.html'
    login_url = 'accounts:index'

    @method_decorator(login_required_director_programa())
    def get(self, request, *args, **kwargs):
        return self.render_to_response({'macromenu_actual': 'cargar-horarios',})

    @method_decorator(login_required_director_programa())
    def post(self, request, *args, **kwargs):
        archivo_horarios = request.FILES.get('archivo_horarios', None)
        xl = pd.ExcelFile(archivo_horarios)
        df1 = xl.parse(xl.sheet_names[0])
        nuevo_nombre_cols = ['materia', 'nombre', 'codigo_docente', 'nombre_docente', 'horario', 'num_matriculados']
        df1.columns = nuevo_nombre_cols

        # Se requiere cargar: docentes, materias, grupos_materias, horarios
        dict_anterior = dict()

        # Actualizar todos los grupos en 0 estudiantes
        Grupo.objects.update(totalalumnos=0)
        # Se eliminan todos los horarios actuales
        Horario.objects.all().delete()

        for row in df1.itertuples():
            materia_incluida = True
            dict_datos = dict()
            if not (isinstance(row.materia, str)) and np.isnan(row.materia):
                dict_datos = dict_anterior
                materia_incluida = False
            else:
                dict_datos['plan_estudios_materia'] = row.materia[:3]
                dict_datos['codigo_materia'] = row.materia[3:-1]
                dict_datos['nombre_materia'] = row.nombre
                dict_datos['grupo'] = row.materia[-1]
                dict_datos['semestre_materia'] = row.materia[4] if row.materia[4] != '0' else '10'
                dict_datos['num_matriculados'] = row.num_matriculados

            dict_datos['seccional'] = '1'

            if materia_incluida:
                if not (isinstance(row.codigo_docente, str)) and np.isnan(row.codigo_docente):
                    dict_datos['codigo_docente'] = ''
                    dict_datos['nombre_docente'] = ''
                else:
                    # Insertar/actualizar el docente
                    dict_datos['codigo_docente'] = str(int(row.codigo_docente))
                    dict_datos['nombre_docente'] = row.nombre_docente
                    profesor, created = Profesor.objects.update_or_create(
                        codigo=dict_datos['codigo_docente'],
                        defaults={
                            'departamento': '',
                            'documento': '',
                            'tipodocumento': 'CC',
                            'nombres': dict_datos['nombre_docente'],
                            'sexo': '',
                            'vinculacion': '',
                            'seccional': '1',
                        }
                    )

            if not (isinstance(row.horario, str)) and np.isnan(row.horario):
                dict_datos['dia_horario'] = ''
                dict_datos['salon_horario'] = ''
                dict_datos['hora_inicio'] = '00:00'
                dict_datos['hora_fin'] = '00:00'
            else:
                horas = row.horario[-17:-6]
                dict_datos['dia_horario'] = row.horario[:-18]
                dict_datos['salon_horario'] = row.horario[-5:]
                dict_datos['hora_inicio'] = horas.split('-')[0]
                dict_datos['hora_fin'] = horas.split('-')[1]

            # Insertar/actualizar la materia
            materia, created = Materia.objects.update_or_create(
                plandeestudios=dict_datos['plan_estudios_materia'],
                codigo=dict_datos['codigo_materia'],
                defaults={
                    'departamento': '',
                    'nombre': dict_datos['nombre_materia'],
                    'semestre': dict_datos['semestre_materia'],
                    'horasteorica': 0,
                    'horaspractica': 0,
                    'creditos': 0,

                }
            )

            # Insertar/actualizar grupo
            grupo, created = Grupo.objects.update_or_create(
                plandeestudios=dict_datos['plan_estudios_materia'],
                materia=dict_datos['codigo_materia'],
                grupo=dict_datos['grupo'],
                defaults={
                    'seccional': dict_datos['seccional'],
                    'profesor': dict_datos['codigo_docente'],
                    'maximoalumnos': 50,
                    'totalalumnos': dict_datos['num_matriculados'],
                }
            )

            # Insertar nuevo horario, habían 230
            horario_nuevo = Horario(
                plandeestudios=dict_datos['plan_estudios_materia'],
                materia=dict_datos['codigo_materia'],
                grupo=dict_datos['grupo'],
                dia=dict_datos['dia_horario'],
                salon=dict_datos['salon_horario'],
                seccional=dict_datos['seccional'],
                horainicial=dict_datos['hora_inicio'],
                horafinal=dict_datos['hora_fin']
            )
            horario_nuevo.save()

            dict_anterior = dict_datos

        return JsonResponse({'operacion': 1, 'detalles': 'Se han actualizado los horarios y carga académica del '
                                                         'semestre.'})


class DificultadAsignaturasView(views.LoginRequiredMixin, generic.TemplateView):
    template_name = 'director_programa/ponderacion_dificultad.html'
    login_url = 'accounts:index'

    @method_decorator(login_required_director_programa())
    def get(self, request, *args, **kwargs):
        codigos_plan = ('0' + request.user.plan_de_estudios, '1' + request.user.plan_de_estudios)
        materias = Materia.objects.filter(
            Q(plandeestudios=codigos_plan[0]) |
            Q(plandeestudios=codigos_plan[1]),
        ).order_by('semestre', 'plandeestudios', 'codigo')
        return self.render_to_response({'macromenu_actual': 'ponderacion-dificultad',
                                        'materias': materias,
                                        })

    @method_decorator(login_required_director_programa())
    def post(self, request, *args, **kwargs):
        materia_asignar_peso = request.POST.get('materia_asignar_peso', None)
        peso_dificultad = request.POST.get('peso_dificultad', None)
        if None in (materia_asignar_peso, peso_dificultad):
            return JsonResponse({'operacion': 0, 'detalles': 'Debe ingresar la información de la asignatura y el peso '
                                                             'para actualizar el nivel de dificultad.'})
        materia_asignar_peso = materia_asignar_peso.split('-')
        try:
            peso_dificultad = int(peso_dificultad)
            if peso_dificultad < 1 or peso_dificultad > 5:
                raise ValueError('El valor del peso no es válido, debe estar entre 1 y 5.')
            materia_buscar = Materia.objects.get(
                plandeestudios=materia_asignar_peso[0],
                codigo=materia_asignar_peso[1]
            )
            materia_buscar.peso = peso_dificultad
            materia_buscar.save()
        except Materia.DoesNotExist as error:
            return JsonResponse({'operacion': 0, 'detalles': 'La asignatura ingresada no se encuentra registrada.'})
        except ValueError as error:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            print("*** print_exception:")
            traceback.print_exception(exc_type, exc_value, exc_traceback,
                                      limit=2, file=sys.stdout)
            print(repr(traceback.format_tb(exc_traceback)))
            print("*** tb_lineno:", exc_traceback.tb_lineno)
            return JsonResponse({'operacion': 0, 'detalles': str(error)})
        except IndexError as error:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            print("*** print_exception:")
            traceback.print_exception(exc_type, exc_value, exc_traceback,
                                      limit=2, file=sys.stdout)
            print(repr(traceback.format_tb(exc_traceback)))
            print("*** tb_lineno:", exc_traceback.tb_lineno)
            return JsonResponse({'operacion': 0, 'detalles': str(error)})
        except (KeyError, TypeError, Exception):
            exc_type, exc_value, exc_traceback = sys.exc_info()
            print("*** print_exception:")
            traceback.print_exception(exc_type, exc_value, exc_traceback,
                                      limit=2, file=sys.stdout)
            print(repr(traceback.format_tb(exc_traceback)))
            print("*** tb_lineno:", exc_traceback.tb_lineno)
            return JsonResponse({'operacion': 0, 'detalles': 'Error durante la programación de exámenes.'})
        else:
            return JsonResponse({'operacion': 1,
                                 'detalles': 'Se ha actualizado el nivel de dificultad para la asignatura {0} - {1}'.
                                 format(materia_asignar_peso[0] + materia_asignar_peso[1], materia_asignar_peso[2])})


@login_required(login_url='accounts:index')
def restaurar_festivo(request):
    if request.method == 'POST':
        fecha_actual = datetime.datetime.now()
        client = Client(wsdl='https://www.kayaposoft.com/enrico/ws/v1.0/index.php?wsdl')
        result = client.service.getPublicHolidaysForYear(fecha_actual.year, 'col', '')
        if result and result['error'] is None:
            FechaFestivaInactiva.objects.all().delete()
            for fecha in result['publicHolidays']:
                FechaFestivaInactiva(fecha=datetime.datetime(fecha['date']['year'], fecha['date']['month'],
                                                             fecha['date']['day']),
                                     descripcion=fecha['localName']
                                     ).save()
        return HttpResponseRedirect(reverse('director-programa:festivos'))
    return HttpResponseBadRequest()


@login_required_ajax
def programar_calendario(request):
    """
    Programa el calendario de exámenes del período especificado con los grupos recibidos.
    # grupos_programar = [
    # {'semestre': 1, 'literal_grupo': 'A', 'plan_estudios': 115, 'codigo': '0101'},
    # {'semestre': 1, 'literal_grupo': 'B', 'plan_estudios': 115, 'codigo': '0101'},
    # {'semestre': 2, 'literal_grupo': 'A', 'plan_estudios': 115, 'codigo': '0201'}
    # ]
    :param request: petición del cliente.
    :return: JSON con resultado de operación y con listado de exámenes programados temporalmente.
    """
    if request.method == 'POST':
        grupos_programar = json.loads(request.POST.get('grupos_programar', None))
        periodo_programar = request.POST.get('periodo_examenes_programar', None)
        try:
            tiempo_total = 0
            if grupos_programar and periodo_programar:
                if periodo_programar not in ('1', '2', '3', '4'):
                    raise ValueError('Debe ingresar un tipo válido de exámenes a programar.')

                codigos_plan = ('0' + request.user.plan_de_estudios, '1' + request.user.plan_de_estudios)
                materias_peso = Materia.objects.filter(
                    Q(plandeestudios=codigos_plan[0]) |
                    Q(plandeestudios=codigos_plan[1]),
                    peso=0
                )
                if materias_peso:
                    return JsonResponse({'operacion': 0, 'detalles': 'Hay asignaturas sin ponderación de dificultad '
                                                                    'asignada (peso 0). Para continuar, '
                                                                    'debe ponderar todas las asignaturas primero en la '
                                                                    'opción "Ponderación de dificultad de '
                                                                    'asignaturas."',
                                        })

                periodo_programar = int(periodo_programar)
                if periodo_programar != 1:
                    periodo_examen_semestre = SemestreCalendarioPeriodoExamen.objects.get(
                        periodo_tipo_examen__tipo_examen=periodo_programar - 1,
                        semestre_calendario_academico__activo=True
                    )
                    if not periodo_examen_semestre.publicado:
                        raise ValueError('Debe programar los períodos de exámenes en orden. Período pendiente: {0}'.
                                         format(periodo_examen_semestre.periodo_tipo_examen.get_tipo_examen_display()))

                periodo_examen_semestre = SemestreCalendarioPeriodoExamen.objects.get(
                    periodo_tipo_examen__tipo_examen=periodo_programar,
                    semestre_calendario_academico__activo=True
                )

                semestres = set(grupo['semestre'] for grupo in grupos_programar)

                if len(semestres) > 0:
                    start_time = time.time()
                    semestres_grupos = {sem: [g for g in grupos_programar if g['semestre'] == sem]
                                        for sem in semestres}

                    dias_semana = {'LUNES': 0, 'MARTES': 1, 'MIERCOLES': 2, 'JUEVES': 3, 'VIERNES': 4, 'SABADO': 5,
                                   'DOMINGO': 6}

                    # Limpiamos todos los exámenes programados con anterioridad para el período indicado.
                    codigos_plan = ('0' + request.user.plan_de_estudios, '1' + request.user.plan_de_estudios)
                    ExamenCalendario.objects.filter(Q(plan_de_estudios=codigos_plan[0]) |
                                                    Q(plan_de_estudios=codigos_plan[1]),
                                                    tipo_examen=periodo_examen_semestre.periodo_tipo_examen,
                                                    semestre_calendario_academico__activo=True).delete()

                    fechas_inactivas = [fecha_inactiva.fecha for fecha_inactiva in FechaFestivaInactiva.objects
                        .filter(estado=True)]

                    for key_sem, grupos_sem in semestres_grupos.items():

                        query_materias = set(Materia.objects.get(plandeestudios=grupo_materia['plan_estudios'],
                                                                 codigo=grupo_materia['codigo'])
                                             for grupo_materia in grupos_sem)

                        materias = [{'plan_de_estudios': materia.plandeestudios, 'codigo': materia.codigo,
                                     'nombre': materia.nombre, 'creditos': materia.creditos,
                                     'peso': materia.peso} for materia in query_materias]

                        query_grupos = set(Grupo.objects.get(plandeestudios=grupo_materia['plan_estudios'],
                                                                materia=grupo_materia['codigo'],
                                                                grupo=grupo_materia['literal_grupo'])
                                           for grupo_materia in grupos_sem)

                        grupos = [
                            {'literal': grupo_materia.grupo, 'materia': grupo_materia.plandeestudios + grupo_materia.materia,
                             'horarios': [
                                 {
                                     'dia': dias_semana[unidecode.unidecode(horario.dia).upper()],
                                     'hora_inicio': horario.horainicial, 'hora_fin': horario.horafinal,
                                     'salon': horario.salon,
                                 }
                                 for horario in Horario.objects.filter(plandeestudios=grupo_materia.plandeestudios,
                                                                       materia=grupo_materia.materia,
                                                                       grupo=grupo_materia.grupo)
                                 ]
                             } for grupo_materia in query_grupos]

                        tipo_examenes = 1 if periodo_programar in (1, 2, 3) else 2
                        print(tipo_examenes)
                        nuevo_ag = ga.GeneticAlgorithm(max_population=50, crossover_rate=0, mutation_rate=0.05,
                                                       max_generations=20, BEST_FITNESS=0,
                                                       start_date=periodo_examen_semestre.fecha_inicio,
                                                       end_date=periodo_examen_semestre.fecha_fin,
                                                       tipo_examenes=tipo_examenes)

                        nuevo_ag.set_fechas_inactivas(fechas_inactivas)
                        nuevo_ag.set_materias(materias)
                        nuevo_ag.set_grupos(grupos)
                        calendario_semestre = nuevo_ag.programar_calendario()
                        semestre_academico_activo = SemestreCalendarioAcademico.objects.get(activo=True)

                        for examen in calendario_semestre:
                            examen_nuevo = ExamenCalendario(
                                hora_inicio=examen['hora_inicio'],
                                hora_fin=examen['hora_fin'],
                                fecha_presentacion=datetime.datetime.combine(
                                    datetime.datetime.strptime(examen['fecha'], '%Y-%m-%d').date(),
                                    datetime.datetime.strptime(examen['hora_inicio'], '%H:%M:%S').time()),
                                fecha_finalizacion=datetime.datetime.combine(
                                    datetime.datetime.strptime(examen['fecha'], '%Y-%m-%d').date(),
                                    datetime.datetime.strptime(examen['hora_fin'], '%H:%M:%S').time()),
                                dia=datetime.datetime.strptime(examen['fecha'], '%Y-%m-%d').date().weekday(),
                                salon=examen['salon'] if tipo_examenes == 1 else '',
                                seccional='',
                                tipo_examen=periodo_examen_semestre.periodo_tipo_examen,
                                plan_de_estudios=examen['plan_de_estudios'],
                                codigo_materia=examen['codigo_materia'],
                                departamento_materia='',
                                nombre_materia=examen['nombre'],
                                grupo=examen['literal_grupo'],
                                semestre_pensum=key_sem,
                                semestre_calendario_academico=semestre_academico_activo
                            )
                            examen_nuevo.save()
                    periodo_examen_semestre.pendiente_temporal = True
                    periodo_examen_semestre.save()
                    end_time = time.time()
                    tiempo_total = end_time - start_time
                    print('Tiempo total:', tiempo_total)
                else:
                    raise ValueError('Debe seleccionar por lo menos un grupo a programar.')
            else:
                raise ValueError('Debe seleccionar por lo menos un grupo y el tipo de exámenes a programar.')

        except SemestreCalendarioAcademico.DoesNotExist as error:
            return JsonResponse({'operacion': 0, 'detalles': 'No se encuentra semestre académico activo en '
                                                             'este momento.'})
        except SemestreCalendarioPeriodoExamen.DoesNotExist as error:
            return JsonResponse({'operacion': 0, 'detalles': 'No se encuentra registrado el período de exámenes '
                                                             'seleccionado para el semestre en curso.'})
        except ValueError as error:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            print("*** print_exception:")
            traceback.print_exception(exc_type, exc_value, exc_traceback,
                                      limit=2, file=sys.stdout)
            print(repr(traceback.format_tb(exc_traceback)))
            print("*** tb_lineno:", exc_traceback.tb_lineno)
            return JsonResponse({'operacion': 0, 'detalles': str(error)})
        except IndexError as error:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            print("*** print_exception:")
            traceback.print_exception(exc_type, exc_value, exc_traceback,
                                      limit=2, file=sys.stdout)
            print(repr(traceback.format_tb(exc_traceback)))
            print("*** tb_lineno:", exc_traceback.tb_lineno)
            return JsonResponse({'operacion': 0, 'detalles': str(error)})
        except (KeyError, TypeError, Exception):
            exc_type, exc_value, exc_traceback = sys.exc_info()
            print("*** print_exception:")
            traceback.print_exception(exc_type, exc_value, exc_traceback,
                                      limit=2, file=sys.stdout)
            print(repr(traceback.format_tb(exc_traceback)))
            print("*** tb_lineno:", exc_traceback.tb_lineno)
            return JsonResponse({'operacion': 0, 'detalles': 'Error durante la programación de exámenes.'})
        else:
            return JsonResponse({'operacion': 1,
                                 'periodo_examenes_programar': periodo_examen_semestre.periodo_tipo_examen.tipo_examen,
                                 'detalles': 'Se ha programado el calendario de exámenes. Verifique las '
                                             'fechas para confirmar el calendario o descartarlo.'})
    return HttpResponseBadRequest(content='Operación inválida')


def notificar(request):
    # Se envían las notificaciones móviles de nuevo calendario programado.
    send_mobile_notifications(data_message={
        'title': 'Khronos UFPS: Calendario de exámenes',
        'body': 'Se ha generado el calendario de X período del semestre.',
        'detalles': 'Las fechas del calendario han sido actualizadas.'
    })
    return HttpResponse('notificando...')


@login_required_ajax
def guardar_nuevo_calendario(request):
    if request.method == 'POST':
        try:
            semestre_academico_activo = SemestreCalendarioAcademico.objects.get(activo=True)
            periodo_examen_semestre = SemestreCalendarioPeriodoExamen.objects.get(
                pendiente_temporal=True,
                semestre_calendario_academico=semestre_academico_activo
            )
            codigos_plan = ('0' + request.user.plan_de_estudios, '1' + request.user.plan_de_estudios)
            examenes_temporales = ExamenCalendario.objects.filter(Q(plan_de_estudios=codigos_plan[0]) |
                                                                  Q(plan_de_estudios=codigos_plan[1]),
                                                                  tipo_examen=periodo_examen_semestre.periodo_tipo_examen,
                                                                  publicado=False,
                                                                  semestre_calendario_academico__activo=True)

            # Actualizamos el estado de todos los exámenes programados temporalmente (publicado)
            # para el período indicado.
            examenes_temporales.update(publicado=True)

            # Se actualiza el estado del período de evaluaciones en el semestre correspondiente (publicado).
            # Ejemplo: Período Exámenes Finales en Semestre 2017-2 -> publicado = True.
            periodo_examen_semestre.pendiente_temporal = False
            periodo_examen_semestre.publicado = True
            periodo_examen_semestre.save()

            # Se envían las notificaciones móviles de nuevo calendario programado.
            send_mobile_notifications(data_message={
                'title': 'Khronos UFPS: Calendario de exámenes',
                'body': 'Se ha generado el calendario de {0} del semestre.'.format(
                    periodo_examen_semestre.periodo_tipo_examen.get_tipo_examen_display().lower()
                ),
                'detalles': '{0}: las fechas del calendario han sido actualizadas.'.format(
                    periodo_examen_semestre.periodo_tipo_examen.get_tipo_examen_display()
                )
            })

        except SemestreCalendarioPeriodoExamen.DoesNotExist as error:
            return JsonResponse({'operacion': 0, 'detalles': 'No hay exámenes temporales o pendientes por confirmar.'})
        except SemestreCalendarioAcademico.DoesNotExist as error:
            return JsonResponse({'operacion': 0, 'detalles': 'No se encuentra semestre académico activo en '
                                                             'este momento.'})
        except PeriodoTipoExamen.DoesNotExist as error:
            return JsonResponse({'operacion': 0, 'detalles': 'El tipo de examen ingresado no es válido.'})
        except ValueError as error:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            print("*** print_exception:")
            traceback.print_exception(exc_type, exc_value, exc_traceback,
                                      limit=2, file=sys.stdout)
            print(repr(traceback.format_tb(exc_traceback)))
            print("*** tb_lineno:", exc_traceback.tb_lineno)
            return JsonResponse({'operacion': 0, 'detalles': str(error)})
        except KeyError:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            print("*** print_exception:")
            traceback.print_exception(exc_type, exc_value, exc_traceback,
                                      limit=2, file=sys.stdout)
            print(repr(traceback.format_tb(exc_traceback)))
            print("*** tb_lineno:", exc_traceback.tb_lineno)
            return JsonResponse({'operacion': 0, 'detalles': 'Error durante la programación de exámenes.'})
        else:
            return JsonResponse({'operacion': 1, 'detalles': 'Calendario guardado para {0}'.
                                format(periodo_examen_semestre.periodo_tipo_examen.get_tipo_examen_display())})
    return HttpResponseBadRequest()


@login_required_ajax
def actualizar_programacion_valida(request):
    if request.method == 'POST':
        fecha_nueva_programar = request.POST.get('fecha_nueva', None)
        plan_estudios = request.POST.get('plan_estudios', None)
        materia = request.POST.get('materia', None)
        grupo = request.POST.get('grupo', None)
        try:
            if None in (fecha_nueva_programar, plan_estudios, materia, grupo):
                raise ValueError('Debe ingresar la fecha en la que desea programar el examen y los datos del grupo.')
            semestre_academico_activo = SemestreCalendarioAcademico.objects.filter(activo=True)
            if not semestre_academico_activo:
                raise ValueError('No se encuentra semestre académico activo en este momento. '
                                 'Debe registrar un nuevo semestre o activarlo en caso de ya estar creado.')
            periodo_examen_semestre = SemestreCalendarioPeriodoExamen.objects.get(
                pendiente_temporal=True,
                semestre_calendario_academico=semestre_academico_activo
            )
            fecha_nueva_programar = validate_date_es(fecha_nueva_programar, formato='%Y-%m-%d')
            if not fecha_nueva_programar:
                return JsonResponse({'operacion': 0, 'detalles': 'La fecha de programación tienen un formato '
                                                                 'inválido o está vacía.'})
            if fecha_nueva_programar < periodo_examen_semestre.fecha_inicio or fecha_nueva_programar > \
                    periodo_examen_semestre.fecha_fin:
                return JsonResponse({'operacion': 0, 'detalles': 'La fecha de programación está por fuera del rango '
                                                                 'de fechas del período.'})

            hora_inicial = datetime.time(0, 0)
            hora_final = datetime.time(0, 0)
            salon = ''
            seccional = ''
            horario_seleccionado_grupo = None
            if periodo_examen_semestre.periodo_tipo_examen.tipo_examen != 4:
                dias_semana = {0: 'LUNES', 1: 'MARTES', 2: 'MIERCOLES', 3: 'JUEVES', 4: 'VIERNES', 5: 'SABADO',
                               6: 'DOMINGO'}
                dia_examen = dias_semana[fecha_nueva_programar.weekday()]
                horarios_grupo = Horario.objects.filter(
                    Q(dia=dia_examen),
                    plandeestudios=plan_estudios,
                    materia=materia,
                    grupo=grupo,
                )
                if not horarios_grupo:
                    return JsonResponse({'operacion': 0, 'detalles': 'El grupo seleccionado no tiene horarios disponibles '
                                                                     'en el día a programar.'})
                horario_seleccionado_grupo = sorted(horarios_grupo, key=attrgetter('get_horario_size'))[0]
                hora_inicial = horario_seleccionado_grupo.horainicial
                hora_final = horario_seleccionado_grupo.horafinal
                salon = horario_seleccionado_grupo.salon
                seccional = horario_seleccionado_grupo.seccional
            try:
                fecha_hora_inicio = datetime.datetime.combine(
                    fecha_nueva_programar,
                    hora_inicial
                )
                fecha_hora_fin = datetime.datetime.combine(
                    fecha_nueva_programar,
                    hora_final
                )
            except ValueError:
                return JsonResponse({'operacion': 0, 'detalles': 'La fecha y hora de programación tienen un formato '
                                                                 'inválido o está vacía.'})
            grupo = Grupo.objects.get(plandeestudios=plan_estudios,
                                      materia=materia,
                                      grupo=grupo,)

            # Actualizo la tabla de exámenes con la fecha y horario nuevos
            examen = ExamenCalendario.objects.get(
                plan_de_estudios=grupo.plandeestudios,
                codigo_materia=grupo.materia,
                grupo=grupo.grupo,
                tipo_examen=periodo_examen_semestre.periodo_tipo_examen,
                semestre_calendario_academico=semestre_academico_activo
            )
            size_hora_actual_examen = (datetime.datetime.combine(datetime.date.min, examen.hora_inicio) -
                                       datetime.datetime.combine(datetime.date.min, examen.hora_fin))
            if horario_seleccionado_grupo.get_horario_size > size_hora_actual_examen:
                return JsonResponse({'operacion': 0, 'detalles': 'Debe seleccionar el horario más amplio disponible '
                                                                 'para el examen.'})

            examen.hora_inicio = fecha_hora_inicio.time()
            examen.hora_fin = fecha_hora_fin.time()
            examen.dia = fecha_nueva_programar.weekday()
            examen.fecha_presentacion = fecha_hora_inicio
            examen.fecha_finalizacion = fecha_hora_fin
            examen.salon = salon
            examen.seccional = seccional
            examen.save()

        except Grupo.DoesNotExist as error:
            return JsonResponse({'operacion': 0, 'detalles': 'El grupo ingresado no se encuentra registrado en el '
                                                             'sistema.'})
        except ValueError as error:
            return JsonResponse({'operacion': 0, 'detalles': str(error)})
        except KeyError:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            print("*** print_exception:")
            traceback.print_exception(exc_type, exc_value, exc_traceback,
                                      limit=2, file=sys.stdout)
            print(repr(traceback.format_tb(exc_traceback)))
            print("*** tb_lineno:", exc_traceback.tb_lineno)
            return JsonResponse({'operacion': 0, 'detalles': 'Error durante la actualización del examen.'})
        except Exception:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            print("*** print_exception:")
            traceback.print_exception(exc_type, exc_value, exc_traceback,
                                      limit=2, file=sys.stdout)
            print(repr(traceback.format_tb(exc_traceback)))
            print("*** tb_lineno:", exc_traceback.tb_lineno)
            return JsonResponse({'operacion': 0, 'detalles': 'Error durante la actualización del examen.'})
        else:
            return JsonResponse({'operacion': 1,
                                 'examen': serializers.ExamenCalendarioSerializer(examen).data,
                                 })
    return HttpResponseBadRequest()


@login_required_ajax
def programar_grupo_individual(request):
    if request.method == 'POST':
        grupo_programar = request.POST.get('grupo_programar', None)
        periodo_programar = request.POST.get('periodo_programar', None)
        fecha_programar = request.POST.get('fecha_programar', None)
        hora_inicio_programar = request.POST.get('hora_inicio_programar', None)
        hora_fin_programar = request.POST.get('hora_fin_programar', None)
        try:
            if None in (grupo_programar, periodo_programar, hora_inicio_programar, hora_fin_programar):
                raise ValueError('Debe ingresar los datos del grupo y período a programar.')
            semestre_academico_activo = SemestreCalendarioAcademico.objects.filter(activo=True)
            if not semestre_academico_activo:
                raise ValueError('No se encuentra semestre académico activo en este momento. '
                                 'Debe registrar un nuevo semestre o activarlo en caso de ya estar creado.')

            periodo_programar = int(periodo_programar)
            tipo_examenes = 1 if periodo_programar in (1, 2, 3) else 2
            # if tipo_examenes == 1:
            try:
                hora_inicio_programar = datetime.datetime.strptime(hora_inicio_programar, '%H:%M').time()
                hora_fin_programar = datetime.datetime.strptime(hora_fin_programar, '%H:%M').time()
            except ValueError:
                return JsonResponse({'operacion': 0, 'detalles': 'La hora de programación tiene un formato '
                                                                 'inválido o está vacía.'})
            try:
                fecha_hora_inicio = datetime.datetime.combine(
                    validate_date_es(fecha_programar, formato='%Y-%m-%d'),
                    hora_inicio_programar
                )
                fecha_hora_fin = datetime.datetime.combine(
                    validate_date_es(fecha_programar, formato='%Y-%m-%d'),
                    hora_fin_programar
                )
            except ValueError:
                return JsonResponse({'operacion': 0, 'detalles': 'La fecha de programación tienen un formato '
                                                                 'inválido o está vacía.'})

            grupo_programar = grupo_programar.split('-')
            periodo_examen = PeriodoTipoExamen.objects.get(tipo_examen=periodo_programar)
            grupo = Grupo.objects.get(plandeestudios=grupo_programar[0],
                                      materia=grupo_programar[1],
                                      grupo=grupo_programar[2],)
            materia = Materia.objects.get(plandeestudios=grupo.plandeestudios, codigo=grupo.materia)

            examen_grupo, created = ExamenCalendario.objects.update_or_create(
                plan_de_estudios=grupo.plandeestudios,
                codigo_materia=grupo.materia,
                grupo=grupo.grupo,
                tipo_examen=periodo_examen,
                semestre_calendario_academico=semestre_academico_activo[0],
                publicado=True,
                defaults={
                    'hora_inicio': hora_inicio_programar,
                    'hora_fin': hora_fin_programar,
                    'fecha_presentacion': fecha_hora_inicio,
                    'fecha_finalizacion': fecha_hora_fin,
                    'salon': '',
                    'seccional': '',
                    'departamento_materia': materia.departamento,
                    'nombre_materia': materia.nombre,
                    'semestre_pensum': materia.semestre,
                })

        except Grupo.DoesNotExist as error:
            return JsonResponse({'operacion': 0, 'detalles': 'El grupo ingresado no se encuentra registrado en el sistema.'})
        except ValueError as error:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            print("*** print_exception:")
            traceback.print_exception(exc_type, exc_value, exc_traceback,
                                      limit=2, file=sys.stdout)
            print(repr(traceback.format_tb(exc_traceback)))
            print("*** tb_lineno:", exc_traceback.tb_lineno)
            return JsonResponse({'operacion': 0, 'detalles': str(error)})
        except (KeyError, TypeError, Exception):
            exc_type, exc_value, exc_traceback = sys.exc_info()
            print("*** print_exception:")
            traceback.print_exception(exc_type, exc_value, exc_traceback,
                                      limit=2, file=sys.stdout)
            print(repr(traceback.format_tb(exc_traceback)))
            print("*** tb_lineno:", exc_traceback.tb_lineno)
            return JsonResponse({'operacion': 0, 'detalles': 'Error durante la actualización del examen.'})
        else:
            return JsonResponse({'operacion': 1, 'detalles': '<br>'
                                                             '<br>Grupo: {0}{1}-{2} {3}'
                                                             '<br>'
                                                             '<br>Período: {7}'
                                                             '<br>'
                                                             '<br>Fecha: {4}'
                                                             '<br>Hora: {5}-{6}'.
                                format(examen_grupo.plan_de_estudios, examen_grupo.codigo_materia,
                                       examen_grupo.grupo, examen_grupo.nombre_materia,
                                       examen_grupo.fecha_presentacion.date().strftime('%d/%m/%Y'),
                                       examen_grupo.hora_inicio,
                                       examen_grupo.hora_fin,
                                       periodo_examen.get_tipo_examen_display(),)})
    return HttpResponseBadRequest()


@login_required_ajax
def registrar_semestre(request):
    if request.method == 'POST':
        fecha_inicio = request.POST.get('fecha_inicio', None)
        fecha_fin = request.POST.get('fecha_fin', None)
        periodo_semestre = request.POST.get('periodo_semestre', None)
        try:
            if None in (fecha_inicio, fecha_fin, periodo_semestre):
                raise ValueError('Debe ingresar una fecha de inicio y fecha de fin del semestre.')
            fecha_inicio_val = validate_date_es(fecha_inicio)
            if not fecha_inicio_val:
                raise ValueError('La fecha de inicio tiene un formato inválido o está vacía.')
            fecha_fin_val = validate_date_es(fecha_fin)
            if not fecha_fin_val:
                raise ValueError('La fecha de fin tiene un formato inválido o está vacía.')
            if fecha_fin <= fecha_inicio:
                raise ValueError('La fecha de fin de semestre debe ser mayor a la fecha de inicio.')
            semestre_academico_activo = SemestreCalendarioAcademico.objects.filter(activo=True)
            if semestre_academico_activo:
                raise ValueError('Actualmente hay un semestre académico activo. Para poder registrar un nuevo semestre '
                                 'debe modificar la fecha de finalización del que está en curso o inactivarlo.')
            nuevo_semestre = SemestreCalendarioAcademico(periodo=periodo_semestre,
                                                         anio=fecha_inicio_val.year,
                                                         fecha_inicio=fecha_inicio_val,
                                                         fecha_fin=fecha_fin_val,
                                                         activo=True)
            nuevo_semestre.save()
            semestre_periodo = SemestreCalendarioPeriodoExamen(
                semestre_calendario_academico=nuevo_semestre,
                periodo_tipo_examen=PeriodoTipoExamen(tipo_examen=1)
            )
            semestre_periodo.save()
            semestre_periodo = SemestreCalendarioPeriodoExamen(
                semestre_calendario_academico=nuevo_semestre,
                periodo_tipo_examen=PeriodoTipoExamen(tipo_examen=2)
            )
            semestre_periodo.save()
            semestre_periodo = SemestreCalendarioPeriodoExamen(
                semestre_calendario_academico=nuevo_semestre,
                periodo_tipo_examen=PeriodoTipoExamen(tipo_examen=3)
            )
            semestre_periodo.save()
            semestre_periodo = SemestreCalendarioPeriodoExamen(
                semestre_calendario_academico=nuevo_semestre,
                periodo_tipo_examen=PeriodoTipoExamen(tipo_examen=4)
            )
            semestre_periodo.save()
            SemestreCalendarioPeriodoExamen.objects.filter(pendiente_temporal=True).update(pendiente_temporal=False)
            # Restaurar Festivos
            restaurar_festivo(request)
        except ValueError as error:
            return JsonResponse({'operacion': 0, 'detalles': str(error)})
        except KeyError:
            return JsonResponse({'operacion': 0, 'detalles': 'Error durante el registro del semestre.'})
        else:
            return JsonResponse({'operacion': 1,
                                 'detalles': 'Registrado nuevo semestre académico. Semestre activo: {0} - {1}.'
                                .format(nuevo_semestre.anio,
                                        nuevo_semestre.periodo)})
    return HttpResponseBadRequest()


@login_required_ajax
def actualizar_estado_semestre(request):
    if request.method == 'POST':
        try:
            ultimo_semestre_academico = SemestreCalendarioAcademico.objects.latest('fecha_inicio')
            if ultimo_semestre_academico.activo:
                ultimo_semestre_academico.activo = False
            else:
                ultimo_semestre_academico.activo = True
            ultimo_semestre_academico.save()
            # Restaurar Festivos
            restaurar_festivo(request)
        except SemestreCalendarioAcademico.DoesNotExist:
            return JsonResponse({'operacion': 0, 'detalles': 'El sistema no registra semestres académicos hasta la fecha.'})
        except ValueError as error:
            return JsonResponse({'operacion': 0, 'detalles': str(error)})
        except KeyError:
            return JsonResponse({'operacion': 0, 'detalles': 'Error durante el registro del semestre.'})
        else:
            return JsonResponse({'operacion': 1,
                                 'detalles': 'Semestre académico en curso {0}.'
                                .format('REACTIVADO' if ultimo_semestre_academico.activo else 'INACTIVADO'),
                                 'estado': ultimo_semestre_academico.activo})
    return HttpResponseBadRequest()


@login_required_ajax
def actualizar_periodo(request):
    if request.method == 'POST':
        fecha_inicio = request.POST.get('fecha_inicio', None)
        fecha_fin = request.POST.get('fecha_fin', None)
        tipo_examen = request.POST.get('tipo_examen', None)
        numero_resolucion = request.POST.get('numero_resolucion', None)
        fecha_resolucion = request.POST.get('fecha_resolucion', None)
        try:
            if None in (fecha_inicio, fecha_fin, tipo_examen):
                raise ValueError('Debe ingresar una fecha de inicio y fecha de fin del corte.')
            if None in (numero_resolucion, fecha_resolucion) or '' in (numero_resolucion, fecha_resolucion):
                raise ValueError('Debe ingresar el número y fecha de resolución del período.')
            fecha_inicio_val = validate_date_es(fecha_inicio)
            if not fecha_inicio_val:
                raise ValueError('La fecha de inicio tiene un formato inválido o está vacía.')
            fecha_fin_val = validate_date_es(fecha_fin)
            if not fecha_fin_val:
                raise ValueError('La fecha de fin tiene un formato inválido o está vacía.')
            if fecha_fin <= fecha_inicio:
                raise ValueError('La fecha de fin de corte debe ser mayor a la fecha de inicio.')
            if tipo_examen:
                semestre_academico_activo = SemestreCalendarioAcademico.objects.get(activo=True)
                if semestre_academico_activo.fecha_inicio > fecha_inicio_val:
                    raise ValueError('La fecha de inicio de corte debe ser mayor o igual a la fecha de inicio del '
                                     'semestre académico activo ({0}).'.format(semestre_academico_activo.fecha_inicio))
                if semestre_academico_activo.fecha_fin < fecha_fin_val:
                    raise ValueError('La fecha de fin de corte debe ser menor o igual a la fecha de fin del '
                                     'semestre académico activo ({0}).'.format(semestre_academico_activo.fecha_fin))
                periodo_examen = PeriodoTipoExamen.objects.get(tipo_examen=tipo_examen)
                semestre_periodo = SemestreCalendarioPeriodoExamen.objects.get(
                    semestre_calendario_academico=semestre_academico_activo,
                    periodo_tipo_examen=periodo_examen
                )
                semestre_periodo.fecha_inicio = fecha_inicio
                semestre_periodo.fecha_fin = fecha_fin
                semestre_periodo.numero_resolucion = numero_resolucion
                semestre_periodo.fecha_resolucion = fecha_resolucion
                semestre_periodo.save()
            else:
                raise ValueError('Debe ingresar todos los campos para actualizar el período correspondiente.')
        except SemestreCalendarioAcademico.DoesNotExist:
            return JsonResponse({'operacion': 0, 'detalles': 'No se encuentra activo ningún semestre académico.'})
        except PeriodoTipoExamen.DoesNotExist:
            return JsonResponse({'operacion': 0, 'detalles': 'El tipo de exámenes ingresado no se encuentra registrado.'})
        except SemestreCalendarioPeriodoExamen.DoesNotExist:
            return JsonResponse({'operacion': 0, 'detalles': 'El semestre en curso no registra períodos de evaluación '
                                                             'asociados.'})
        except ValueError as error:
            return JsonResponse({'operacion': 0, 'detalles': str(error)})
        except KeyError:
            return JsonResponse({'operacion': 0, 'detalles': 'Error durante la actualización de la fecha.'})
        else:
            return JsonResponse({'operacion': 1,
                                 'detalles': 'Fecha actualizada para {0} en el semestre activo ({1} - {2})'
                                .format(periodo_examen.get_tipo_examen_display(),
                                        semestre_academico_activo.anio,
                                        semestre_academico_activo.periodo)})
    return HttpResponseBadRequest()


@login_required_ajax
def actualizar_correo_docente(request):
    if request.method == 'POST':
        id_docente = request.POST.get('id_docente', None)
        correo_docente = request.POST.get('correo_docente', None)
        try:
            if None in (id_docente,):
                raise ValueError('Debe ingresar los datos del docente a actualizar')
            profesor = Profesor.objects.get(id=int(id_docente))
            profesor.correoelectronico = correo_docente
            profesor.save()
        except Profesor.DoesNotExist:
            return JsonResponse({'operacion': 0, 'detalles': 'No se encuentra registrado el docente ingresado.'})
        except ValueError as error:
            return JsonResponse({'operacion': 0, 'detalles': str(error)})
        except KeyError:
            return JsonResponse({'operacion': 0, 'detalles': 'Error durante la actualización del doccente.'})
        else:
            return JsonResponse({'operacion': 1,
                                 'detalles': 'Docente actualizado: {0}'
                                .format(profesor.nombres)})
    return HttpResponseBadRequest()


@login_required(login_url='accounts:index')
def agregar_festivo(request):
    if request.method == 'POST':
        fecha_nueva = request.POST.get('fechaNoHabil', None)
        descripcion = request.POST.get('descripcionFechaNoHabil', None)
        if None not in (fecha_nueva, descripcion):
            FechaFestivaInactiva(fecha=fecha_nueva,
                                 descripcion=descripcion
                                 ).save()
        return HttpResponseRedirect(reverse('director-programa:festivos'))
    return HttpResponseBadRequest()


@login_required_ajax
def actualizar_festivo(request):
    if request.method == 'POST':
        fecha = request.POST.get('fecha', None)
        try:
            if None not in tuple(fecha):
                fecha_actual = FechaFestivaInactiva.objects.get(fecha=fecha)
                estado = not fecha_actual.estado
                FechaFestivaInactiva.objects.filter(fecha=fecha).update(estado=estado)
        except (KeyError, ValueError):
            return JsonResponse({'operacion': 0, 'detalles': 'Error durante la actualización de la fecha.'})
        return JsonResponse({'operacion': 1, 'estado': 1 if estado else 0})

    return HttpResponseBadRequest()
