from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.IndexDocenteView.as_view(), name='index'),
    url(r'^cursos/$', views.CursosDocenteView.as_view(), name='cursos'),
]
