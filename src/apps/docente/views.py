from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.views import generic
from braces import views
from django.contrib.auth import get_user_model
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.template import loader
from fcm_django.models import FCMDevice
from apps.timetabling.models import ExamenCalendario
from apps.timetabling.timetabling import QueriesManager
from apps.timetabling import custom_model
from schedulex import settings


class IndexDocenteView(views.LoginRequiredMixin, generic.TemplateView):
    template_name = 'docente/inicio_docente.html'
    login_url = 'accounts:index'

    def get(self, request, *args, **kwargs):
        return self.render_to_response({'macromenu_actual': 'index'})


class CursosDocenteView(views.LoginRequiredMixin, generic.TemplateView):
    template_name = 'docente/mis_cursos.html'
    login_url = 'accounts:index'

    def get(self, request, *args, **kwargs):
        plan_estudios = self.request.GET.get('planEstudios', '115')
        cursos_matriculados = custom_model.get_cursos_profesor(
            #departamento=self.request.user.departamento,
            codigo=self.request.user.codigo,
            plan_de_estudios_matricula=plan_estudios)
        return self.render_to_response({'macromenu_actual': 'cursos',
                                        'cursos_matriculados': cursos_matriculados,
        })