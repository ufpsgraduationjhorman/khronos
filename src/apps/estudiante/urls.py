from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.IndexEstudianteView.as_view(), name='index'),
    url(r'^cursos/$', views.CursosEstudianteView.as_view(), name='cursos'),
    url(r'^notificaciones/$', views.NotificacionesEstudianteView.as_view(), name='notificaciones'),
]
