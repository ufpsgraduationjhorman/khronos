from django.http import HttpResponseRedirect, HttpResponse
from django.views import generic
from braces import views
from django.contrib.auth import get_user_model
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.template import loader
from fcm_django.models import FCMDevice
from apps.timetabling.models import ExamenCalendario
from apps.timetabling.timetabling import QueriesManager
from apps.timetabling import custom_model
from schedulex import settings


User = get_user_model()


class IndexEstudianteView(views.LoginRequiredMixin, generic.TemplateView):
    template_name = 'estudiante/inicio_estudiante.html'
    login_url = 'accounts:index'

    def get(self, request, *args, **kwargs):
        return self.render_to_response({'macromenu_actual': 'index'})


class CursosEstudianteView(views.LoginRequiredMixin, generic.TemplateView):
    template_name = 'estudiante/mis_cursos.html'
    login_url = 'accounts:index'

    def get(self, request, *args, **kwargs):
        cursos_matriculados = custom_model.get_cursos_estudiante(plan_de_estudios=request.user.plan_de_estudios,
                                                                 codigo=request.user.codigo)
        print(cursos_matriculados)
        return self.render_to_response({'macromenu_actual': 'cursos',
                                        'cursos_matriculados': cursos_matriculados,
        })


class NotificacionesEstudianteView(views.LoginRequiredMixin, generic.TemplateView):
    template_name = 'timetabling/mis_cursos.html'
    login_url = 'accounts:index'

    def get(self, request, *args, **kwargs):
        return self.render_to_response({})