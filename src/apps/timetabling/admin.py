from django.contrib import admin
from.models import ExamenCalendario

# Register your models here.


class ExamenCalendarioAdmin(admin.ModelAdmin):
    list_display = ('id', 'plan_de_estudios', 'codigo_materia', 'grupo', 'nombre_materia', 'tipo_examen')
    list_display_links = ('id', 'nombre_materia')
    list_filter = ['fecha_presentacion']

admin.site.register(ExamenCalendario, ExamenCalendarioAdmin)
