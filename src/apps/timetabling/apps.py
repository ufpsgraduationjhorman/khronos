from django.apps import AppConfig


class TimetablingConfig(AppConfig):
    name = 'apps.timetabling'
    verbose_name = 'Calendario de Exámenes'
