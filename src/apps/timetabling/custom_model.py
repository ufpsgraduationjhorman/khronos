from django.db import connections


def get_cursos_estudiante(**credentials):
    cursor = connections['default'].cursor()
    cursor.execute("SELECT mtr.plandeestudiosmatricula, mtr.materiamatricula, mtr.grupo, mat.nombre "
                   "FROM matricula mtr, materia mat, alumno al "
                   "WHERE al.plandeestudios IN %s AND al.plandeestudios = mtr.plandeestudios AND al.codigo = %s AND "
                   "al.codigo = mtr.alumno AND mat.codigo = mtr.materiamatricula AND "
                   "mtr.estado = '1'",
                   [credentials['plan_de_estudios'], credentials['codigo']])
    rows = cursor.fetchall()
    columns = [col[0] for col in cursor.description]
    cursor.close()
    return [dict(zip(columns, row)) for row in rows]


def get_cursos_profesor(**credentials):
    cursor = connections['default'].cursor()
    cursor.execute("SELECT mat.plandeestudios, mat.codigo, gp.grupo, mat.nombre "
                   "FROM grupo gp, materia mat, profesor pr "
                   "WHERE pr.codigo = %s AND pr.codigo = gp.profesor AND "
                   "gp.plandeestudios IN %s AND mat.plandeestudios = gp.plandeestudios AND mat.codigo = gp.materia",
                   [credentials['codigo'], credentials['plan_de_estudios_matricula']])
    rows = cursor.fetchall()
    columns = [col[0] for col in cursor.description]
    cursor.close()
    return [dict(zip(columns, row)) for row in rows]


def get_cursos_total_profesor(**credentials):
    cursor = connections['default'].cursor()
    cursor.execute("SELECT mat.plandeestudios, mat.codigo, gp.grupo, mat.nombre "
                   "FROM grupo gp, materia mat, profesor pr "
                   "WHERE pr.codigo = %s AND pr.codigo = gp.profesor AND "
                   "mat.codigo = gp.materia",
                   [credentials['codigo']])
    rows = cursor.fetchall()
    columns = [col[0] for col in cursor.description]
    cursor.close()
    return [dict(zip(columns, row)) for row in rows]


def get_cursos_activos(**credentials):
    cursor = connections['default'].cursor()
    cursor.execute("SELECT DISTINCT materia.plandeestudios, materia.codigo, gp.grupo, materia.nombre, materia.semestre "
                   "FROM grupo gp "
                   "INNER JOIN materia ON gp.plandeestudios = materia.plandeestudios "
                   "AND gp.materia = materia.codigo "
                   "INNER JOIN horario ON (gp.plandeestudios = horario.plandeestudios "
                   "AND gp.materia = horario.materia AND gp.grupo != 'Z' AND "
                   "horario.dia in ('LUNES', 'MARTES', 'MIERCOLES', 'JUEVES', 'VIERNES', 'SABADO', 'DOMINGO')) "
                   "WHERE gp.plandeestudios IN %s AND gp.totalalumnos > 0 "
                   "ORDER BY materia.plandeestudios, materia.codigo, materia.semestre",
                   [credentials['plan_de_estudios']])
    rows = cursor.fetchall()
    columns = [col[0] for col in cursor.description]
    cursor.close()
    return [dict(zip(columns, row)) for row in rows]


def get_horarios_cursos_activos(**credentials):
    cursor = connections['default'].cursor()
    cursor.execute("SELECT DISTINCT horario.plandeestudios, horario.materia, horario.grupo, "
                   "materia.nombre, materia.semestre, "
                   "horario.dia, horario.horainicial, horario.horafinal, horario.salon "
                   "FROM grupo gp "
                   "INNER JOIN materia ON (gp.plandeestudios = materia.plandeestudios "
                   "AND gp.materia = materia.codigo AND gp.grupo != 'Z') "
                   "INNER JOIN horario ON (materia.plandeestudios = horario.plandeestudios "
                   "AND materia.codigo = horario.materia AND "
                   "horario.dia in ('LUNES', 'MARTES', 'MIERCOLES', 'JUEVES', 'VIERNES', 'SABADO', 'DOMINGO')) "
                   "WHERE gp.plandeestudios in %s AND gp.totalalumnos > 0 "
                   "ORDER BY materia.semestre, plandeestudios, materia, grupo",
                   [credentials['plan_de_estudios']])
    rows = cursor.fetchall()
    columns = [col[0] for col in cursor.description]
    cursor.close()
    return [dict(zip(columns, row)) for row in rows]