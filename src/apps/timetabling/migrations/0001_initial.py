# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ExamenCalendario',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dia', models.IntegerField(choices=[(1, 'Lunes'), (2, 'Martes'), (3, 'Miércoles'), (4, 'Jueves'), (5, 'Viernes'), (6, 'Sábado'), (7, 'Domingo')])),
                ('hora_inicio', models.TimeField()),
                ('hora_fin', models.TimeField()),
                ('salon', models.CharField(max_length=5)),
                ('seccional', models.CharField(max_length=2)),
                ('tipo_examen', models.IntegerField()),
                ('plan_de_estudio', models.CharField(max_length=3)),
                ('codigo_materia', models.CharField(max_length=4)),
                ('departamento_materia', models.CharField(max_length=2)),
                ('nombre_materia', models.CharField(max_length=60)),
                ('grupo', models.CharField(max_length=2)),
                ('semestre', models.IntegerField()),
                ('anio', models.PositiveSmallIntegerField()),
                ('fecha_generacion', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='examencalendario',
            unique_together=set([('tipo_examen', 'plan_de_estudio', 'codigo_materia', 'grupo', 'semestre', 'anio')]),
        ),
    ]
