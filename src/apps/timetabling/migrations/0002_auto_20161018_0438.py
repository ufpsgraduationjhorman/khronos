# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='examencalendario',
            name='fecha_presentacion',
            field=models.DateField(default=datetime.datetime(2016, 10, 18, 4, 38, 44, 934135, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='examencalendario',
            name='fecha_generacion',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='examencalendario',
            name='tipo_examen',
            field=models.IntegerField(choices=[(1, 'P1'), (2, 'P2'), (3, 'EF'), (4, 'EH'), (5, 'EO')]),
        ),
    ]
