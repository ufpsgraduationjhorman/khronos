# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0002_auto_20161018_0438'),
    ]

    operations = [
        migrations.AlterField(
            model_name='examencalendario',
            name='fecha_presentacion',
            field=models.DateField(default=django.utils.timezone.now),
        ),
    ]
