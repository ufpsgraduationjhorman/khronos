# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0003_auto_20161018_0439'),
    ]

    operations = [
        migrations.RenameField(
            model_name='examencalendario',
            old_name='plan_de_estudio',
            new_name='plan_de_estudios',
        ),
        migrations.AlterUniqueTogether(
            name='examencalendario',
            unique_together=set([('tipo_examen', 'plan_de_estudios', 'codigo_materia', 'grupo', 'semestre', 'anio')]),
        ),
    ]
