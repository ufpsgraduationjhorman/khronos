# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0004_auto_20161018_2158'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='examencalendario',
            options={'verbose_name_plural': 'Exámenes Programados'},
        ),
        migrations.AlterField(
            model_name='examencalendario',
            name='anio',
            field=models.PositiveSmallIntegerField(verbose_name='Año'),
        ),
    ]
