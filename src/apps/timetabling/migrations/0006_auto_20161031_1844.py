# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0005_auto_20161020_1952'),
    ]

    operations = [
        migrations.AlterField(
            model_name='examencalendario',
            name='tipo_examen',
            field=models.IntegerField(choices=[(1, 'Primer previo'), (2, 'Segundo previo'), (3, 'Examen final'), (4, 'Examen de habilitación/opcional')]),
        ),
    ]
