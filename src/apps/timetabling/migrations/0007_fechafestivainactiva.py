# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0006_auto_20161031_1844'),
    ]

    operations = [
        migrations.CreateModel(
            name='FechaFestivaInactiva',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('fecha', models.DateField()),
                ('descripcion', models.CharField(max_length=100)),
                ('estado', models.BooleanField()),
            ],
        ),
    ]
