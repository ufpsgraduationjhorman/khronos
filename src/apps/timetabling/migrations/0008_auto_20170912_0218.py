# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0007_fechafestivainactiva'),
    ]

    operations = [
        migrations.CreateModel(
            name='PeriodoTipoExamen',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('fecha_inicio', models.DateField()),
                ('fecha_fin', models.DateField()),
                ('tipo_examen', models.IntegerField(choices=[(1, 'Primer previo'), (2, 'Segundo previo'), (3, 'Examen final'), (4, 'Examen de habilitación/opcional')])),
            ],
        ),
        migrations.AlterField(
            model_name='fechafestivainactiva',
            name='estado',
            field=models.BooleanField(default=True),
        ),
    ]
