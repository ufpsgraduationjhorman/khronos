# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0008_auto_20170912_0218'),
    ]

    operations = [
        migrations.AddField(
            model_name='periodotipoexamen',
            name='descripcion',
            field=models.CharField(max_length=50, default=''),
            preserve_default=False,
        ),
    ]
