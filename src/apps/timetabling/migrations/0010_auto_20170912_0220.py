# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0009_periodotipoexamen_descripcion'),
    ]

    operations = [
        migrations.AlterField(
            model_name='periodotipoexamen',
            name='fecha_fin',
            field=models.DateField(null=True),
        ),
        migrations.AlterField(
            model_name='periodotipoexamen',
            name='fecha_inicio',
            field=models.DateField(null=True),
        ),
    ]
