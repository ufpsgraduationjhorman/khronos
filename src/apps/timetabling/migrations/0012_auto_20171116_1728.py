# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0011_auto_20170914_0722'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExamenTemporalCalendario',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('dia', models.IntegerField(choices=[(0, 'Lunes'), (1, 'Martes'), (2, 'Miércoles'), (3, 'Jueves'), (4, 'Viernes'), (5, 'Sábado'), (6, 'Domingo')])),
                ('hora_inicio', models.TimeField()),
                ('hora_fin', models.TimeField()),
                ('fecha_presentacion', models.DateField(default=django.utils.timezone.now)),
                ('fecha_generacion', models.DateTimeField(auto_now=True)),
                ('salon', models.CharField(max_length=5)),
                ('seccional', models.CharField(max_length=2)),
                ('tipo_examen', models.IntegerField(choices=[(1, 'Primer previo'), (2, 'Segundo previo'), (3, 'Examen final'), (4, 'Examen de habilitación/opcional')])),
                ('plan_de_estudios', models.CharField(max_length=3)),
                ('codigo_materia', models.CharField(max_length=4)),
                ('departamento_materia', models.CharField(max_length=2)),
                ('nombre_materia', models.CharField(max_length=60)),
                ('grupo', models.CharField(max_length=2)),
                ('semestre', models.IntegerField()),
                ('anio', models.PositiveSmallIntegerField(verbose_name='Año')),
            ],
            options={
                'verbose_name_plural': 'Exámenes Programados',
            },
        ),
        migrations.AlterField(
            model_name='periodotipoexamen',
            name='tipo_examen',
            field=models.IntegerField(choices=[(1, 'Primeros previos'), (2, 'Segundos previos'), (3, 'Exámenes finales'), (4, 'Exámenes de habilitación/opcionales')]),
        ),
        migrations.AlterUniqueTogether(
            name='examentemporalcalendario',
            unique_together=set([('tipo_examen', 'plan_de_estudios', 'codigo_materia', 'grupo', 'semestre', 'anio')]),
        ),
    ]
