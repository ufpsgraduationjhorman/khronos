# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0012_auto_20171116_1728'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='examentemporalcalendario',
            options={'verbose_name_plural': 'Exámenes Temporales Programados'},
        ),
    ]
