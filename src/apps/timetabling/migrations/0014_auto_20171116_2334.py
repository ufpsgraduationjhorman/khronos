# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0013_auto_20171116_1743'),
    ]

    operations = [
        migrations.AlterField(
            model_name='examencalendario',
            name='dia',
            field=models.IntegerField(null=True, choices=[(0, 'Lunes'), (1, 'Martes'), (2, 'Miércoles'), (3, 'Jueves'), (4, 'Viernes'), (5, 'Sábado'), (6, 'Domingo')]),
        ),
        migrations.AlterField(
            model_name='examentemporalcalendario',
            name='dia',
            field=models.IntegerField(null=True, choices=[(0, 'Lunes'), (1, 'Martes'), (2, 'Miércoles'), (3, 'Jueves'), (4, 'Viernes'), (5, 'Sábado'), (6, 'Domingo')]),
        ),
    ]
