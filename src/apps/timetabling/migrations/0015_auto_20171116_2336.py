# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0014_auto_20171116_2334'),
    ]

    operations = [
        migrations.AlterField(
            model_name='examencalendario',
            name='anio',
            field=models.PositiveSmallIntegerField(verbose_name='Año', null=True),
        ),
        migrations.AlterField(
            model_name='examentemporalcalendario',
            name='anio',
            field=models.PositiveSmallIntegerField(verbose_name='Año', null=True),
        ),
    ]
