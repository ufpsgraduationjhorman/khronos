# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0015_auto_20171116_2336'),
    ]

    operations = [
        migrations.AlterField(
            model_name='examencalendario',
            name='salon',
            field=models.CharField(null=True, max_length=5),
        ),
        migrations.AlterField(
            model_name='examencalendario',
            name='seccional',
            field=models.CharField(null=True, max_length=2),
        ),
        migrations.AlterField(
            model_name='examentemporalcalendario',
            name='salon',
            field=models.CharField(null=True, max_length=5),
        ),
        migrations.AlterField(
            model_name='examentemporalcalendario',
            name='seccional',
            field=models.CharField(null=True, max_length=2),
        ),
    ]
