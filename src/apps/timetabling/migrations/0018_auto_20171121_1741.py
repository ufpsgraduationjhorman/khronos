# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0017_auto_20171121_1735'),
    ]

    operations = [
        migrations.AlterField(
            model_name='examentemporalcalendario',
            name='fecha_presentacion',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
