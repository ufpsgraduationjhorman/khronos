# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0019_auto_20171206_1852'),
    ]

    operations = [
        migrations.CreateModel(
            name='SemestreCalendarioAcademico',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('numero', models.SmallIntegerField()),
                ('anio', models.IntegerField()),
                ('descripcion', models.CharField(default='', max_length='200')),
            ],
        ),
        migrations.RenameField(
            model_name='examencalendario',
            old_name='semestre',
            new_name='semestre_pensum',
        ),
        migrations.RenameField(
            model_name='examentemporalcalendario',
            old_name='semestre',
            new_name='semestre_pensum',
        ),
        migrations.AlterUniqueTogether(
            name='examencalendario',
            unique_together=set([('tipo_examen', 'plan_de_estudios', 'codigo_materia', 'grupo', 'semestre_pensum', 'anio')]),
        ),
        migrations.AlterUniqueTogether(
            name='examentemporalcalendario',
            unique_together=set([('tipo_examen', 'plan_de_estudios', 'codigo_materia', 'grupo', 'semestre_pensum', 'anio')]),
        ),
    ]
