# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0020_auto_20171208_2339'),
    ]

    operations = [
        migrations.CreateModel(
            name='SemestreCalendarioPeriodoExamen',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_inicio', models.DateField(null=True)),
                ('fecha_fin', models.DateField(null=True)),
                ('estado', models.BooleanField(default=False)),
                ('publicado', models.BooleanField(default=False)),
            ],
        ),
        migrations.DeleteModel(
            name='ExamenTemporalCalendario',
        ),
        migrations.RenameField(
            model_name='semestrecalendarioacademico',
            old_name='numero',
            new_name='periodo',
        ),
        migrations.RemoveField(
            model_name='periodotipoexamen',
            name='fecha_fin',
        ),
        migrations.RemoveField(
            model_name='periodotipoexamen',
            name='fecha_inicio',
        ),
        migrations.AddField(
            model_name='examencalendario',
            name='publicado',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='examencalendario',
            name='semestre_academico',
            field=models.ForeignKey(to='timetabling.SemestreCalendarioAcademico', default=3),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='semestrecalendarioacademico',
            name='activo',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='examencalendario',
            name='tipo_examen',
            field=models.ForeignKey(to='timetabling.PeriodoTipoExamen'),
        ),
        migrations.AlterField(
            model_name='semestrecalendarioacademico',
            name='descripcion',
            field=models.CharField(max_length='200', default='', null=True),
        ),
        migrations.AddField(
            model_name='semestrecalendarioperiodoexamen',
            name='periodo_tipo_examen',
            field=models.OneToOneField(to='timetabling.PeriodoTipoExamen'),
        ),
        migrations.AddField(
            model_name='semestrecalendarioperiodoexamen',
            name='semestre_calendario',
            field=models.OneToOneField(to='timetabling.SemestreCalendarioAcademico'),
        ),
        migrations.AlterUniqueTogether(
            name='semestrecalendarioperiodoexamen',
            unique_together=set([('periodo_tipo_examen', 'semestre_calendario')]),
        ),
    ]
