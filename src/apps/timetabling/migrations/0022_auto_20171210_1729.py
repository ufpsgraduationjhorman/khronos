# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0021_auto_20171210_1716'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='periodotipoexamen',
            name='id',
        ),
        migrations.AlterField(
            model_name='periodotipoexamen',
            name='tipo_examen',
            field=models.IntegerField(serialize=False, primary_key=True, choices=[(1, 'Primeros previos'), (2, 'Segundos previos'), (3, 'Exámenes finales'), (4, 'Exámenes de habilitación/opcionales')]),
        ),
    ]
