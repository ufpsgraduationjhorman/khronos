# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0022_auto_20171210_1729'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='semestrecalendarioperiodoexamen',
            unique_together=set([]),
        ),
        migrations.RemoveField(
            model_name='semestrecalendarioperiodoexamen',
            name='periodo_tipo_examen',
        ),
        migrations.RemoveField(
            model_name='semestrecalendarioperiodoexamen',
            name='semestre_calendario',
        ),
        migrations.DeleteModel(
            name='SemestreCalendarioPeriodoExamen',
        ),
    ]
