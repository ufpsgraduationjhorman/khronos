# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0023_auto_20171210_1738'),
    ]

    operations = [
        migrations.CreateModel(
            name='SemestreCalendarioPeriodoExamen',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('fecha_inicio', models.DateField(null=True)),
                ('fecha_fin', models.DateField(null=True)),
                ('estado', models.BooleanField(default=False)),
                ('publicado', models.BooleanField(default=False)),
                ('periodo_tipo_examen', models.OneToOneField(to='timetabling.PeriodoTipoExamen')),
                ('semestre_calendario', models.OneToOneField(to='timetabling.SemestreCalendarioAcademico')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='semestrecalendarioperiodoexamen',
            unique_together=set([('periodo_tipo_examen', 'semestre_calendario')]),
        ),
    ]
