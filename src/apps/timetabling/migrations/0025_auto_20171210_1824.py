# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0024_auto_20171210_1744'),
    ]

    operations = [
        migrations.RenameField(
            model_name='examencalendario',
            old_name='semestre_academico',
            new_name='semestre_calendario_academico',
        ),
        migrations.RenameField(
            model_name='semestrecalendarioperiodoexamen',
            old_name='semestre_calendario',
            new_name='semestre_calendario_academico',
        ),
        migrations.AlterUniqueTogether(
            name='semestrecalendarioperiodoexamen',
            unique_together=set([('periodo_tipo_examen', 'semestre_calendario_academico')]),
        ),
    ]
