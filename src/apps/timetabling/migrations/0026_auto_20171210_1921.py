# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0025_auto_20171210_1824'),
    ]

    operations = [
        migrations.AlterField(
            model_name='semestrecalendarioperiodoexamen',
            name='fecha_fin',
            field=models.DateField(default=django.utils.timezone.now, null=True),
        ),
        migrations.AlterField(
            model_name='semestrecalendarioperiodoexamen',
            name='fecha_inicio',
            field=models.DateField(default=django.utils.timezone.now, null=True),
        ),
    ]
