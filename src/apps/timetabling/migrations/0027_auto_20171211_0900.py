# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0026_auto_20171210_1921'),
    ]

    operations = [
        migrations.AlterField(
            model_name='semestrecalendarioperiodoexamen',
            name='periodo_tipo_examen',
            field=models.ForeignKey(to='timetabling.PeriodoTipoExamen'),
        ),
        migrations.AlterField(
            model_name='semestrecalendarioperiodoexamen',
            name='semestre_calendario_academico',
            field=models.ForeignKey(to='timetabling.SemestreCalendarioAcademico'),
        ),
    ]
