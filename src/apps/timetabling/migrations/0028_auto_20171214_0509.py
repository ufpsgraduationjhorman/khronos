# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0027_auto_20171211_0900'),
    ]

    operations = [
        migrations.AddField(
            model_name='semestrecalendarioacademico',
            name='fecha_fin',
            field=models.DateField(null=True, default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name='semestrecalendarioacademico',
            name='fecha_inicio',
            field=models.DateField(null=True, default=django.utils.timezone.now),
        ),
    ]
