# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0028_auto_20171214_0509'),
    ]

    operations = [
        migrations.AddField(
            model_name='semestrecalendarioperiodoexamen',
            name='fecha_resolucion',
            field=models.DateField(default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name='semestrecalendarioperiodoexamen',
            name='numero_resolucion',
            field=models.CharField(null=True, max_length=10),
        ),
    ]
