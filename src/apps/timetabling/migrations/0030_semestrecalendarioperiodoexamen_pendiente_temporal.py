# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timetabling', '0029_auto_20180108_0612'),
    ]

    operations = [
        migrations.AddField(
            model_name='semestrecalendarioperiodoexamen',
            name='pendiente_temporal',
            field=models.BooleanField(default=False),
        ),
    ]
