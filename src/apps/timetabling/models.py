from django.db import models
from accounts.models import UsuarioUFPS
from django.utils import timezone


class SemestreCalendarioAcademico(models.Model):
    periodo = models.SmallIntegerField()
    anio = models.IntegerField()
    fecha_inicio = models.DateField(default=timezone.now, null=True)
    fecha_fin = models.DateField(default=timezone.now, null=True)
    descripcion = models.CharField(max_length='200', default='', null=True)
    activo = models.BooleanField(default=False)


class ExamenCalendario(models.Model):
    DIAS_SEMANA = (
        (0, 'Lunes'),
        (1, 'Martes'),
        (2, 'Miércoles'),
        (3, 'Jueves'),
        (4, 'Viernes'),
        (5, 'Sábado'),
        (6, 'Domingo'),
    )
    dia = models.IntegerField(choices=DIAS_SEMANA, null=True)
    hora_inicio = models.TimeField()
    hora_fin = models.TimeField()
    fecha_presentacion = models.DateTimeField(default=timezone.now)
    fecha_finalizacion = models.DateTimeField(default=timezone.now)
    fecha_generacion = models.DateTimeField(auto_now=True)
    salon = models.CharField(max_length=5, null=True)
    seccional = models.CharField(max_length=2, null=True)
    tipo_examen = models.ForeignKey('PeriodoTipoExamen')
    plan_de_estudios = models.CharField(max_length=3)
    codigo_materia = models.CharField(max_length=4)
    departamento_materia = models.CharField(max_length=2)
    nombre_materia = models.CharField(max_length=60)
    grupo = models.CharField(max_length=2)
    semestre_pensum = models.IntegerField()
    anio = models.PositiveSmallIntegerField('Año', null=True)
    semestre_calendario_academico = models.ForeignKey('SemestreCalendarioAcademico')
    publicado = models.BooleanField(default=False)

    def __repr__(self):
        return (self.plan_de_estudios + self.codigo_materia + '-' + self.grupo + ' ' + self.nombre_materia +
                ' ' + self.tipo_examen.get_tipo_examen_display() + ': ' + self.get_dia_display() if self.dia else '' +
                ' ' + self.fecha_presentacion.isoformat())

    def __str__(self):
        return (self.plan_de_estudios + self.codigo_materia + '-' + self.grupo + ' ' + self.nombre_materia +
                ' ' + self.get_tipo_examen_display() + ': ' + self.get_dia_display() if self.dia else '' + ' ' +
                self.fecha_presentacion.isoformat())

    class Meta:
        unique_together = ('tipo_examen', 'plan_de_estudios', 'codigo_materia', 'grupo', 'semestre_pensum', 'anio')
        verbose_name_plural = 'Exámenes Programados'


class FechaFestivaInactiva(models.Model):
    fecha = models.DateField()
    descripcion = models.CharField(max_length=100)
    estado = models.BooleanField(default=True)


class PeriodoTipoExamen(models.Model):
    TIPO_EXAMEN = (
        (1, 'Primeros previos'),
        (2, 'Segundos previos'),
        (3, 'Exámenes finales'),
        (4, 'Exámenes de habilitación/opcionales'),
    )
    tipo_examen = models.IntegerField(choices=TIPO_EXAMEN, primary_key=True)
    descripcion = models.CharField(max_length=50)


class SemestreCalendarioPeriodoExamen(models.Model):
    """
    Modelo intermedio de SemestreCalendarioAcademico y PeriodoTipoExamen.
    Guarda el estado y fechas de cada período de evaluaciones del semestre.
    """
    periodo_tipo_examen = models.ForeignKey('PeriodoTipoExamen')
    semestre_calendario_academico = models.ForeignKey('SemestreCalendarioAcademico')
    fecha_inicio = models.DateField(default=timezone.now, null=True)
    fecha_fin = models.DateField(default=timezone.now, null=True)
    estado = models.BooleanField(default=False)
    publicado = models.BooleanField(default=False)
    pendiente_temporal = models.BooleanField(default=False)
    numero_resolucion = models.CharField(null=True, max_length=10)
    fecha_resolucion = models.DateField(default=timezone.now)

    class Meta:
        unique_together = ('periodo_tipo_examen', 'semestre_calendario_academico',)