from .custom_model import get_cursos_estudiante, get_cursos_profesor


class QueriesManager:

    @staticmethod
    def get_courses_student(**kwargs):
        return get_cursos_estudiante(**kwargs)

    @staticmethod
    def get_courses_professor(**kwargs):
        return get_cursos_profesor(**kwargs)
