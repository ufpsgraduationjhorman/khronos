from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.IndexCalendarioView.as_view(), name='index'),
    url(r'^generar-reporte/$', views.generar_reporte_calendario, name='generar-reporte-calendario'),
]
