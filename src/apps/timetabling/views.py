from django.http import HttpResponseRedirect, HttpResponse, Http404, HttpResponseBadRequest
from django.views import generic
from braces import views
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from django.template.loader import get_template
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.template import loader
from fcm_django.models import FCMDevice
from .models import (
    ExamenCalendario,
    PeriodoTipoExamen,
    SemestreCalendarioAcademico,
    SemestreCalendarioPeriodoExamen,
)
from .timetabling import QueriesManager
from schedulex import settings
from weasyprint import HTML, CSS
import unidecode

# Create your views here.

User = get_user_model()


class IndexCalendarioView(views.LoginRequiredMixin, generic.TemplateView):
    login_url = 'accounts:index'

    def get(self, request, *args, **kwargs):
        if not request.user.es_activo():
            return HttpResponseRedirect(reverse('accounts:log-out'))
        usuario = request.user
        if usuario.es_estudiante():
            return HttpResponseRedirect(reverse('estudiante:index'))
        if usuario.es_docente():
            return HttpResponseRedirect(reverse('docente:index'))
        if usuario.es_director():
            return HttpResponseRedirect(reverse('director-programa:index'))
        # Sin rol
        return HttpResponseRedirect(reverse('accounts:log-out'))


@login_required(login_url='accounts:index')
def generar_reporte_calendario(request):
    if not request.user.is_active:
        return HttpResponseRedirect(reverse('accounts:index'))
    if request.method == 'GET':
        # Parámetro alternativo para filtrar período de evaluaciones a consultar.
        periodo_examenes = request.GET.get('periodo_examen', None)
        # Parámetro alternativo para filtrar exámenes por semestre pénsum.
        semestre_pensum = request.GET.get('semestre_pensum', None)
        # Parámetro alternativo para filtrar exámenes por año.
        anio_calendario_academico = request.GET.get('anio_calendario_academico', None)
        # Parámetro alternativo para filtrar exámenes por semestre académico del año.
        semestre_calendario_academico = request.GET.get('semestre_calendario_academico', None)
        # Parámetro alternativo para filtrar exámenes por semestre académico del año.
        tipo_reporte = request.GET.get('tipo_reporte', None)

        if tipo_reporte is None or tipo_reporte not in ('0', '1'):
            return HttpResponseBadRequest()
        try:
            periodo_examen_info = ''
            semestre_academico = ''
            codigos_plan = ('0' + request.user.plan_de_estudios, '1' + request.user.plan_de_estudios)

            if tipo_reporte == '1':     # Tipo de reporte 1 para los exámenes del semestre según filtro de búsqueda.
                if None in (anio_calendario_academico, semestre_calendario_academico):
                    raise Http404('Página no encontrada.')
                semestre_academico = SemestreCalendarioAcademico.objects.get(anio=anio_calendario_academico,
                                                                             periodo=semestre_calendario_academico)
                examenes = ExamenCalendario.objects.filter(Q(plan_de_estudios=codigos_plan[0]) |
                                                           Q(plan_de_estudios=codigos_plan[1]),
                                                           semestre_calendario_academico=semestre_academico,
                                                           publicado=True).\
                    order_by('codigo_materia', 'grupo')
                if periodo_examenes and periodo_examenes != '0':
                    periodo_examen_info = SemestreCalendarioPeriodoExamen.objects.get(
                        semestre_calendario_academico=semestre_academico,
                        periodo_tipo_examen__tipo_examen=periodo_examenes)
                    examenes = examenes.filter(tipo_examen=periodo_examen_info.periodo_tipo_examen.tipo_examen)
                if semestre_pensum and semestre_pensum != '0':
                    examenes = examenes.filter(semestre_pensum=semestre_pensum)
            else:   # Tipo de reporte 0 para todos los exámenes del semestre activo.
                examenes = ExamenCalendario.objects.filter(Q(plan_de_estudios=codigos_plan[0]) |
                                                           Q(plan_de_estudios=codigos_plan[1]),
                                                           semestre_calendario_academico__activo=True). \
                    order_by('fecha_presentacion')

            semestres = {examen.semestre_pensum: [] for examen in examenes}
            semestres_examenes = {semestre: examenes.filter(semestre_pensum=semestre) for semestre in semestres}

            reporte_body_template = get_template('reportes/director_programa/reporte_calendario.html')
            reporte_header_template = get_template('reportes/director_programa/reporte_calendario_header.html')

            context = {
                'entidad': ''
            }

            # Procesamos el template del header con el formato del reporte.
            rendered_reporte = reporte_header_template.render(context, request)
            header_layout = HTML(string=rendered_reporte, base_url=request.build_absolute_uri()).render()
            header_page = header_layout.pages[0]
            header_body = get_page_body(header_page._page_box.all_children())
            header_body = header_body.copy_with_children(header_body.all_children())

            context = {
                'periodo_examenes_reporte': periodo_examen_info,
                'semestre_academico': semestre_academico,
                'semestres_examenes': semestres_examenes,
            }

            # Procesamos el template con el formato del reporte.
            rendered_reporte = reporte_body_template.render(context, request)
            body_layout = HTML(string=rendered_reporte, base_url=request.build_absolute_uri()).render()  # .write_pdf()

            for i, page in enumerate(body_layout.pages):
                if i is None:
                    continue
                page_body = get_page_body(page._page_box.all_children())
                page_body.children += header_body.all_children()

            reporte_pdf = body_layout.write_pdf()
        except PeriodoTipoExamen.DoesNotExist:
            raise Http404('Página no encontrada.')
        else:
            # Enviamos archivo PDF como respuesta.
            response = HttpResponse(reporte_pdf, content_type='application/pdf')
            response['Content-Disposition'] = 'filename="calendario_{0}_{1}.pdf"'\
                .format(unidecode.unidecode(periodo_examen_info.periodo_tipo_examen.get_tipo_examen_display()),
                        unidecode.unidecode(str(semestre_academico.anio)))
            return response

    raise Http404('Página no encontrada.')


def get_page_body(boxes):
    for box in boxes:
        if box.element_tag == 'body':
            return box
        return get_page_body(box.all_children())