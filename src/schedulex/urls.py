"""schedulex URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

admin.site.site_header = 'Schedulex Admin'
admin.site.index_title = 'Administración de la aplicación'
admin.site.site_title = 'Administrador Schedulex'
admin.site.site_url = '/login'

urlpatterns = [
    # url(r'^grappelli/', include('grappelli.urls')),
    # url(r'^jet/', include('jet.urls', 'jet')),
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^api/v1.0/', include('api.urls', namespace='api')),

    url(r'', include('accounts.urls', namespace='accounts')),
    url(r'^calendario/', include('apps.timetabling.urls', namespace='timetabling')),
    url(r'^estudiante/', include('apps.estudiante.urls', namespace='estudiante')),
    url(r'^docente/', include('apps.docente.urls', namespace='docente')),
    url(r'^director-programa/', include('apps.director_programa.urls', namespace='director-programa')),
    #url(r'^docente/', include('apps.timetabling.urls', namespace='timetabling')),
]

urlpatterns += staticfiles_urlpatterns()
