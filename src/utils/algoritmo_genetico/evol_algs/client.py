"""
Cliente de ejemplo de ejecución del algoritmo de calendarización. Se muestra cómo deben ser pasados los parámetros
al algoritmo y la forma en que el calendario solución es devuelto.
La programación se realiza semestre por semestre de cada Programa Académico, para que el resultado sea óptimo y
correcto.
En este ejemplo se pasan materias de un mismo semestre y sus respectivos grupos asociados. No es necesario especificar
el número del semestre, pero las variables "materias" y "grupos" deben contener datos de un mismo semestre del pénsum.
Por ejemplo, si son diez (10) semestres de un Programa Académico, el algoritmo se ejecuta las mismas 10 veces.
Cada calendario obtenido puede definirse como un sub-calendario y corresponde a un semestre del pénsum.
Se tendrá que formar una respuesta completa con todos los sub-calendarios generados.
"""
import sys, os

path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
if not path in sys.path:
    sys.path.insert(1, path)
del path

from ga import GeneticAlgorithm
from evol_algs.utils import wsdl_holidays_client
import datetime

# El algoritmo de calendarización requiere obligatoriamente ser informado de las fechas de inicio y fin de cada
# período de evaluación y el tipo de exámenes.
# EXÁMENES TIPO 1: PRIMEROS PREVIOS, SEGUNDOS PREVIOS, EXÁMENES FINALES
# EXÁMENES TIPO 2: EXÁMENES DE HABILITACIÓN/OPCIONALES
ga = GeneticAlgorithm(start_date=datetime.date(2018, 3, 12), end_date=datetime.date(2018, 3, 24),
                      tipo_examenes=1)

# Las fechas inactivas son aquellas en las que no se podrán asignar exámenes. Por ejemplo, los días festivos del país
# o fechas personalizadas que se agreguen.
# Los días festivos de Colombia son leídos a través del web service Enrico Service
# https://kayaposoft.com/enrico
# https://github.com/jurajmajer/enrico
# Puede visualizar el módulo wsdl_holidays_client para conocer la configuración respectiva del servicio.
fechas_inactivas = [f.date() for f in wsdl_holidays_client.listar_fechas_simple()]

ga.set_fechas_inactivas(fechas_inactivas)

materias = [
    {'plan_de_estudios': '115', 'codigo': '0101', 'nombre': 'Cálculo Diferencial', 'creditos': 4, 'peso': 4},
    {'plan_de_estudios': '115', 'codigo': '0103', 'nombre': 'Mat. Discretas', 'creditos': 3, 'peso': 3},
    {'plan_de_estudios': '115', 'codigo': '0104', 'nombre': 'Fund. Programación', 'creditos': 3, 'peso': 6},
    {'plan_de_estudios': '115', 'codigo': '0105', 'nombre': 'Introd. Ing. Sistemas', 'creditos': 3, 'peso': 2},
    {'plan_de_estudios': '115', 'codigo': '0106', 'nombre': 'Comunicación I', 'creditos': 2, 'peso': 1},
]

dias_semana = {0: 'Lunes', 1: 'Martes', 2: 'Miércoles', 3: 'Jueves', 4: 'Viernes', 5: 'Sábado',
                           6: 'Domingo'}

grupos = [
    {'literal': 'A', 'materia': '1150101', 'horarios': [
        {'dia': 3, 'hora_inicio': datetime.time(10), 'hora_fin': datetime.time(13), 'salon': 'TE000'},
        {'dia': 4, 'hora_inicio': datetime.time(8), 'hora_fin': datetime.time(9), 'salon': 'TE000'},
    ]},
    {'literal': 'B', 'materia': '1150101', 'horarios': [
        {'dia': 1, 'hora_inicio': datetime.time(10), 'hora_fin': datetime.time(12), 'salon': 'TE000'},
        {'dia': 4, 'hora_inicio': datetime.time(6), 'hora_fin': datetime.time(7), 'salon': 'TE000'},
    ]},
    {'literal': 'C', 'materia': '1150101', 'horarios': [
        {'dia': 1, 'hora_inicio': datetime.time(10), 'hora_fin': datetime.time(12), 'salon': 'TE000'},
        {'dia': 4, 'hora_inicio': datetime.time(6), 'hora_fin': datetime.time(7), 'salon': 'TE000'},
    ]},
    {'literal': 'A', 'materia': '1150103', 'horarios': [
        {'dia': 0, 'hora_inicio': datetime.time(8), 'hora_fin': datetime.time(11), 'salon': 'TE000'},
    ]},
    {'literal': 'B', 'materia': '1150103', 'horarios': [
        {'dia': 2, 'hora_inicio': datetime.time(10), 'hora_fin': datetime.time(13), 'salon': 'TE000'},
        {'dia': 3, 'hora_inicio': datetime.time(8), 'hora_fin': datetime.time(10), 'salon': 'TE000'},
    ]},
    {'literal': 'C', 'materia': '1150103', 'horarios': [
        {'dia': 1, 'hora_inicio': datetime.time(12), 'hora_fin': datetime.time(14), 'salon': 'TE000'},
        {'dia': 4, 'hora_inicio': datetime.time(8), 'hora_fin': datetime.time(9), 'salon': 'TE000'},
    ]},
    {'literal': 'A', 'materia': '1150104', 'horarios': [
        {'dia': 0, 'hora_inicio': datetime.time(8), 'hora_fin': datetime.time(11), 'salon': 'TE000'},
    ]},
    {'literal': 'B', 'materia': '1150104', 'horarios': [
        {'dia': 2, 'hora_inicio': datetime.time(10), 'hora_fin': datetime.time(13), 'salon': 'TE000'},
        {'dia': 3, 'hora_inicio': datetime.time(8), 'hora_fin': datetime.time(10), 'salon': 'TE000'},
    ]},
    {'literal': 'C', 'materia': '1150104', 'horarios': [
        {'dia': 1, 'hora_inicio': datetime.time(12), 'hora_fin': datetime.time(14), 'salon': 'TE000'},
        {'dia': 4, 'hora_inicio': datetime.time(8), 'hora_fin': datetime.time(9), 'salon': 'TE000'},
    ]},
    {'literal': 'A', 'materia': '1150105', 'horarios': [
        {'dia': 0, 'hora_inicio': datetime.time(8), 'hora_fin': datetime.time(11), 'salon': 'TE000'},
    ]},
    {'literal': 'B', 'materia': '1150105', 'horarios': [
        {'dia': 2, 'hora_inicio': datetime.time(10), 'hora_fin': datetime.time(13), 'salon': 'TE000'},
        {'dia': 3, 'hora_inicio': datetime.time(8), 'hora_fin': datetime.time(10), 'salon': 'TE000'},
    ]},
    {'literal': 'C', 'materia': '1150105', 'horarios': [
        {'dia': 1, 'hora_inicio': datetime.time(12), 'hora_fin': datetime.time(14), 'salon': 'TE000'},
        {'dia': 4, 'hora_inicio': datetime.time(8), 'hora_fin': datetime.time(9), 'salon': 'TE000'},
    ]},
    {'literal': 'A', 'materia': '1150106', 'horarios': [
        {'dia': 0, 'hora_inicio': datetime.time(8), 'hora_fin': datetime.time(11), 'salon': 'TE000'},
    ]},
    {'literal': 'B', 'materia': '1150106', 'horarios': [
        {'dia': 2, 'hora_inicio': datetime.time(10), 'hora_fin': datetime.time(13), 'salon': 'TE000'},
        {'dia': 3, 'hora_inicio': datetime.time(8), 'hora_fin': datetime.time(10), 'salon': 'TE000'},
    ]},
    {'literal': 'C', 'materia': '1150106', 'horarios': [
        {'dia': 1, 'hora_inicio': datetime.time(12), 'hora_fin': datetime.time(14), 'salon': 'TE000'},
        {'dia': 4, 'hora_inicio': datetime.time(8), 'hora_fin': datetime.time(9), 'salon': 'TE000'},
    ]},
]

ga.set_materias(materias)
ga.set_grupos(grupos)
calendario = ga.programar_calendario()

# Generar HTML del Calendario resultante del semestre.
# Obtener
print(calendario)
print(ga.leer_calendario_json())
ga.leer_calendario_html()