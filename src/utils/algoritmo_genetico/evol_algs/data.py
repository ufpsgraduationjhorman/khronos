from operator import (
    attrgetter
)
import datetime


class SemestreAcademico:
    """
    Representa una instancia de ejecución del algoritmo de calendarización tipo I o II. Contiene los grupos de un
    semestre de pénsum académico.
    """
    def __init__(self, grupos, materias):
        self.grupos = grupos
        self.max_dificultad = sum([g.materia.creditos for g in self.grupos])
        self.materias = materias


class GrupoMateria:
    """
    Representa a los grupos tomados como datos de entrada para programar sus exámenes.
    """
    def __init__(self, literal, materia, horarios):
        self.literal = literal
        self.materia = materia
        self.horarios = sorted(horarios, key=attrgetter('size_timeslot'), reverse=True)
        self.examen_programado = False

    def get_widest_timeslot(self):
        return self.horarios[0]

    @property
    def cantidad_horarios_max(self):
        h_max = self.get_widest_timeslot()
        return len([h for h in self.horarios if h.size_timeslot == h_max.size_timeslot])

    @property
    def size_widest_timeslot(self):
        return self.get_widest_timeslot().size_timeslot

    def __str__(self):
        # return '{self.materia} - {self.literal} -> {self.horarios}'.format(self=self)
        return '{self.materia} - {self.literal} PesoDif: {self.materia.peso} '.format(self=self)


class Materia:
    """
    Representa las materias a las que está asociado cada uno de los grupos o cursos cuyos exámenes serán programados.
    """
    def __init__(self, plan_de_estudios, codigo, nombre, creditos, peso):
        self.plan_de_estudios = plan_de_estudios
        self.codigo = codigo
        self.codigo_completo = self.plan_de_estudios + self.codigo
        self.creditos = creditos
        self.nombre = nombre
        self.peso = peso

    def __str__(self):
        return '{self.codigo_completo}'.format(self=self)


class Horario:
    """
    Representa el horario de cada grupo o curso académico. Se identifican por el día de la semana, las horas de inicio
    y fin, y el salón en el cual se dicta la clase.
    """
    def __init__(self, dia, hora_inicio, hora_fin, salon='TE000'):
        self.dia = dia
        self.hora_inicio = hora_inicio
        self.hora_fin = hora_fin
        self.salon = salon
        self.semana = {0: 'Lunes', 1: 'Martes', 2: 'Miércoles', 3: 'Jueves', 4: 'Viernes', 5: 'Sábado', 6: 'Domingo'}
        self.size_timeslot = self.get_horario_size()

    def get_horario_size(self):
        return (datetime.datetime.combine(datetime.date.min, self.hora_fin) -
                datetime.datetime.combine(datetime.date.min, self.hora_inicio))

    def __repr__(self):
        dia = self.semana[self.dia]
        return 'Clase: {0} {self.hora_inicio}-{self.hora_fin} {self.salon}'.format(dia, self=self)

    def __str__(self):
        dia = self.semana[self.dia]
        return 'Clase: {0} {self.hora_inicio}-{self.hora_fin} {self.salon}'.format(dia, self=self)


class Examen:
    """
    Cada objeto de la clase Examen representa la unidad básica de información en el CalendarChromosome. En el ámbito de
    los Algoritmos Genéticos, es la analogía al Gen.
    """
    def __init__(self, grupo, fecha, horario):
        self.grupo = grupo
        self.fecha = fecha
        self.horario = horario

    def dificulty(self):
        return self.grupo.materia.peso

    def __repr__(self):
        return '{self.grupo} -> Examen: {self.fecha} {self.horario}'.format(self=self)

    def __str__(self):
        return '{self.grupo} -> {self.fecha} {self.horario}'.format(self=self)

    def __eq__(self, other):
        return self.grupo.materia.codigo_completo == other.grupo.materia.codigo_completo and \
               self.grupo.literal == other.grupo.literal


class ExamenTipoII:
    """
    Cada objeto de la clase Examen representa la unidad básica de información en el CalendarChromosome. En el ámbito de
    los Algoritmos Genéticos, es la analogía al Gen.
    """
    def __init__(self, materia, fecha, horario):
        self.materia = materia
        self.fecha = fecha
        self.horario = horario

    def dificulty(self):
        return self.materia.peso

    def __repr__(self):
        return '{self.materia} -> Examen: {self.fecha} {self.horario}'.format(self=self)

    def __str__(self):
        return '{self.materia} -> {self.fecha} {self.horario}'.format(self=self)
