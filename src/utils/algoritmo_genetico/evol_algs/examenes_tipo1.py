import sys, os

path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
if not path in sys.path:
    sys.path.insert(1, path)
del path

from operator import (
    itemgetter,
    attrgetter
)
from evol_algs.data import (
    Examen,
)
import math
import random
import datetime


class CalendarChromosomeTipoI:
    """
    Cada objeto de la clase CalendarioChromosome hace las veces de un Cromosoma o Individuo en la población
    (espacio de soluciones). Se le conoce como Fenotipo, pues está constituído por información genética.
    Este tipo de Calendario representa una solución para Exámenes Tipo I: exámenes previos y finales.
    """

    def __init__(self, rango_fechas=None, materias=None, fechas_inactivas=None):
        self.rango_fechas = rango_fechas
        self.examenes = list()
        self.semanas_examenes = list()
        self.materias = materias
        self.fechas_inactivas = fechas_inactivas
        self.violated_constraints = 0
        self.fitness = 0
        self.conflicto_SC1 = 0
        self.conflicto_SC2 = 0
        self.conflicto_SC3 = 0
        self.conflicto_SC4 = 0

    def set_dates_weekday(self):
        """
        Según el rango de fechas inicializado, agrupa las fechas que tienen el común el día de la semana al que
        corresponden.
        :return: dict con los días de la semana y su respectivo listado de fechas asociadas.
        """
        days = [day.dayofweek for day in self.rango_fechas]   # Días de la semana dentro del rango de fechas
        days_date_range = list(zip(self.rango_fechas, days))  # Días de la semana con cada fecha asociada
        dict_days = {day: '' for day in days}  # Diccionario que utiliza como clave cada día de la semana (de 0 a 6)

        for weekday in dict_days.keys():
            # Agrupamos las fechas que son del mismo día de la semana y lo agregamos al diccionario de días.
            dict_days[weekday] = [f.date() for f, d in days_date_range if d == weekday]
        return dict_days

    def inicializar(self, grupos):
        """
        Generamos un nuevo genotipo del individuo, es decir, la representación de una instancia de solución (su ADN).
        Cada gen está representado por una unidad de información, que para el caso equivale a cada examen del calendario
        creado. El cromosoma creado satisface todas las HC del problema, de tal forma que luego se empiece a optimizar
        la solución a partir de las SC.

        Hard Constraints (HC):

        1. Los exámenes previos y finales deben ser programados en alguno de sus horarios de clase asignados. OK
        2. No se pueden programar múltiples exámenes para un estudiante en el mismo horario. OK
        3. No se pueden programar exámenes en días NO hábiles.
        4. No se pueden programar múltiples exámenes para un docente en el mismo horario. OK
        5. Los exámenes se deben programar en un período de fechas limitado. OK
        6. Cada examen sólo puede ser programado una vez en cada corte de evaluación. OK
        7. No puede haber más de un exámen en el mismo salón de clase. OK
        8. Los exámenes para asignaturas del mismo semestre no deben cruzarse entre sí. OK
        9. Los exámenes deben ser programados en su horario más amplio siempre y cuando haya disponibilidad de días
        hábiles.

        :return:
        """
        dict_days = self.set_dates_weekday()    # Listado de días en rango de fechas de programación.
        for grupo in grupos:
            examen_programado = False  # Examen programado para el grupo.
            horarios_grupo = grupo.horarios
            horarios_disponibles = len(horarios_grupo)
            fechas_asociadas_horarios_g = []
            for h in horarios_grupo:
                fechas_asociadas_horarios_g += dict_days[h.dia] if h.dia in dict_days.keys() else []
            while horarios_disponibles > 0 and not examen_programado:
                # Se selecciona el horario más amplio del grupo.
                max_h = horarios_grupo[0]

                # Recrear lista de horarios excluyendo max_h.
                horarios_grupo = [h for h in horarios_grupo if h != max_h]
                fechas_dia = dict_days[max_h.dia] if max_h.dia in dict_days.keys() else []
                fechas_dia_disponibles = len(fechas_dia)

                # Caso especial: Programar examen en fecha válida de otros grupos de la MISMA materia.
                # Validar si hay otros grupos de la misma materia ya programados.
                examenes_misma_materia = [examen for examen in self.examenes if
                                          examen.grupo.materia.codigo == grupo.materia.codigo and
                                          examen.fecha in fechas_asociadas_horarios_g]

                # Buscamos el horario del grupo ACTUAL que corresponde al día de la semana que comparte con el
                # examen ya asignado para otro grupo de la MISMA materia.
                horario_dia_incidencia = [h for h in grupo.horarios
                                          if examenes_misma_materia and
                                          h.dia == examenes_misma_materia[0].horario.dia and
                                          h.size_timeslot == max_h.size_timeslot]
                if examenes_misma_materia and horario_dia_incidencia:
                    nuevo_examen = Examen(grupo, examenes_misma_materia[0].fecha, horario_dia_incidencia[0])
                    examen_programado = True
                    self.examenes.append(nuevo_examen)
                    grupo.examen_programado = True
                else:
                    # Caso especial: Programar examen en fecha válida en donde haya menos carga de exámenes.
                    # Se busca si hay más horarios del mismo "tamaño", no siempre el primer horario amplio que encuentre
                    # es la mejor opción (en caso de grupos con horarios de igual tamaño).
                    horarios_maximo_size = [h for h in grupo.horarios if h.size_timeslot == max_h.size_timeslot]
                    if len(horarios_maximo_size) > 1:  # Si encontró varios horarios del mismo "tamaño"...
                        fecha_menos_carga = None
                        peso_menor_examenes = -1
                        horario_seleccionado = None
                        horarios_max_disponibles = len(horarios_maximo_size)
                        while horarios_max_disponibles > 0:
                            h_max_size = horarios_maximo_size[random.randint(0, horarios_max_disponibles - 1)]
                            fechas_dia = dict_days[h_max_size.dia] if h_max_size.dia in dict_days.keys() else []
                            fechas_dia_disponibles = len(fechas_dia)
                            while fechas_dia_disponibles > 0:
                                # Seleccionar fecha de asignación del examen.
                                fecha_sel = fechas_dia[random.randint(0, fechas_dia_disponibles - 1)]

                                # Validar fecha a programar.
                                if fecha_sel in self.fechas_inactivas:
                                    fechas_dia_disponibles -= 1
                                    continue  # Avanzar a siguiente iteración para probar una nueva fecha.

                                # Validar si hay grupos programados en la misma fecha seleccionada.
                                peso_examenes_en_fecha = set(examen.grupo.materia for examen in self.examenes if
                                                             examen.fecha == fecha_sel)

                                peso_examenes_en_fecha = sum(materia.peso for materia in peso_examenes_en_fecha)

                                if peso_menor_examenes == -1 or peso_menor_examenes > peso_examenes_en_fecha:
                                    fecha_menos_carga = fecha_sel
                                    peso_menor_examenes = peso_examenes_en_fecha
                                    horario_seleccionado = h_max_size

                                fechas_dia = [f for f in fechas_dia if f != fecha_sel]
                                fechas_dia_disponibles -= 1

                            horarios_maximo_size = [h for h in horarios_maximo_size if h != h_max_size]
                            horarios_max_disponibles -= 1

                        if fecha_menos_carga is not None and horario_seleccionado is not None:
                            nuevo_examen = Examen(grupo, fecha_menos_carga, horario_seleccionado)
                            examen_programado = True
                            self.examenes.append(nuevo_examen)
                            grupo.examen_programado = True
                    else:
                        while fechas_dia_disponibles > 0 and not examen_programado:
                            # Seleccionar fecha de asignación del examen.
                            fecha_sel = fechas_dia[fechas_dia_disponibles - 1]

                            # Validar si hay grupos programados en la misma fecha seleccionada.
                            examenes_en_fecha = [examen.grupo.materia.codigo for examen in self.examenes if
                                                 examen.fecha == fecha_sel]
                            if examenes_en_fecha:
                                examenes_fechas_posibles = {fecha_posible:
                                                                set(examen.grupo.materia
                                                                    for examen in self.examenes if
                                                                    examen.fecha == fecha_posible)
                                                            for fecha_posible in fechas_dia if
                                                            fecha_posible not in self.fechas_inactivas}

                                examenes_fechas_posibles = {f: sum([mat.peso for mat in materias_fecha])
                                                            for f, materias_fecha in examenes_fechas_posibles.items()}

                                fecha_menos_carga = sorted(examenes_fechas_posibles.items(), key=itemgetter(1))[0][0]
                                nuevo_examen = Examen(grupo, fecha_menos_carga, max_h)
                            else:
                                # Validar fecha a programar.
                                if fecha_sel in self.fechas_inactivas:
                                    fechas_dia_disponibles -= 1
                                    continue    # Avanzar a siguiente iteración para probar una nueva fecha.
                                nuevo_examen = Examen(grupo, fecha_sel, max_h)

                            examen_programado = True
                            self.examenes.append(nuevo_examen)
                            grupo.examen_programado = True

                            fechas_dia_disponibles -= 1
                horarios_disponibles -= 1
        self.organizar_semanas()

    def listar_indices_semanas(self):
        """
        Lista el índice de cada semana disponible (generalmente, [0, 1]).
        :return: lista de índices de semana.
        """
        days = [day.dayofweek for day in self.rango_fechas]
        dia_corte = 6  # Día final de la semana (Domingo).
        return [i+1 for i, d in enumerate(days) if d == dia_corte]

    def organizar_semanas(self):
        """
        Realiza una organización por semana de los exámenes programados.
        :return: None
        """
        self.semanas_examenes = list()
        days = [day.dayofweek for day in self.rango_fechas]  # Días de la semana enmarcados en el rango de fechas
        dates = [d.date() for d in self.rango_fechas]
        examenes_dias = [(f, []) for f in days]

        for exam in self.examenes:
            index = dates.index(exam.fecha)
            examenes_dias[index][1].append(exam)

        DC = 6  # Día final de la semana (Domingo).
        start = 0

        for i, d in enumerate(examenes_dias):
            if d[0] == DC:
                self.semanas_examenes.append(examenes_dias[start:i + 1])
                start = i + 1
        self.semanas_examenes.append(examenes_dias[start:])

    def calculate_fitness(self, fechas_sugeridas_docentes=None):
        """
        Calcula el grado de factibilidad (feasibility) que tiene un individuo de la población, con respecto a las
        restricciones de preferencia (Soft Constraints).

        Soft Constraints (SC):

        1. Los exámenes cuyas asignaturas sean las de mayor peso (nivel de dificultad) deben quedar programados para la
        última semana del período de evaluaciones. 5!
        2. Los exámenes deben quedar distribuidos a lo largo de las fechas disponibles de tal forma que cada semana de
        evaluación tenga una carga equivalente. 4!
        3. Los grupos de la misma asignatura deben programarse en las fechas iguales o más cercanas posibles entre sí. 3!
        4. Algunos docentes tienen preferencia por sugerir la fecha de programación para algún examen (en cuál día o
        semana programarlo). 2!

        Fitness Function (FF): min (FF)
        Minimizar el grado de penalización por violación de las restricciones de preferencia definidas (SC).

        :return: Valor de calidad o fitness del individuo evaluado. Mientras más pequeño es el valor,
        mejor fitness representa el individuo.
        """

        # Evaluación de cada SC para calcular el Fitness del individuo (calendario).
        sc1 = self.evaluar_asignaturas_peso()
        sc2 = self.evaluar_distribucion()
        sc3 = self.evaluar_grupos_comunes()
        sc4 = self.evaluar_preferencia_docente(fechas_sugeridas_docentes)
        self.fitness = sc1 + sc2 + sc3 + sc4

    def crossover(self, partner):
        """
        :param partner: Individuo seleccionado para formar una pareja y hacer la reproducción.
        Realiza la recombinación de dos cromosomas. Esto significa que en este paso del Algoritmo Genético, se crea un
        nuevo hijo (un nuevo calendario) a partir de la mezcla de dos cromosomas madre y padre. El nuevo cromosoma se agrega a
        la población de individuos utilizando alguna técnica de reemplazo.
        :return: offspring, un nuevo calendario hijo producto de la reproducción de dos individuos.
        """
        offspring = CalendarChromosomeTipoI(rango_fechas=self.rango_fechas, materias=self.materias,
                                       fechas_inactivas=self.fechas_inactivas)

        # Nueva implementación de Crossover
        # Semanas: [Semana0[(Día, [Exámenes])], Semana1[(Día, [Exámenes])]]

        # Lleno con Semana 1 del cromosoma actual
        for dia, examenes_dia in self.semanas_examenes[0]:
            for examen in examenes_dia:
                offspring.examenes.append(examen)

        # Lleno con Semana 2 del cromosoma partner
        for dia, examenes_dia in partner.semanas_examenes[1]:
            for examen in examenes_dia:
                if not any(examen == ex_registro for ex_registro in offspring.examenes):
                    offspring.examenes.append(examen)

        # Se agregan los exámenes que no se encontraban en ninguna de las semanas seleccionadas
        # Aleatoriamente se elige uno de los calendarios padres y se utiliza la semana que no se tuvo en cuenta antes
        calendario_random = random.randint(1, 2)
        if calendario_random == 1:
            for dia, examenes_dia in self.semanas_examenes[1]:
                for examen in examenes_dia:
                    if not any(examen == ex_registro for ex_registro in offspring.examenes):
                        offspring.examenes.append(examen)
        else:
            for dia, examenes_dia in partner.semanas_examenes[0]:
                for examen in examenes_dia:
                    if not any(examen == ex_registro for ex_registro in offspring.examenes):
                        offspring.examenes.append(examen)

        offspring.organizar_semanas()
        return offspring

    def mutate(self, mutation_rate):
        """
        Realiza alteraciones aleatorias en el cromosoma para que la población a la que pertenece sea más diversa dentro
        del espacio de búsqueda.
        :return: None
        """
        dict_days = self.set_dates_weekday()
        for exam in self.examenes:
            rand = random.randint(0, 10) / 100
            if rand < mutation_rate:
                fecha_sel = dict_days[exam.horario.dia][random.randint(0, len(dict_days[exam.horario.dia]) - 1)]
                if fecha_sel not in self.fechas_inactivas:
                    exam.fecha = fecha_sel

    def evaluar_asignaturas_peso(self):
        """
        Método asociado a la SC No. 1: Los exámenes cuyas asignaturas sean las de mayor peso (nivel de dificultad) deben
        quedar programados para la última semana del período de evaluaciones. Penalty = 5!

        W = # de semanas que constituyen el corte de programación de exámenes.
        Ei = Examen i en el listado de exámenes programados.
        P1 = Costo total de dificultad de los exámenes en la primera semana.
        P2 = Costo total de dificultad de los exámenes en la segunda semana.
        P(Ei) = Valor de dificultad del examen Ei.
        penalty = Valor de penalización asignado a la SC No. 1.
        :return: Valor de penalización de la restricción.
        """
        penalty = math.factorial(5)
        P1, P2 = 0, 0
        examenes_w1, examenes_w2 = 0, 0
        try:
            for day, exams in self.semanas_examenes[0]:
                P1 += sum([e.dificulty() for e in exams])
                examenes_w1 += len(exams)
            for day, exams in self.semanas_examenes[1]:
                P2 += sum([e.dificulty() for e in exams])
                examenes_w2 += len(exams)
            if examenes_w1 > 0 and examenes_w2 > 0:
                diferencia_dificultad = (P1 / examenes_w1) - (P2 / examenes_w2)
                if diferencia_dificultad > 0:
                    self.conflicto_SC1 = diferencia_dificultad
                    return penalty
        except IndexError:
            raise IndexError('Las fechas disponibles para los exámenes son inconsistentes (deben ocupar dos semanas).')
        return 0

    def evaluar_distribucion(self):
        """
        Método asociado a la SC No. 2: Los exámenes deben quedar distribuidos a lo largo de las fechas disponibles de
        tal forma que cada semana de evaluación tenga una carga equivalente. Penalty = 4!

        Variables:
        W = # de semanas que constituyen el corte de programación de exámenes.
        ED = # de exámenes en un día.
        Ei = Examen i en el listado de exámenes programados.
        P = Costo total de dificultad de los exámenes en una semana.
        Pi = Valor de dificultad del examen Ei.

        :return: Valor de penalización de la restricción.
        """
        penalty = math.factorial(4)
        # Máximo grado de dificultad que debería programarse en una semana de exámenes.
        max_dificultad = math.ceil(sum([mat.peso for mat in self.materias]) / len(self.semanas_examenes))
        for week in self.semanas_examenes:
            P = 0
            for d, exams in week:
                P += sum([e.dificulty() for e in exams])
            if P > max_dificultad:
                self.conflicto_SC2 += (P - max_dificultad)

        if self.conflicto_SC2 > 0:
            return penalty
        return 0

    def evaluar_grupos_comunes(self):
        """
        Método asociado a la SC No. 3: Los grupos de la misma asignatura deben programarse en las fechas iguales o más
        cercanas posibles entre sí. 3!
        :return: Valor de penalización de la restricción.
        """
        penalty = math.factorial(3)
        dict_materias_comunes = {key.codigo_completo: [] for key in self.materias}
        # Agrupamos los exámenes de las mismas materias en sus respectivos grupos.
        for exam in self.examenes:
            dict_materias_comunes[exam.grupo.materia.codigo_completo].append(exam)
        for materia, examenes in dict_materias_comunes.items():
            if examenes and not self.examenes_igual_semana(examenes):
                self.conflicto_SC3 += len(examenes)

        if self.conflicto_SC3 > 0:
            return penalty
        return 0

    def examenes_igual_semana(self, examenes):
        numeros_semana = []
        for exam in examenes:
            for i, week in enumerate(self.semanas_examenes):
                for dia, exams in week:
                    if exam.grupo.materia.codigo_completo in [e.grupo.materia.codigo_completo for e in exams]:
                        numeros_semana.append(i)
        return numeros_semana[0] != (sum(numeros_semana) / len(numeros_semana))

    def evaluar_preferencia_docente(self, fechas_sugeridas_docentes=None):
        """
        Método asociado a la SC No. 4: Algunos docentes tienen preferencia por sugerir la fecha de programación para
        algún examen (en cuál día o semana programarlo). 2!
        :return: Valor de penalización de la restricción.
        """
        penalty = math.factorial(2)
        if fechas_sugeridas_docentes:
            for exam in self.examenes:
                keydict_grupo = exam.grupo.materia.codigo_completo + '-' + exam.grupo.literal
                if keydict_grupo in fechas_sugeridas_docentes and exam.fecha != fechas_sugeridas_docentes[keydict_grupo]:
                    self.conflicto_SC4 += 1

            if self.conflicto_SC4 > 0:
                return penalty
        return 0

    def output_html(self, info=''):
        """
        Genera calendario impreso en HTML.
        :param info: encabezado del contenido
        :return: str que representa al HTML
        """
        st = info if info else ''
        dias_semana = {0: 'Lunes', 1: 'Martes', 2: 'Miércoles', 3: 'Jueves', 4: 'Viernes', 5: 'Sábado',
                       6: 'Domingo'}
        st += '<!doctype html><html lang="en"><head><meta charset="UTF-8">' \
              '<title>Document</title></head><body><table width="">' \
              '<tr><td><h2 style="font-size:100%">CalendarioIndividuo</h2></td></tr>'

        calstr = 'CalStr\n'
        for i, week in enumerate(self.semanas_examenes):
            calstr += 'Semana {0}--> '.format(i + 1)
            for day, exams in week:
                calstr += ' {0} - ('.format(dias_semana[day])
                for exam in exams:
                    calstr += '**>{0}'.format(exam)
                calstr += ' )'
            calstr += '\n'

        for i, week in enumerate(self.semanas_examenes):
            st += '<tr><td><strong>Semana{0}</strong></td></tr><tr>'.format(i + 1)
            for day, exams in week:
                st += '<td style="vertical-align:top"><i>{0}</i></br>'.format(dias_semana[day])
                for exam in exams:
                    st += '</br>{0}'.format(exam)
                st += '</td>'
            st += '</tr></br><tr><td>' + '-' * 20 + '</td></tr>'
        st += '</table></br>'
        st += 'FITNESS: ' + str(self.fitness) + ' - ' + str(self.conflicto_SC1) + ' ' + \
              str(self.conflicto_SC2) + ' ' + str(self.conflicto_SC3) + ' ' + str(self.conflicto_SC4) + \
              '</body>\
          </html>'
        return st

    def __str__(self):
        """
        Imprime el contenido del calendario como texto.
        :return: str con el contenido del calendario
        """
        dias_semana = {0: 'Lunes', 1: 'Martes', 2: 'Miércoles', 3: 'Jueves', 4: 'Viernes', 5: 'Sábado', 6: 'Domingo'}
        output = ''
        for i, week in enumerate(self.semanas_examenes):
            output += 'Semana {0}\n'.format(i + 1)
            for day, exams in week:
                output += '{0}\n{1}\n'.format(dias_semana[day], exams)
        return output
