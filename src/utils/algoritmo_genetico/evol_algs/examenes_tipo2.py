import sys, os

path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
if not path in sys.path:
    sys.path.insert(1, path)
del path

from operator import (
    attrgetter,
    itemgetter
)
from evol_algs.data import (
    Horario,
    Examen,
    ExamenTipoII,
)
import math
import random
import datetime


class CalendarChromosomeTipoII:
    """
    Cada objeto de la clase CalendarioChromosome hace las veces de un Cromosoma o Individuo en la población
    (espacio de soluciones). Se le conoce como Fenotipo, pues está constituído por información genética.
    Este tipo de Calendario representa una solución para Exámenes Tipo II: habilitaciones y opcionales.
    """
    def __init__(self, rango_fechas, materias, fechas_inactivas):
        self.rango_fechas = rango_fechas
        self.examenes = list()
        self.examenes_materias = list()
        self.semanas_examenes = list()
        self.dias_examenes = list()
        self.materias = materias
        self.fechas_inactivas = fechas_inactivas
        self.violated_constraints = 0
        self.fitness = 0
        self.conflicto_SC1 = 0
        self.conflicto_SC2 = 0
        self.conflicto_SC3 = 0
        self.conflicto_SC4 = 0
        self.set_examenes_dias()

    def set_examenes_dias(self):
        """
        Inicializa el listado de días disponibles para los exámenes de la forma [(fecha, [examenes_fecha])].
        :return: None
        """
        self.dias_examenes = [(fecha.date(), []) for fecha in self.rango_fechas]

    def organizar_semanas(self):
        """
        Realiza una organización por semana de los exámenes programados.
        :return: None
        """
        self.semanas_examenes = list()
        days = [day.dayofweek for day in self.rango_fechas]  # Días de la semana enmarcados en el rango de fechas
        dates = [d.date() for d in self.rango_fechas]
        examenes_dias = [(f, []) for f in days]

        for exam in self.examenes:
            index = dates.index(exam.fecha)
            examenes_dias[index][1].append(exam)

        DC = 6  # Día final de la semana (Domingo).
        start = 0

        for i, d in enumerate(examenes_dias):
            if d[0] == DC:
                self.semanas_examenes.append(examenes_dias[start:i + 1])
                start = i + 1
        self.semanas_examenes.append(examenes_dias[start:])

    def inicializar(self, grupos):
        """
        Generamos un nuevo genotipo del individuo, es decir, la representación de una instancia de solución (su ADN).
        Cada gen está representado por una unidad de información, que para el caso equivale a cada examen del calendario
        creado. El cromosoma creado satisface todas las HC del problema, de tal forma que luego se empiece a optimizar
        la solución a partir de las SC.

        Hard Constraints (HC):
        HC1. 1. No se pueden programar exámenes en días NO hábiles.
        HC2. 2. Los exámenes se deben programar en el período de fechas estipulado en el
        calendario académico (normalmente tres días).
        HC3. 3. Un examen de habilitación u opcional sólo debe ser programado una vez.
        HC4. 4. El número de exámenes de habilitación/opcionales debe ser equitativo
        entre los días programados.
        HC5. 5. Los exámenes cuyas asignaturas sean las de mayor peso (nivel de dificultad)
        deben quedar programados para los últimos días.
        :return: None
        """
        # Se realiza la programación por materias.
        limite_examenes = math.ceil(len(self.materias) / len(self.rango_fechas)) # Límite de exámenes diarios según
        # cantidad de materias del semestre y número de días disponibles.

        materias_ordenadas_peso = sorted(self.materias, key=attrgetter('peso'))
        index_fecha = 0
        for materia in materias_ordenadas_peso:
            if len(self.dias_examenes[index_fecha][1]) >= limite_examenes:
                index_fecha += 1
            fecha_programar = self.dias_examenes[index_fecha]
            if fecha_programar[0] in self.fechas_inactivas:
                raise ValueError('La fecha {0} se encuentra dentro de los días inactivos y no puede incluirse en la '
                                 'programación. Si desea ignorar esta condición, desmarque esta fecha del listado '
                                 'de inactivos.'.format(fecha_programar[0]))
            nuevo_examen = ExamenTipoII(materia, fecha_programar[0], Horario(dia=fecha_programar[0].weekday(),
                                                                             hora_inicio=datetime.time(0),
                                                                             hora_fin=datetime.time(0)))
            fecha_programar[1].append(nuevo_examen)
            self.examenes_materias.append(nuevo_examen)
        self.organizar_grupos_examenes(grupos)
        self.organizar_semanas()

    def organizar_grupos_examenes(self, grupos):
        """
        Realiza una organización por semana de los exámenes programados.
        :return: None
        """
        for grupo in grupos:
            for examen in self.examenes_materias:
                if grupo.materia.codigo_completo == examen.materia.codigo_completo:
                    self.examenes.append(Examen(grupo=grupo, fecha=examen.fecha, horario=examen.horario))

    def calculate_fitness(self, fechas_sugeridas_docentes):
        """
        Calcula el grado de factibilidad (feasibility) que tiene un individuo de la población, con respecto a las
        restricciones de preferencia (Soft Constraints).

        Fitness Function (FF): min (FF)
        Minimizar el grado de penalización por violación de las restricciones de preferencia definidas (SC).

        :return: Valor de calidad o fitness del individuo evaluado. Mientras más pequeño es el valor,
        mejor fitness representa el individuo.
        """
        self.fitness = 0

    def crossover(self, partner):
        """
        :param partner: Individuo seleccionado para formar una pareja y hacer la reproducción.
        Realiza la recombinación de dos cromosomas. Esto significa que en este paso del Algoritmo Genético, se crea un
        nuevo hijo (un nuevo calendario) a partir de la mezcla de dos cromosomas madre y padre. El nuevo cromosoma se agrega a
        la población de individuos utilizando alguna técnica de reemplazo.
        :return: offspring, un nuevo calendario hijo producto de la reproducción de dos individuos.
        """
        crossing_index = random.randint(0, len(self.examenes_materias) - 1)
        offspring = CalendarChromosomeTipoI(rango_fechas=self.rango_fechas, materias=self.materias,
                                            fechas_inactivas=self.fechas_inactivas)
        offspring.examenes = self.examenes_materias[:crossing_index] + partner.examenes[crossing_index:]
        return offspring

    def evaluar_preferencia_docente(self, fechas_sugeridas_docentes):
        """
        Método asociado a la SC No. 4: Algunos docentes tienen preferencia por sugerir la fecha de programación para
        algún examen (en cuál día o semana programarlo). 2!
        :return: Valor de penalización de la restricción.
        """
        penalty = math.factorial(2)
        for exam in self.examenes_materias:
            keydict_grupo = exam.grupo.materia.codigo_completo + '-' + exam.grupo.literal
            if keydict_grupo in fechas_sugeridas_docentes and exam.fecha != fechas_sugeridas_docentes[keydict_grupo]:
                self.conflicto_SC4 += 1

        if self.conflicto_SC4 > 0:
            return penalty
        return 0

    def output_html(self, info=''):
        """
        Genera calendario impreso en HTML.
        :param info: encabezado del contenido
        :return: str que representa al HTML
        """
        st = info if info else ''
        dias_semana = {0: 'Lunes', 1: 'Martes', 2: 'Miércoles', 3: 'Jueves', 4: 'Viernes', 5: 'Sábado',
                       6: 'Domingo'}
        st += '<!doctype html><html lang="en"><head><meta charset="UTF-8">' \
              '<title>Document</title></head><body><table width="">' \
              '<tr><td><h2 style="font-size:100%">CalendarioIndividuo</h2></td></tr>'

        calstr = 'CalStr\n'
        for i, week in enumerate(self.semanas_examenes):
            calstr += 'Semana {0}--> '.format(i + 1)
            for day, exams in week:
                calstr += ' {0} - ('.format(dias_semana[day])
                for exam in exams:
                    calstr += '**>{0}'.format(exam)
                calstr += ' )'
            calstr += '\n'

        for i, week in enumerate(self.semanas_examenes):
            st += '<tr><td><strong>Semana{0}</strong></td></tr><tr>'.format(i + 1)
            for day, exams in week:
                st += '<td style="vertical-align:top"><i>{0}</i></br>'.format(dias_semana[day])
                for exam in exams:
                    st += '</br>{0}'.format(exam)
                st += '</td>'
            st += '</tr></br><tr><td>' + '-' * 20 + '</td></tr>'
        st += '</table></br>'
        st += 'FITNESS: ' + str(self.fitness) + ' - ' + str(self.conflicto_SC1) + ' ' + \
              str(self.conflicto_SC2) + ' ' + str(self.conflicto_SC3) + ' ' + str(self.conflicto_SC4) + \
              '</body>\
          </html>'
        return st

    def __str__(self):
        """
        Imprime el contenido del calendario como texto.
        :return: str con el contenido del calendario
        """
        dias_semana = {0: 'Lunes', 1: 'Martes', 2: 'Miércoles', 3: 'Jueves', 4: 'Viernes', 5: 'Sábado', 6: 'Domingo'}
        output = ''
        for i, week in enumerate(self.semanas_examenes):
            output += 'Semana {0}\n'.format(i + 1)
            for day, exams in week:
                output += '{0}\n{1}\n'.format(dias_semana[day], exams)
        return output
