import sys, os

path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
if not path in sys.path:
    sys.path.insert(1, path)
del path

from evol_algs.utils import wsdl_holidays_client
from operator import (
    attrgetter,
    itemgetter
)
from pympler import asizeof
from evol_algs.data import (
    SemestreAcademico,
    GrupoMateria,
    Materia,
    Horario,
    Examen,
)
from evol_algs.examenes_tipo1 import CalendarChromosomeTipoI
from evol_algs.examenes_tipo2 import CalendarChromosomeTipoII
import datetime
import json
import math
import random
import pandas


class GeneticAlgorithm:
    """
    Clase que representa el Algoritmo Genético. Dependiendo del tipo de exámenes ingresa o se omite el proceso de
    optimización heurística.
    """
    def __init__(self, max_population=50, crossover_rate=0, mutation_rate=0.05, max_generations=20, BEST_FITNESS=0,
                 start_date=None, end_date=None, tipo_examenes=None):
        """
        Inicializa el Algoritmo de Calendarización.
        :param max_population: cantidad máxima de población configurada para optimización evolutiva.
        :param crossover_rate: parámetro de probabilidad de cruce para optimización.
        :param mutation_rate: parámetro de probabilidad de mutación para optimización.
        :param max_generations: cantidad máxima de iteraciones de la población de individuos.
        :param BEST_FITNESS: valor definido para identificar un calendario "perfecto".
        :param start_date: fecha de inicio del período de exámenes a programar.
        :param end_date: fecha de fin del período de exámenes a programar.
        :param tipo_examenes: clasificación de los exámenes a programar (tipo 1 y tipo 2).
        """
        self.max_population = max_population  # Tamaño máximo de la población.
        self.crossover_rate = crossover_rate  # Índice o probabilidad de realizar recombinación de cromosomas.
        self.mutation_rate = mutation_rate  # Índice o probabilidad de aplicar mutación a un cromosoma.
        self.max_generations = max_generations
        self.BEST_FITNESS = BEST_FITNESS
        self.start_date = start_date  # Fecha de inicio de corte
        self.end_date = end_date  # Fecha de fin de corte
        self.materias = []
        self.dict_materias = dict()
        self.grupos = []
        self.fechas_sugeridas_docentes = []
        self.tipo_examenes = tipo_examenes
        self.population = Population(self.max_population, self.mutation_rate, tipo_examenes=tipo_examenes)
        self.fechas_inactivas = wsdl_holidays_client.listar_fechas_simple()

        if not self.start_date or not self.end_date:
            raise ValueError('Debe proporcionar las fechas de inicio y fin del período de programación.')
        self.rango_fechas = pandas.date_range(self.start_date, self.end_date)  # Rango de fechas para programar

    def set_materias(self, materias):
        """
        Inicializa las materias a programar.
        :param materias: diccionario de materias
        :return: None
        """
        for m in materias:
            materia = Materia(plan_de_estudios=m['plan_de_estudios'], codigo=m['codigo'], nombre=m['nombre'],
                              creditos=m['creditos'], peso=m['peso'])
            self.materias.append(materia)
            self.dict_materias[m['plan_de_estudios'] + m['codigo']] = materia

    def set_grupos(self, grupos):
        """
        Inicializa los grupos a programar.
        :param materias: diccionario de grupos
        :return: None
        """
        for g in grupos:
            self.grupos.append(
                GrupoMateria(g['literal'], self.dict_materias[g['materia']], [Horario(h['dia'], h['hora_inicio'],
                                                                      h['hora_fin'], h['salon'])
                                                              for h in g['horarios']])
            )
        self.grupos.sort(key=attrgetter('cantidad_horarios_max', 'size_widest_timeslot'))

    def set_fechas_inactivas(self, fechas_inactivas=list()):
        self.fechas_inactivas = fechas_inactivas

    def set_fechas_sugeridas(self, fechas_sugeridas_docentes=list()):
        self.fechas_sugeridas_docentes = fechas_sugeridas_docentes

    def programar_calendario(self):
        """
        Inicializa y ejecuta todo el algoritmo de calendarización dependiendo del tipo de exámenes.
        :param tipo_examenes: int que especifica qué tipo de exámenes se programarán.
        :return: JSON con exámenes programados para los grupos.
        """
        semestre = SemestreAcademico(self.grupos, self.materias)
        self.population.populate(semestre.grupos, semestre.materias, self.rango_fechas, self.fechas_inactivas,)
        excelent_solution = False
        counter_gen = 1
        self.population.get_population_fitness(semestre.max_dificultad, self.fechas_sugeridas_docentes)
        best_individual = self.population.get_best_individual()
        self.max_generations -= 1

        if self.tipo_examenes == 1:
            while self.max_generations > 0 and not excelent_solution:
                if best_individual.fitness == self.BEST_FITNESS:
                    excelent_solution = True
                    continue
                if best_individual.fitness > \
                        self.population.get_best_individual().fitness:
                    best_individual = self.population.get_best_individual()
                self.population = self.population.evolve(semestre.grupos)
                self.population.get_population_fitness(semestre.max_dificultad, self.fechas_sugeridas_docentes)
                self.max_generations -= 1
                counter_gen += 1

            # Se programan los grupos en la siguiente semana del período de exámenes cuando la disponibilidad es nula
            # en el período normal de evaluaciones.
            grupos_sin_programar = [grupo for grupo in self.grupos if not grupo.examen_programado]
            calendario_temporal = CalendarChromosomeTipoI(rango_fechas=self.rango_fechas)
            dict_days = calendario_temporal.set_dates_weekday()
            for grupo in grupos_sin_programar:
                max_h = grupo.horarios[0]
                fechas_dia = dict_days[max_h.dia] if max_h.dia in dict_days.keys() else []
                if not fechas_dia:
                    raise ValueError('No es posible encontrar una fecha para alguno de los días de clase del '
                                     'grupo {0}'.format(grupo))
                ultima_fecha_clase = max(fechas_dia)
                fecha_siguiente_semana = ultima_fecha_clase + datetime.timedelta(days=7)
                self.population.get_best_individual().examenes.append(Examen(grupo, fecha_siguiente_semana, max_h))
                grupo.examen_programado = True
        return self.leer_calendario_json()

    def leer_calendario_json(self):
        """
        Genera una estructura JSON con el listado de exámenes programados.
        :return: JSON de exámenes
        """
        calendario_semestre = [
            {'plan_de_estudios': examen.grupo.materia.plan_de_estudios,
             'codigo_materia': examen.grupo.materia.codigo,
             'literal_grupo': examen.grupo.literal,
             'nombre': examen.grupo.materia.nombre,
             'fecha': examen.fecha,
             'hora_inicio': examen.horario.hora_inicio,
             'hora_fin': examen.horario.hora_fin,
             'salon': examen.horario.salon,
             }
            for examen in self.population.get_best_individual().examenes
        ]

        return json.loads(json.dumps(calendario_semestre, default=self.date_handler))

    def leer_calendario_html(self):
        """
        Escribe un HTML con el calendario solución.
        :return: Archivo .html escrito
        """
        self.population.print_html_best_individual()

    def date_handler(self, obj):
        """
        Hace que un objeto date sea serializable mediante su conversión a formato ISO (AAAA-MM-DD).
        :param obj:
        :return: str con fecha en formato ISO
        """
        if hasattr(obj, 'isoformat'):
            return obj.isoformat()
        else:
            raise TypeError


class Population:
    """
    La clase Population ilustra todo el espacio de búsqueda o espacio de soluciones posibles al problema de la
    calendarización de exámenes. Bajo el modelo de Algoritmos Genéticos, cada población de individuos representa
    una nueva generación en el proceso evolutivo.
    """
    def __init__(self, max_population=1, mutation_rate=0.06, tipo_examenes=None):
        """
        Inicializa individuos (primeros calendarios a optimizar).
        :param max_population: número máximo de individuos de la población
        :param mutation_rate: parámetro de probabilidad de mutación para optimización.
        :param tipo_examenes: int que especifica qué tipo de exámenes se programarán.
        """
        self.individuals = []
        self.max_population = max_population
        self.crossover_rate = 0
        self.mutation_rate = mutation_rate
        self.tournament_size = 2
        if not tipo_examenes:
            raise ValueError('Tipo de exámenes no especificado.')
        self.tipo_examenes = tipo_examenes

    def populate(self, grupos, materias, rango_fechas, fechas_inactivas):
        """
        Agrega los individuos a la población de calendarios.
        :param grupos: listado de objetos tipo Grupo
        :param materias: listado de objetos tipo Materia
        :param rango_fechas: listado de fechas disponibles desde el inicio hasta el fin del período a programar
        :param fechas_inactivas: listado de fechas configuradas como festivas o inactivas
        :return: None
        """
        # Generamos una población aleatoria como Primera Generación de individios.
        if self.tipo_examenes == 1:
            for i in range(self.max_population):
                # Cada calendario satisface todas las HC.
                calendario = CalendarChromosomeTipoI(rango_fechas=rango_fechas, materias=materias,
                                                     fechas_inactivas=fechas_inactivas)
                calendario.inicializar(grupos)
                self.individuals.append(calendario)
        elif self.tipo_examenes == 2:
            for i in range(self.max_population):
                # Cada calendario satisface todas las HC.
                calendario = CalendarChromosomeTipoII(rango_fechas=rango_fechas, materias=materias,
                                                      fechas_inactivas=fechas_inactivas)
                calendario.inicializar(grupos)
                self.individuals.append(calendario)
        else:
            raise ValueError('Tipo de exámenes especificado inválido (debe ser 1 o 2.')

    def get_best_individual(self):
        """
        Retorna el calendario final obtenido.
        :return: Calendario solución tipo I o tipo II
        """
        return self.individuals[0]

    def get_population_fitness(self, max_creditos, fechas_sugeridas_docentes):
        """
        Calcula el valor fitness que califica la factibilidad de los individuos y ordena la población según dicho
        criterio, en orden ascendente (el mejor fitness tiende a cero).
        :param max_creditos: Número máximo de créditos que servirá de parámetro para evaluar la SC No. 1.
        :return None
        """
        if self.tipo_examenes == 1:
            for individual in self.individuals:
                # Organizamos los exámenes de cada calendario por semanas para la evaluación de las SC.
                individual.organizar_semanas()
                individual.calculate_fitness(fechas_sugeridas_docentes)

            # Ordenamos los calendarios según su fitness y el grado de impacto por violación de cada SC.
            self.individuals.sort(key=attrgetter('fitness', 'conflicto_SC1', 'conflicto_SC2', 'conflicto_SC3',
                                                 'conflicto_SC4'))
        if self.tipo_examenes == 2:
            for individual in self.individuals:
                individual.calculate_fitness(fechas_sugeridas_docentes)
            self.individuals.sort(key=attrgetter('fitness'))

    def evolve(self, grupos):
        """
        Crea nuevas generaciones de individuos a partir de la población inicial, siguiendo el proceso evolutivo de
        selección y reproducción (crossover y mutation) correspondiente. En términos del Algoritmo Genético, evoluciona
        la población a través de diferentes generaciones. Cada nueva población representa un espacio de soluciones
        posible para la calendarización de exámenes.
        :return: None
        """
        # Hay dos opciones para evolucionar: crear una nueva población temporal o reemplazando individuos de la
        # población actual.
        # Creamos una nueva generación de individuos utilizando una población temporal.
        new_population = Population(self.max_population, self.mutation_rate, self.tipo_examenes)

        # Agregamos el mejor individuo actual para mantener la calidad de la población
        new_population.individuals.append(self.get_best_individual())
        population_counter = 1
        while population_counter < new_population.max_population:
            # Realizamos proceso de selección de los cromosomas padres.
            # Opciones: Roulette wheel, tournament selection, truncation selection, stochastic uniform selection,
            # gender selection, stochastic universal sampling.
            # Se aplica Gender selection para dividir la población en dos (grupo de padres y grupo de madres).
            # Como paso siguiente, a cada uno de dichos subgrupos se les aplica Tournament Selection.
            if self.max_population > 1:
                partner_x = self.tournament_selection(self.individuals[:int(self.max_population / 2)])
                partner_y = self.tournament_selection(self.individuals[int(self.max_population / 2):])

                # Realizamos la reproducción de dos cromosomas para generar un nuevo hijo.
                offspring1 = partner_x #.crossover(partner_y)
                offspring2 = partner_y #.crossover(partner_x)

                # Agregamos los hijos a la población temporal.
                new_population.individuals.append(offspring1)
                population_counter += 1
                if population_counter < new_population.max_population:
                    new_population.individuals.append(offspring2)
                    population_counter += 1
            else:
                offspring = self.get_best_individual()
                # Agregamos el hijo a la población temporal.
                new_population.individuals.append(offspring)
                population_counter += 1
        self.print_html_evol_poblacion()
        return new_population

    def tournament_selection(self, individuals):
        """
        Realiza la selección por torneo a partir de una muestra de individuos y devuelve el individuo ganador si el
        proceso es exitoso.
        :param individuals:
        :return: Retorna el individuo ganador de la selección por torneo. Retorna None si el tamaño del torneo
        (tournament_size) es cero o si es mayor que la cantidad de individuos disponibles.
        """
        size_individuals = len(individuals)
        if size_individuals >= self.tournament_size:
            individual_index = 0
            best_individual = None
            while individual_index < self.tournament_size:
                selected_individual = individuals[random.randint(0, len(individuals) - 1)]
                if best_individual is None:
                    best_individual = selected_individual
                    continue
                if selected_individual.fitness < best_individual.fitness:
                    best_individual = selected_individual
                individual_index += 1
            return best_individual
        if size_individuals > 0:
            return individuals[random.randint(0, len(individuals) - 1)]
        return None

    def __str__(self, info=''):
        """
        Imprime como texto todos los calendarios presentes en la población de individuos.
        :param info: encabezado del contenido
        :return: str con el contenido
        """
        population_str = ''
        for calendario in self.individuals:
            population_str += str(calendario)
        return population_str

    def print_html_evol_poblacion(self, info=''):
        """
        Escribe HTML con todos los calendarios presentes en la población de individuos en evolución.
        :param info: encabezado del contenido
        :return: Archivo .html escrito
        """
        with open('evolucion_ag.html', 'w') as evolucion_ag:
            html = ''
            for calendario in self.individuals:
                html += calendario.output_html(info)
            evolucion_ag.write(html)

    def print_html_best_individual(self):
        """
        Escribe HTML con el calendario solución.
        :return: Archivo .html escrito
        """
        with open('calendario_solucion.html', 'w') as calendario:
            calendario.write(self.get_best_individual().output_html('Calendario solución'))
