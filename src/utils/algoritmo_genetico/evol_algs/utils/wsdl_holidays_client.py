from zeep import Client
import datetime


def consultar_dias_inactivos():
    """
    Retorna objeto JSON del servicio conteniendo las fechas festivas identificadas.
    :return: Objeto JSON
    """
    client = Client(wsdl='https://www.kayaposoft.com/enrico/ws/v1.0/index.php?wsdl')
    result = client.service.getPublicHolidaysForYear(
        2018, 'col', '')
    return result


def listar_fechas_simple():
    """
    Retorna el listado de las fechas festivas identificadas en formato simple.
    :return: [] Lista de fechas en formato datetime.
    """
    result = consultar_dias_inactivos()
    if result and result['error'] is None:
        fechas = [datetime.datetime(fecha['date']['year'], fecha['date']['month'],
                                    fecha['date']['day'])
                  for fecha in result['publicHolidays']]
        return fechas
    return None
