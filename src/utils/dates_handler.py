
import pandas


def calcular_rango_fechas(fecha_inicio=None, fecha_fin=None, fechas_ignorar=None):
    fechas = [f.date() for f in pandas.date_range(fecha_inicio, fecha_fin)]
    if fechas_ignorar:
        fechas = [f for f in fechas if f not in fechas_ignorar]
    return fechas   # Rango de fechas para programar


def set_dates_weekday_isoformat(rango_fechas=None, dias_filtro=None, isoformat=False):
    """
    Según el rango de fechas inicializado, agrupa las fechas que tienen el común el día de la semana al que
    corresponden.
    :return: dict con los días de la semana y su respectivo listado de fechas asociadas.
    """
    if dias_filtro:
        rango_fechas = [fecha for fecha in rango_fechas if fecha.weekday() in dias_filtro]

    days = [day.weekday() for day in rango_fechas]   # Días de la semana dentro del rango de fechas
    days_date_range = list(zip(rango_fechas, days))  # Días de la semana con cada fecha asociada
    dict_days = {day: '' for day in days}  # Diccionario que utiliza como clave cada día de la semana (de 0 a 6)

    for weekday in dict_days.keys():
        # Agrupamos las fechas que son del mismo día de la semana y lo agregamos al diccionario de días.
        if isoformat:
            dict_days[weekday] = [f.isoformat() for f, d in days_date_range if d == weekday]
        else:
            dict_days[weekday] = [f for f, d in days_date_range if d == weekday]
    return dict_days


def formatear_dict_fechas_dia(dict_fechas=None):
    dias_semana = {0: 'LUNES', 1: 'MARTES', 2: 'MIERCOLES', 3: 'JUEVES', 4: 'VIERNES', 5: 'SABADO',
                   6: 'DOMINGO'}
    dict_formateado = [{'dia_numero': dia, 'dia_nombre': dias_semana[dia], 'fechas': fechas} for dia, fechas in
                       dict_fechas.items()]
    return dict_formateado