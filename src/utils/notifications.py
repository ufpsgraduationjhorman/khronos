from fcm_django.models import FCMDevice
from pyfcm import FCMNotification
from schedulex.settings import FCM_DJANGO_SETTINGS, EMAIL_HOST_USER, EMAIL_HOST_PASSWORD
from django.contrib.auth import get_user_model
from django.template import loader
from django.core.mail import send_mail


User = get_user_model()


def send_mobile_notifications(subject=None, body=None, topic=None, data_message=None):
    push_service = FCMNotification(api_key=FCM_DJANGO_SETTINGS['FCM_SERVER_KEY'])
    # Enviar notificación a múltiples dispositivos mediante un listado de IDs de registro.
    devices_ids = [device.registration_id for device in FCMDevice.objects.all()]
    if topic:
        push_service.notify_topic_subscribers(topic_name="news", message_body=body)
    else:
        push_service.notify_multiple_devices(registration_ids=devices_ids,
                                             data_message=data_message)


def send_email_notifications():
    body = 'Se ha generado/actualizado el calendario de exámenes del semestre.'
    body += ' Puede conocer su calendario a través de la aplicación móvil o revisar el calendario de todo el ' \
            'Programa dirigiéndose al sitio web (sección Estudiante >> Calendario de Previos y Exámenes)'
    subject = 'Calendario de exámenes: Programa de Ingeniería de Sistemas'
    email_usuarios = [user.correo for user in User.objects.all()]
    html_message = loader.render_to_string('email/nuevo_calendario.html',
                                           {
                                               'titulo_mensaje': 'Calendario de exámenes',
                                               'mensaje': body,
                                               'website': '//ingsistemas.ufps.edu.co',
                                           })
    send_mail(subject, body, EMAIL_HOST_USER, email_usuarios, auth_user=EMAIL_HOST_USER,
              auth_password=EMAIL_HOST_PASSWORD, html_message=html_message)


def send_email_analisis_tiempo(info=None):
    body = 'Se ha generado/actualizado el calendario de exámenes del semestre.\n' + info
    subject = 'Calendario de exámenes: Programa de Ingeniería de Sistemas'
    html_message = loader.render_to_string('email/nuevo_calendario.html',
                                           {
                                               'titulo_mensaje': 'Análisis de Tiempo Calendario de Exámenes',
                                               'mensaje': body,
                                               'website': '//ingsistemas.ufps.edu.co',
                                           })
    send_mail(subject, body, EMAIL_HOST_USER, ['schedulex.app@gmail.com'], auth_user=EMAIL_HOST_USER,
              auth_password=EMAIL_HOST_PASSWORD, html_message=html_message)