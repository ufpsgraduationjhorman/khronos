import datetime


def validate_date_es(date, formato='%Y-%m-%d'):
    """
    Ej: '%d/%m/%Y'
    :param date:
    :param formato:
    :return: date o False
    """
    try:
        fecha = datetime.datetime.strptime(date, formato).date()
        if len(date) == 10:
            return fecha
    except ValueError:
        return False